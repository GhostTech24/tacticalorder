package test.tacticalOrder.dialog;

import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSetScenarioDialogCompleteness{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSetScenarioDialogCompleteness.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		
		// When
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		//Then
		WebAction.isVisible(SetScenarioDialog.getSetScenarioDialog(), true);
		WebAction.isVisible(SetScenarioDialog.getSetScenarioDialogTitle(), true);
		
		WebAction.isVisible(SetScenarioDialog.getScenarioCurrentTitle(), true);
		WebAction.isVisible(SetScenarioDialog.getScenarioNameInput(), true);
		WebAction.isVisible(SetScenarioDialog.getScenarioSaveButton(), true);
		WebAction.isVisible(SetScenarioDialog.getScenarioResetButton(), true);
		
		WebAction.isVisible(SetScenarioDialog.getScenariosTitle(), true);
		WebAction.isVisible(SetScenarioDialog.getScenarioLoadButton(), true);
		WebAction.isVisible(SetScenarioDialog.getScenarioDeleteButton(), true);
		WebAction.isVisible(SetScenarioDialog.getScenarioSelectionContainer(), true);
	}
}
