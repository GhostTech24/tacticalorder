package test.tacticalOrder.dialog;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSetPlayerDialogCompleteness{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSetPlayerDialogCompleteness.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		// When
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		
		//Then
		WebAction.isVisible(SetPlayerDialog.getSetPlayerDialog(), true);
		WebAction.isVisible(SetPlayerDialog.getSetPlayerDialogTitle(), true);
		
		WebAction.isVisible(SetPlayerDialog.getPlayersTitle(), true);
		WebAction.isVisible(SetPlayerDialog.getArrowDownPlayerButton(), true);
		WebAction.isVisible(SetPlayerDialog.getArrowUpPlayerButton(), true);
		WebAction.isVisible(SetPlayerDialog.getPlayerValueCurrent(), true);
		
		WebAction.isVisible(SetPlayerDialog.getColorBevoreButton(1), true);
		WebAction.isVisible(SetPlayerDialog.getColorValue(1), true);
		WebAction.isVisible(SetPlayerDialog.getColorNextButton(1), true);		
		
		WebAction.isVisible(SetPlayerDialog.getColorBevoreButton(2), true);
		WebAction.isVisible(SetPlayerDialog.getColorValue(2), true);
		WebAction.isVisible(SetPlayerDialog.getColorNextButton(2), true);
	}
}
