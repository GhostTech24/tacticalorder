package test.tacticalOrder.dialog;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSetLosingConditionDialogCompleteness{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSetLosingConditionDialogCompleteness.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		// When
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		
		//Then
		WebAction.isVisible(SetLosingConditionDialog.getSetLosingConditionDialog(), true);
		WebAction.isVisible(SetLosingConditionDialog.getSetLosingConditionDialogTitle(), true);
		
		WebAction.isVisible(SetLosingConditionDialog.getAllUnitsLostContainer(), true);
		WebAction.isVisible(SetLosingConditionDialog.getAllUnitsLostContainerTitle(), true);
		WebAction.isVisible(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldContainer(), true);	
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldContainerTitle(), true);
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), true);
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldXCoordinateValue(), true);
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldYCoordinateValue(), true);
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldSelectFieldButton(), false);
		
		WebAction.isVisible(SetLosingConditionDialog.getPlayerColorLosingConditionBevoreButton(), true);
		WebAction.isVisible(SetLosingConditionDialog.getPlayerColorLosingConditionValue(), true);
		WebAction.isVisible(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton(), true);
	}
}
