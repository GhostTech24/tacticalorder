package test.tacticalOrder.general;

import objectRepository.tacticalOrder.GeneralElement;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckAppName{
	
	private final static String expectedText = "Tactical Order";
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckAppName.class.getSimpleName()));
		
		// Given 
		// -
		
		// When
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		//Then
		WebAction.isText(GeneralElement.getTabName(), expectedText);
		
		// reset
		// -
	}
}
