package test.tacticalOrder.view.leveleditor.player;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckInvalidDeletePlayer{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckInvalidDeletePlayer.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		
		// When
		WebAction.click(SetPlayerDialog.getArrowDownPlayerButton());
		
		//Then		
		WebAction.isVisible(SetPlayerDialog.getColorBevoreButton(1), true);
		WebAction.isVisible(SetPlayerDialog.getColorValue(1), true);
		WebAction.isVisible(SetPlayerDialog.getColorNextButton(1), true);		
		
		WebAction.isVisible(SetPlayerDialog.getColorBevoreButton(2), true);
		WebAction.isVisible(SetPlayerDialog.getColorValue(2), true);
		WebAction.isVisible(SetPlayerDialog.getColorNextButton(2), true);
	}
}
