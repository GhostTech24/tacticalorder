package test.tacticalOrder.view.leveleditor.scenario;

import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;

import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSetScenarioNameBevoreDialogClose{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSetScenarioNameBevoreDialogClose.class.getSimpleName()));
		
		final String scenarioNameExpected = "My customly named Map";
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// When
		SetScenarioDialog.getScenarioNameInput().clear();
		SetScenarioDialog.getScenarioNameInput().sendKeys(scenarioNameExpected);
		
		// Then
		WebAction.isText(SetScenarioDialog.getScenarioNameInput(), scenarioNameExpected);
	}
}
