package test.tacticalOrder.view.leveleditor.playerColorUnit;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetTerrainDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetUnitDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckUnitColorAfterColorChange {
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckUnitColorAfterColorChange.class.getSimpleName()));
		
		final int xCoordinate = 0;
		final int yCoordinate = 0;
		final int playerId = 2;
		final String unwantedClassNames = "unitGraphicOnGameMap";
		final String textExpected = "playerColorGreen";
		final String textCurrent;	
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getUnitSelectionButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitBevoreButton()); // select for player 2
		WebAction.click(SetUnitDialog.getUnitButtons().get("infantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(xCoordinate).get(yCoordinate));
		WebAction.click(Leveleditor.getSetPlayerButton());
		
		// When
		WebAction.click(SetPlayerDialog.getColorNextButton(playerId));
		
		//Then
		textCurrent = Leveleditor.getUnitsCurrent().get(xCoordinate).get(yCoordinate).getAttribute("class").replace(unwantedClassNames, "").trim();
		WebAction.isText(textCurrent, textExpected);
	}
}
