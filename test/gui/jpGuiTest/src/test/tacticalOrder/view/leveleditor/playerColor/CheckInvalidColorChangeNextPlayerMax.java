package test.tacticalOrder.view.leveleditor.playerColor;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckInvalidColorChangeNextPlayerMax {
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckInvalidColorChangeNextPlayerMax.class.getSimpleName()));
		
		final int playerId = 1;
		final int colorsSize = 6;
		final int playersSizeDefault = 2;
		final String textExpected = "playerColorRed";
		final String textCurrent;	
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		for(int i = playersSizeDefault; i < colorsSize; i++) {
			WebAction.click(SetPlayerDialog.getArrowUpPlayerButton());
		}
		
		// When
		WebAction.click(SetPlayerDialog.getColorNextButton(playerId));
		
		//Then
		textCurrent = SetPlayerDialog.getColorValue(playerId).getAttribute("class");
		WebAction.isText(textCurrent, textExpected);
	}
}
