package test.tacticalOrder.view.leveleditor.scenario;

import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;

import testEngine.TestController;
import testEngine.WebAction;

public final class CheckGetScenarioName{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckGetScenarioName.class.getSimpleName()));
		
		final String scenarioNameExpected = "newScenario";
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		
		// When
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// Then
		WebAction.isText(SetScenarioDialog.getScenarioNameInput(), scenarioNameExpected);
	}
}
