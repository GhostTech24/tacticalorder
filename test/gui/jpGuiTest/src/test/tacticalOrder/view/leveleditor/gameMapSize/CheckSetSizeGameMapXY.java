package test.tacticalOrder.view.leveleditor.gameMapSize;

import java.util.List;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.leveleditor.SetSizeDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSetSizeGameMapXY{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSetSizeGameMapXY.class.getSimpleName()));
		
		final int xFieldsSizeExpected = 3;
		final int yFieldsSizeExpected = 4;
		final List<List<WebElement>> terrainsCurrent;
		final int xFieldsSizeCurrent , yFieldsSizeCurrent;
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
				
		// When
		// set size X
		WebAction.click(Leveleditor.getSetSizeButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 5
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 3
		// set size y
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 5
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 4
		
		WebAction.click(SetSizeDialog.getCloseDialogButton());
		
		// Then
		terrainsCurrent = Leveleditor.getTerrainsCurrent();
		xFieldsSizeCurrent = terrainsCurrent.size();
		yFieldsSizeCurrent = terrainsCurrent.get(0).size();
		WebAction.isNumber(xFieldsSizeCurrent, xFieldsSizeExpected);
		WebAction.isNumber(yFieldsSizeCurrent, yFieldsSizeExpected);
	}
}
