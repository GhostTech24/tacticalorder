package test.tacticalOrder.view.leveleditor.unit;

import objectRepository.tacticalOrder.dialog.leveleditor.SetTerrainDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetUnitDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testData.TestData;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckUnitPlacementValidTerrainDefault{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckUnitPlacementValidTerrainDefault.class.getSimpleName()));
		
		
		final int unit1XCoordinate = 2;
		final int unit1YCoordinate = 4;
		final String unit1Id = "infantry";
		final int unit2XCoordinate = 9;
		final int unit2YCoordinate = 9;
		final String unit2Id = "lightTank";
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getUnitSelectionButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitNextButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get(unit1Id));
		
		// When
		WebAction.click(Leveleditor.getTerrainsCurrent().get(unit1XCoordinate).get(unit1YCoordinate));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get(unit2Id));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(unit2XCoordinate).get(unit2YCoordinate));
		
		// Then
		WebAction.isEqualUnits(Leveleditor.getUnitsCurrent(), TestData.getUnitsPlaced1PlayerTerrainDefault());	
	}
}
