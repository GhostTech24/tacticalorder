package test.tacticalOrder.view.leveleditor.playerColor;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckValidColorChangeBevore1FreeBevore1FreeAfter2Player {
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckValidColorChangeBevore1FreeBevore1FreeAfter2Player.class.getSimpleName()));
		
		final int player1Id = 1;
		final int player2Id = 2;
		final int colorsSize = 6;
		final int clickUpCounter = colorsSize - 1;
		final int playersSizeDefault = 2;
		final String textExpected = "playerColorGreen";
		final String textCurrent;	
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		for(int i = playersSizeDefault; i < clickUpCounter; i++) {
			WebAction.click(SetPlayerDialog.getColorNextButton(player2Id));
			WebAction.click(SetPlayerDialog.getColorNextButton(player1Id));
		}
		
		// When
		WebAction.click(SetPlayerDialog.getColorBevoreButton(player1Id));
		
		//Then
		textCurrent = SetPlayerDialog.getColorValue(player1Id).getAttribute("class");
		WebAction.isText(textCurrent, textExpected);
	}
}
