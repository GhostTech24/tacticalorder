package test.tacticalOrder.view.leveleditor.saveAndLoad;

import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;

import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSaveAndLoadOfScenarioBevoreRefresh{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSaveAndLoadOfScenarioBevoreRefresh.class.getSimpleName()));
		
		final int entriySizeExpected = 1;
		
		final int entryIndex = 0;
		final boolean isCheckboxVisibleExpected = false;
		final String scenarioNameExpected = "newScenario";
				
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.deleteIndexedDb(WebAction.indexedDbName);
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// When
		WebAction.click(SetScenarioDialog.getScenarioSaveButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// Then
		WebAction.isNumber(SetScenarioDialog.getScenarioEntries().size(), entriySizeExpected);
		WebAction.isEqualScenarioToLoad(SetScenarioDialog.getScenarioEntries(), entryIndex, isCheckboxVisibleExpected, scenarioNameExpected);
	}
}
