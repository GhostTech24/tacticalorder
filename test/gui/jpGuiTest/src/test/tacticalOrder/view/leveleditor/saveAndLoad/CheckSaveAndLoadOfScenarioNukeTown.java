package test.tacticalOrder.view.leveleditor.saveAndLoad;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetSizeDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetTerrainDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testData.TestData;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSaveAndLoadOfScenarioNukeTown{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSaveAndLoadOfScenarioNukeTown.class.getSimpleName()));
		
		final int entriySizeExpected = 1;
		
		final int entryIndex = 0;
		final boolean isCheckboxVisibleExpected = false;
		final String scenarioNameExpected = "Nuke Town";
		final List<List<Map<String, String>>> terrainsExpected = TestData.getTerrainsSmall();
		final List<List<Map<String, String>>> unitsExpected = TestData.getUnitsEmptyTerrainSmall();
		final int playerSizeExpected = 2;
		final String playerColorValue1Expected = "playerColorRed";
		final String playerColorValue2Expected = "playerColorBlue";
		
		final Map<String, WebElement> terrainButtons;
		final List<List<WebElement>> terrainsCurrent, unitsCurrent;
		final int playerSizeCurrent;
		
		// Given
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.deleteIndexedDb(WebAction.indexedDbName);
		WebAction.click(MainMenu.getLevelEditorButton());	
		// set size X
		WebAction.click(Leveleditor.getSetSizeButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 5
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 3
		// set size y
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 5
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 4
		WebAction.click(SetSizeDialog.getCloseDialogButton());
		// set terrains - cities	
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		terrainButtons = SetTerrainDialog.getTerrainButtons();
		WebAction.click(terrainButtons.get("city"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(0).get(1));
		// set terrains - streets
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("street"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(0));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(1));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(2));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(3));
		// set terrains - woods
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("woods"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(0).get(3));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(0));
		// set terrains - sea
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("sea"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(1));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(2));
		// set name
		WebAction.click(Leveleditor.getSetScenarioButton());
		WebAction.insertText(SetScenarioDialog.getScenarioNameInput(), scenarioNameExpected, true);
		// save
		WebAction.click(SetScenarioDialog.getScenarioSaveButton());
		// prepare for load
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// When
		WebAction.click(SetScenarioDialog.getScenarioEntries().get(0).get(0));
		WebAction.click(SetScenarioDialog.getScenarioLoadButton());		
		
		// Then
		terrainsCurrent = Leveleditor.getTerrainsCurrent();
		WebAction.isEqualTerrains(terrainsCurrent, terrainsExpected);
		
		unitsCurrent = Leveleditor.getUnitsCurrent();
		WebAction.isEqualUnits(unitsCurrent, unitsExpected);
		
		WebAction.click(Leveleditor.getSetPlayerButton());
		playerSizeCurrent = Integer.parseInt(SetPlayerDialog.getPlayerValueCurrent().getText());
		WebAction.isNumber(playerSizeCurrent, playerSizeExpected);
		WebAction.isText(SetPlayerDialog.getColorValue(1).getAttribute("class"), playerColorValue1Expected);
		WebAction.isText(SetPlayerDialog.getColorValue(2).getAttribute("class"), playerColorValue2Expected);
		WebAction.click(SetPlayerDialog.getCloseDialogButton());
		
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), false);
		
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), false);
		WebAction.click(SetLosingConditionDialog.getCloseDialogButton());
		
		WebAction.click(Leveleditor.getSetScenarioButton());
		WebAction.isNumber(SetScenarioDialog.getScenarioEntries().size(), entriySizeExpected);
		WebAction.isEqualScenarioToLoad(SetScenarioDialog.getScenarioEntries(), entryIndex, isCheckboxVisibleExpected, scenarioNameExpected);
	}
}
