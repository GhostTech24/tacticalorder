package test.tacticalOrder.view.leveleditor.losingCondition;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckAddLosingConditionNew{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckAddLosingConditionNew.class.getSimpleName()));
		
		final String xValueExpected = "X: 0";
		final String yValueExpected = "Y: 0";
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
	
		// When
		WebAction.click(SetLosingConditionDialog.getUnitReachedFieldCheckbox());
		
		//Then
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), true);		
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldSelectFieldButton(), true);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldXCoordinateValue(), xValueExpected);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldYCoordinateValue(), yValueExpected);
	}
}
