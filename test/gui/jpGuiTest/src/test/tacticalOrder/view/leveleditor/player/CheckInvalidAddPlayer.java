package test.tacticalOrder.view.leveleditor.player;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckInvalidAddPlayer{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckInvalidAddPlayer.class.getSimpleName()));
		
		final int playerStartSizeDefault = 2;
		final int playerStartSizeMax = 6;
		final int playerStartSizeInvalid = playerStartSizeMax + 1;
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		for(int i = playerStartSizeDefault; i < playerStartSizeMax; i++) {
			WebAction.click(SetPlayerDialog.getArrowUpPlayerButton());
		}
		
		// When
		WebAction.click(SetPlayerDialog.getArrowUpPlayerButton());
		
		//Then
		for(int i = 1; i <= playerStartSizeMax; i++) {
			WebAction.isVisible(SetPlayerDialog.getColorBevoreButton(i), true);
			WebAction.isVisible(SetPlayerDialog.getColorValue(i), true);
			WebAction.isVisible(SetPlayerDialog.getColorNextButton(i), true);		
		}
		
		WebAction.isVisible(SetPlayerDialog.getColorBevoreButton(playerStartSizeInvalid), false);
		WebAction.isVisible(SetPlayerDialog.getColorValue(playerStartSizeInvalid), false);
		WebAction.isVisible(SetPlayerDialog.getColorNextButton(playerStartSizeInvalid), false);
	}
}
