package test.tacticalOrder.view.leveleditor;

import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testData.TestData;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckLevelEditorCompleteness{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckLevelEditorCompleteness.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		// When
		WebAction.click(MainMenu.getLevelEditorButton());
		
		//Then
		
		WebAction.isVisible(Leveleditor.getLeveleditorView(), true);
		WebAction.isVisible(Leveleditor.getSetUnitOrTerrainButton(), true);
		WebAction.isVisible(Leveleditor.getBackButton(), true);
		WebAction.isVisible(Leveleditor.getGameMapElement(), true);
		WebAction.isVisible(Leveleditor.getSetPlayerButton(), true);
		WebAction.isVisible(Leveleditor.getSetLosingConditionButton(), true);
		
		WebAction.isEqualTerrains(Leveleditor.getTerrainsCurrent(), TestData.getTerrainsDefault());
		
		// reset
		// -
	}
}
