package test.tacticalOrder.view.leveleditor.unit;

import objectRepository.tacticalOrder.dialog.leveleditor.SetTerrainDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetUnitDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

/** Implementation of the reported Bug "BUG0001".
 */
public final class CheckUnitPlacementValidAfterUnitAutoRemoval{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckUnitPlacementValidAfterUnitAutoRemoval.class.getSimpleName()));
		
		final boolean expectedVisability = true;
		final int xCoordinate = 0;
		final int yCoordinate = 0;
		final String unitId = "lightTank";
		final String terrainForbiddenForUnitId = "sea";
		final String terrainAllowedForUnitId = "plane";
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getUnitSelectionButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitNextButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get(unitId));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(xCoordinate).get(yCoordinate));
		
		// When
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getTerrainSelectionButton());
		WebAction.click(SetTerrainDialog.getTerrainButtons().get(terrainForbiddenForUnitId));
		
		WebAction.click(Leveleditor.getUnitsCurrent().get(xCoordinate).get(yCoordinate));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getTerrainButtons().get(terrainAllowedForUnitId));
		
		WebAction.click(Leveleditor.getTerrainsCurrent().get(xCoordinate).get(yCoordinate));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getUnitSelectionButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get(unitId));
		
		WebAction.click(Leveleditor.getTerrainsCurrent().get(xCoordinate).get(yCoordinate));
		
		// Then
		WebAction.isVisible(Leveleditor.getUnitsCurrent().get(xCoordinate).get(yCoordinate), expectedVisability);
	}
}
