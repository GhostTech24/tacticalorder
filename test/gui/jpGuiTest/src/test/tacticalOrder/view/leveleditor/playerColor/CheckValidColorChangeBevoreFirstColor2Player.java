package test.tacticalOrder.view.leveleditor.playerColor;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckValidColorChangeBevoreFirstColor2Player {
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckValidColorChangeBevoreFirstColor2Player.class.getSimpleName()));
		
		final int playeId = 1;
		final String textExpected = "playerColorWhite";
		final String textCurrent;	
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		
		// When
		WebAction.click(SetPlayerDialog.getColorBevoreButton(playeId));
		
		//Then
		textCurrent = SetPlayerDialog.getColorValue(playeId).getAttribute("class");
		WebAction.isText(textCurrent, textExpected);
	}
}
