package test.tacticalOrder.view.leveleditor.losingCondition;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckUpdateLosingConditionUnitReachedField2Conditions{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckUpdateLosingConditionUnitReachedField2Conditions.class.getSimpleName()));
		
		final int xCoordinate = 2;
		final int yCoordinate = 4;
		
		final String xValueExpected = "X: 2";
		final String yValueExpected = "Y: 4";
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		WebAction.click(SetLosingConditionDialog.getUnitReachedFieldCheckbox());
	
		// When
		WebAction.click(SetLosingConditionDialog.getUnitReachedFieldSelectFieldButton());
		WebAction.click(Leveleditor.getTerrainsCurrent().get(xCoordinate).get(yCoordinate));
		
		//Then
		WebAction.isVisible(SetLosingConditionDialog.getAllUnitsLostContainer(), true);
		
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), true);		
		WebAction.isVisible(SetLosingConditionDialog.getUnitReachedFieldSelectFieldButton(), true);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldXCoordinateValue(), xValueExpected);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldYCoordinateValue(), yValueExpected);
	}
}
