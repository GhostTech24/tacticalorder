package test.tacticalOrder.view.leveleditor.gameMapSize;

import java.util.List;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.leveleditor.SetSizeDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckInvalidSizeShrinkGameMapXY{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckInvalidSizeShrinkGameMapXY.class.getSimpleName()));
		
		final int xFieldsSizeDefault = 20;
		final int yFieldsSizeDefault = 20;
		final int xFieldsSizeExpected = 2;
		final int yFieldsSizeExpected = 2;
		final List<List<WebElement>> terrainsCurrent;
		final int xFieldsSizeCurrent , yFieldsSizeCurrent;
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetSizeButton());
		for(int i = xFieldsSizeDefault; i > xFieldsSizeExpected; i--) {
			WebAction.click(SetSizeDialog.getArrowDownXButton());
		}
		for(int i = yFieldsSizeDefault; i > yFieldsSizeExpected; i--) {
			WebAction.click(SetSizeDialog.getArrowDownYButton());
		}
				
		// When
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		
		WebAction.click(SetSizeDialog.getCloseDialogButton());
		
		// Then
		terrainsCurrent = Leveleditor.getTerrainsCurrent();
		xFieldsSizeCurrent = terrainsCurrent.size();
		yFieldsSizeCurrent = terrainsCurrent.get(0).size();
		WebAction.isNumber(xFieldsSizeCurrent, xFieldsSizeExpected);
		WebAction.isNumber(yFieldsSizeCurrent, yFieldsSizeExpected);
	}
}
