package test.tacticalOrder.view.leveleditor;

import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckLevelEditorNavigation{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckLevelEditorNavigation.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		
		// When
		WebAction.click(Leveleditor.getBackButton());
		WebAction.click(MainMenu.getLevelEditorButton());
		
		// Then - Leveleditor
		WebAction.isVisible(Leveleditor.getLeveleditorView(), true);	
		
		// reset
		// -
	}
}
