package test.tacticalOrder.view.leveleditor.playerColor;

import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckValidColorChangeNext0FreeBevore0FreeAfter2Player {
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckValidColorChangeNext0FreeBevore0FreeAfter2Player.class.getSimpleName()));
		
		final int playerId = 1;
		final String textExpected = "playerColorGreen";
		final String textCurrent;	
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetPlayerButton());
		
		// When
		WebAction.click(SetPlayerDialog.getColorNextButton(playerId));
		
		//Then
		textCurrent = SetPlayerDialog.getColorValue(playerId).getAttribute("class");
		WebAction.isText(textCurrent, textExpected);
	}
}
