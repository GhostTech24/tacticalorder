package test.tacticalOrder.view.leveleditor.losingCondition;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckGetLosingConditionDefault{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckGetLosingConditionDefault.class.getSimpleName()));
				
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
	
		// When
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		
		//Then
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), false);
	}
}
