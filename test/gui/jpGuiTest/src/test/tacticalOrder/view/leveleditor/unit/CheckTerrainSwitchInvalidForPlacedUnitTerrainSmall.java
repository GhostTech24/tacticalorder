package test.tacticalOrder.view.leveleditor.unit;

import java.util.List;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.leveleditor.SetSizeDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetTerrainDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetUnitDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testData.TestData;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckTerrainSwitchInvalidForPlacedUnitTerrainSmall{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckTerrainSwitchInvalidForPlacedUnitTerrainSmall.class.getSimpleName()));
		
		List<List<WebElement>> terrainsCurrent, unitsCurrent;
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());	
		// set size X
		WebAction.click(Leveleditor.getSetSizeButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 5
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 3
		// set size y
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 5
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 4
		WebAction.click(SetSizeDialog.getCloseDialogButton());
		// set citys
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getTerrainButtons().get("city"));
		terrainsCurrent = Leveleditor.getTerrainsCurrent(); 
		WebAction.click(terrainsCurrent.get(0).get(1));
		// set woods
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getTerrainButtons().get("woods"));
		WebAction.click(terrainsCurrent.get(0).get(3));
		WebAction.click(terrainsCurrent.get(2).get(0));	
		// set street
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getTerrainButtons().get("street"));
		WebAction.click(terrainsCurrent.get(1).get(0));
		WebAction.click(terrainsCurrent.get(1).get(1));
		WebAction.click(terrainsCurrent.get(1).get(2));
		WebAction.click(terrainsCurrent.get(1).get(3));
		// set infantry
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getUnitSelectionButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitNextButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("infantry"));
		WebAction.click(terrainsCurrent.get(0).get(1));
		WebAction.click(terrainsCurrent.get(2).get(1)); // still valid -> plane
		// set light tank
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("lightTank"));
		WebAction.click(terrainsCurrent.get(1).get(2)); 
		WebAction.click(terrainsCurrent.get(2).get(2)); // still valid -> plane
		
		// When
		// set sea
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getTerrainSelectionButton());
		WebAction.click(SetTerrainDialog.getTerrainButtons().get("sea"));
		unitsCurrent = Leveleditor.getUnitsCurrent(); 
		WebAction.click(unitsCurrent.get(2).get(1));
		WebAction.click(unitsCurrent.get(2).get(2));
		
		// Then
		unitsCurrent = Leveleditor.getUnitsCurrent(); 
		WebAction.isEqualUnits(unitsCurrent, TestData.getUnitsPlaced1PlayerTerrainSmall());	
	}
}
