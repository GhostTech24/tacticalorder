package test.tacticalOrder.view.leveleditor.saveAndLoad;

import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;

import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSaveAndLoadOfScenariosAfterRefresh{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSaveAndLoadOfScenariosAfterRefresh.class.getSimpleName()));
		
		final int entriySizeExpected = 2;
		
		final int entryIndexScenarioDefault = 0;
		final boolean isCheckboxVisibleScenarioDefaultExpected = false;
		final String scenarioNameScenarioDefaultExpected = "newScenario";
		
		final int entryIndexScenarioTwo = 1;
		final boolean isCheckboxVisibleScenarioTwoExpected = false;
		final String scenarioNameScenarioTwoExpected = "My Map";
				
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.deleteIndexedDb(WebAction.indexedDbName);
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		// save first scenario
		WebAction.click(SetScenarioDialog.getScenarioSaveButton());
		// build second scenario and save it
		WebAction.click(Leveleditor.getSetScenarioButton());
		WebAction.insertText(SetScenarioDialog.getScenarioNameInput(), scenarioNameScenarioTwoExpected, true);	
		WebAction.click(SetScenarioDialog.getScenarioSaveButton());
		
		// When
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// Then
		WebAction.isNumber(SetScenarioDialog.getScenarioEntries().size(), entriySizeExpected);
		WebAction.isEqualScenarioToLoad(SetScenarioDialog.getScenarioEntries(), entryIndexScenarioDefault, isCheckboxVisibleScenarioDefaultExpected, scenarioNameScenarioDefaultExpected);
		WebAction.isEqualScenarioToLoad(SetScenarioDialog.getScenarioEntries(), entryIndexScenarioTwo, isCheckboxVisibleScenarioTwoExpected, scenarioNameScenarioTwoExpected);
	}
}
