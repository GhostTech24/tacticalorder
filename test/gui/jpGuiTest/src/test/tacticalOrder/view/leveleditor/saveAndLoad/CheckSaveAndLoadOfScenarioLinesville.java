package test.tacticalOrder.view.leveleditor.saveAndLoad;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetPlayerDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetScenarioDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetSizeDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetTerrainDialog;
import objectRepository.tacticalOrder.dialog.leveleditor.SetUnitDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testData.TestData;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckSaveAndLoadOfScenarioLinesville{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckSaveAndLoadOfScenarioLinesville.class.getSimpleName()));
		
		final int entriySizeExpected = 1;
		
		final int entryIndex = 0;
		final boolean isCheckboxVisibleExpected = false;
		final String scenarioNameExpected = "Linesville";
		
		final int playerSizeExpected = 3;
		final String playerColorValue1Expected = "playerColorBlack";
		final String playerColorValue2Expected = "playerColorRed";
		final String playerColorValue3Expected = "playerColorYellow";
		
		final List<List<Map<String, String>>> terrainsExpected = TestData.getTerrainsLinesville();
		final List<List<Map<String, String>>> unitsExpected = TestData.getUnitsPlacedLinesville();	
		
		final String unitReachedFieldXValuePlayer2Expected = "X: 0";
		final String unitReachedFieldYValuePlayer2Expected = "Y: 0";
		final String unitReachedFieldXValuePlayer3Expected = "X: 4";
		final String unitReachedFieldYValuePlayer3Expected = "Y: 3";
		
		final Map<String, WebElement> terrainButtons;
		final List<List<WebElement>> terrainsCurrent, unitsCurrent;
		final int playerSizeCurrent;
		
		// Given
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.deleteIndexedDb(WebAction.indexedDbName);
		WebAction.click(MainMenu.getLevelEditorButton());
		// set player
		WebAction.click(Leveleditor.getSetPlayerButton());
		WebAction.click(SetPlayerDialog.getArrowUpPlayerButton());
		WebAction.click(SetPlayerDialog.getColorBevoreButton(1));
		WebAction.click(SetPlayerDialog.getColorBevoreButton(1)); // black
		WebAction.click(SetPlayerDialog.getColorBevoreButton(2)); // red
		WebAction.click(SetPlayerDialog.getColorNextButton(3)); // yellow	
		
		WebAction.click(SetPlayerDialog.getCloseDialogButton());			
		// set size X
		WebAction.click(Leveleditor.getSetSizeButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownXButton());
		WebAction.click(SetSizeDialog.getArrowDownXButton()); // to 8
		// set size y
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 15
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 10
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton());
		WebAction.click(SetSizeDialog.getArrowDownYButton()); // to 5
		
		WebAction.click(SetScenarioDialog.getCloseDialogButton());
		// set terrains - cities	
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		terrainButtons = SetTerrainDialog.getTerrainButtons();
		WebAction.click(terrainButtons.get("city"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(0).get(0));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(0));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(0));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(3).get(0));
		// set terrains - streets
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("street"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(0).get(4));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(4));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(4));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(3).get(4));
		// set terrains - woods
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("woods"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(4).get(1));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(5).get(1));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(6).get(1));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(7).get(1));
		// set terrains - sea
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("sea"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(0).get(2));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(2));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(2));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(3).get(2));
		// set terrains - mountains
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(terrainButtons.get("mountain"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(4).get(3));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(5).get(3));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(6).get(3));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(7).get(3));
		// set units player 1
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetTerrainDialog.getUnitSelectionButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitNextButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("infantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(4).get(3));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("heavyInfantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(0).get(0));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("lightTank"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(1).get(4));
		// set units player 2
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitNextButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("infantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(6).get(3));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("heavyInfantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(7).get(3));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("lightTank"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(2).get(0));
		// set units player 3
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getPlayerColorUnitNextButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("infantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(4).get(1));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("heavyInfantry"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(5).get(3));
		
		WebAction.click(Leveleditor.getSetUnitOrTerrainButton());
		WebAction.click(SetUnitDialog.getUnitButtons().get("lightTank"));
		WebAction.click(Leveleditor.getTerrainsCurrent().get(3).get(3));	
		// set losing conditions player 3
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		WebAction.click(SetLosingConditionDialog.getUnitReachedFieldCheckbox());
		WebAction.click(SetLosingConditionDialog.getUnitReachedFieldSelectFieldButton()); // still player 3 selected from set player dialog
		WebAction.click(Leveleditor.getUnitsCurrent().get(4).get(3));
		// set losing conditions player 2
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionBevoreButton());
		WebAction.click(SetLosingConditionDialog.getUnitReachedFieldCheckbox());
		
		WebAction.click(SetLosingConditionDialog.getCloseDialogButton());
		// set name
		WebAction.click(Leveleditor.getSetScenarioButton());
		WebAction.insertText(SetScenarioDialog.getScenarioNameInput(), scenarioNameExpected, true);
		// save
		WebAction.click(SetScenarioDialog.getScenarioSaveButton());
		// prepare for load
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetScenarioButton());
		
		// When
		WebAction.click(SetScenarioDialog.getScenarioEntries().get(0).get(0));
		WebAction.click(SetScenarioDialog.getScenarioLoadButton());		
		
		// Then
		terrainsCurrent = Leveleditor.getTerrainsCurrent();
		WebAction.isEqualTerrains(terrainsCurrent, terrainsExpected);
		
		unitsCurrent = Leveleditor.getUnitsCurrent();
		WebAction.isEqualUnits(unitsCurrent, unitsExpected);
		
		WebAction.click(Leveleditor.getSetPlayerButton());
		playerSizeCurrent = Integer.parseInt(SetPlayerDialog.getPlayerValueCurrent().getText());
		WebAction.isNumber(playerSizeCurrent, playerSizeExpected);
		WebAction.isText(SetPlayerDialog.getColorValue(1).getAttribute("class"), playerColorValue1Expected);
		WebAction.isText(SetPlayerDialog.getColorValue(2).getAttribute("class"), playerColorValue2Expected);
		WebAction.isText(SetPlayerDialog.getColorValue(3).getAttribute("class"), playerColorValue3Expected);
		WebAction.click(SetPlayerDialog.getCloseDialogButton());
		
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), false);
		
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), true);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldXCoordinateValue(), unitReachedFieldXValuePlayer2Expected);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldYCoordinateValue(), unitReachedFieldYValuePlayer2Expected);
		
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
		WebAction.isChecked(SetLosingConditionDialog.getUnitReachedFieldCheckbox(), true);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldXCoordinateValue(), unitReachedFieldXValuePlayer3Expected);
		WebAction.isText(SetLosingConditionDialog.getUnitReachedFieldYCoordinateValue(), unitReachedFieldYValuePlayer3Expected);
		WebAction.click(SetLosingConditionDialog.getCloseDialogButton());
		
		WebAction.click(Leveleditor.getSetScenarioButton());
		WebAction.isNumber(SetScenarioDialog.getScenarioEntries().size(), entriySizeExpected);
		WebAction.isEqualScenarioToLoad(SetScenarioDialog.getScenarioEntries(), entryIndex, isCheckboxVisibleExpected, scenarioNameExpected);
	}
}
