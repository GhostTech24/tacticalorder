package test.tacticalOrder.view.leveleditor.losingCondition;

import objectRepository.tacticalOrder.dialog.leveleditor.SetLosingConditionDialog;
import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckDeselectLosingConditionFirst1Condition{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckDeselectLosingConditionFirst1Condition.class.getSimpleName()));
				
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getSetLosingConditionButton());
		
		WebAction.click(SetLosingConditionDialog.getPlayerColorLosingConditionNextButton());
	
		// When
		WebAction.click(SetLosingConditionDialog.getAllUnitsLostCheckbox());
		
		//Then	
		WebAction.isChecked(SetLosingConditionDialog.getAllUnitsLostCheckbox(), true);
	}
}
