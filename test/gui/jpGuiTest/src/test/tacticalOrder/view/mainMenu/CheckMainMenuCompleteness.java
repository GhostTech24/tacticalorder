package test.tacticalOrder.view.mainMenu;

import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckMainMenuCompleteness{
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckMainMenuCompleteness.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		// When
		// -
		
		//Then
		WebAction.isVisible(MainMenu.getMainMenuView(), true);
		WebAction.isVisible(MainMenu.getLevelEditorButton(), true);
		WebAction.isVisible(MainMenu.getSettingsButton(), true);
		
		// reset
		// -
	}
}
