package test.tacticalOrder.view.mainMenu;

import objectRepository.tacticalOrder.view.Leveleditor;
import objectRepository.tacticalOrder.view.MainMenu;
import objectRepository.tacticalOrder.view.Settings;
import testEngine.TestController;
import testEngine.WebAction;

public final class CheckMainMenuNavigation{
		
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", CheckMainMenuNavigation.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		// When - Leveleditor
		WebAction.click(MainMenu.getLevelEditorButton());
		WebAction.click(Leveleditor.getBackButton());	
		// Then - Leveleditor
		WebAction.isVisible(MainMenu.getMainMenuView(), true);	
		// reset
		// -
		
		// When - Settings
		WebAction.click(MainMenu.getSettingsButton());
		WebAction.click(Settings.getBackButton());
		// Then - Settings
		WebAction.isVisible(MainMenu.getMainMenuView(), true);
		// reset
		// -
	}
}
