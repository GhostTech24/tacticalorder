package test.wikipedia;

import org.openqa.selenium.Keys;

import objectRepository.wikipedia.WikiRep;
import testEngine.TestController;
import testEngine.WebAction;

public class WikipediaDemo {


	public final static void execute() {	
		WebAction.navigateToUrl(TestController.getAppUrl());
		
		WebAction.waitForPageLoad();
		
		WebAction.wait(500);
		WikiRep.inputSearchbar().sendKeys("Lol");
		WebAction.wait(1000);
		WikiRep.inputSearchbar().clear();
		WebAction.wait(500);
		WikiRep.inputSearchbar().sendKeys("Test");
		WebAction.wait(1000);
		WikiRep.inputSearchbar().sendKeys(Keys.ENTER);
		WebAction.waitForPageLoad();
		WebAction.wait(500);
	}
}
