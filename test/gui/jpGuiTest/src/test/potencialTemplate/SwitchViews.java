package test.potencialTemplate;


import objectRepository.tacticalOrder.view.MainMenu;
import testEngine.TestController;
import testEngine.WebAction;

public class SwitchViews {
	
	public final static void execute() {
		System.out.println(String.format("Execute the test case |%s|.", SwitchViews.class.getSimpleName()));
		
		// Given
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
//		WebAction.isVisible(SAElement.getDropdownView(), true);
//		WebAction.isVisible(SAElement.getOverviewMenuEntry(), true);
//		WebAction.isVisible(SAElement.getStructureMenuEntry(), true);
//		WebAction.isVisible(SAElement.getPaidContrebutionMenuEntry(), true);
		
//		WebAction.click(SAElement.getOverviewMenuEntry());
		WebAction.isVisible(MainMenu.getMainMenuView(), true);
//		WebAction.isVisible(MainMenu.getStructureView(), false);
		//WebAction.isVisible(MainMenu.getPaidContrebutionView(), false);
		
//		WebAction.click(SAElement.getStructureMenuEntry());
		WebAction.isVisible(MainMenu.getMainMenuView(), false);
//		WebAction.isVisible(MainMenu.getStructureView(), true);
		//WebAction.isVisible(MainMenu.getPaidContrebutionView(), false);
		
//		WebAction.click(SAElement.getPaidContrebutionMenuEntry());
		WebAction.isVisible(MainMenu.getMainMenuView(), false);
//		WebAction.isVisible(MainMenu.getStructureView(), false);
		//WebAction.isVisible(MainMenu.getPaidContrebutionView(), true);
		
		//reset
//		WebAction.click(SAElement.getOverviewMenuEntry());
	}
	
}
