package testData;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public final class TestData {
	public final static List<List<Map<String, String>>> getTerrainsDefault() {
		final List<List<Map<String, String>>> terrainsDefault;
		
		final String tagName = "img";
		final String classValue = "terrainGraphicOnGameMap";
		final String defaultTerrainAttributeValue = "plane";
		
		terrainsDefault = Arrays.asList(
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 1
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 2
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 3
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 4
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 5
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 6
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 7
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 8
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 9
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 10
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 11
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 12
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 13
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 14
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 15
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 16
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 17
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 18
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				), // 19
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 1
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 5
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 10
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue), // 15
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue),
								getReplacementForTerrainElement(tagName, classValue, defaultTerrainAttributeValue)  // 20	
				) // 20
		);
		
		return terrainsDefault;
	}

	public final static List<List<Map<String, String>>> getTerrainsSmall() {
		final List<List<Map<String, String>>> terrainsSmall;
		
		final String tagName = "img";
		final String classValue = "terrainGraphicOnGameMap";
		
		terrainsSmall = Arrays.asList(
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "plane"), // 1
								getReplacementForTerrainElement(tagName, classValue, "city"),
								getReplacementForTerrainElement(tagName, classValue, "plane"),
								getReplacementForTerrainElement(tagName, classValue, "woods")  // 4
				), // 1
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "street"), // 1
								getReplacementForTerrainElement(tagName, classValue, "street"),
								getReplacementForTerrainElement(tagName, classValue, "street"),
								getReplacementForTerrainElement(tagName, classValue, "street")  // 4
				), // 2
				Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "woods"), // 1
								getReplacementForTerrainElement(tagName, classValue, "sea"),
								getReplacementForTerrainElement(tagName, classValue, "sea"),
								getReplacementForTerrainElement(tagName, classValue, "plane")  // 4
				)  // 3
				
		);
		
		return terrainsSmall;
	}
	
	/** Get the terrains for the test scenario "Linesville".
	 * @return terrains for the test scenario "Linesville". The return type work around the problem of creating new WebElement from scratch!
	 */
	public final static List<List<Map<String, String>>> getTerrainsLinesville() {
		final List<List<Map<String, String>>> terrains;
		
		final String tagName = "img";
		final String classValue = "terrainGraphicOnGameMap";
		
		terrains = Arrays.asList(
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "city"), // 0
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "sea"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "street")  // 4
					), // 0
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "city"), // 0
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "sea"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "street")  // 4
					), // 1
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "city"), // 0
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "sea"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "street")  // 4
					), // 2
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "city"), // 0
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "sea"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "street")  // 4
					), // 3
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "plane"), // 0
									getReplacementForTerrainElement(tagName, classValue, "woods"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "mountain"),
									getReplacementForTerrainElement(tagName, classValue, "plane")  // 4
					), // 4
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "plane"), // 0
									getReplacementForTerrainElement(tagName, classValue, "woods"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "mountain"),
									getReplacementForTerrainElement(tagName, classValue, "plane")  // 4
					), // 5
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "plane"), // 0
									getReplacementForTerrainElement(tagName, classValue, "woods"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "mountain"),
									getReplacementForTerrainElement(tagName, classValue, "plane")  // 4
					), // 6
					Arrays.asList(	getReplacementForTerrainElement(tagName, classValue, "plane"), // 0
									getReplacementForTerrainElement(tagName, classValue, "woods"),
									getReplacementForTerrainElement(tagName, classValue, "plane"),
									getReplacementForTerrainElement(tagName, classValue, "mountain"),
									getReplacementForTerrainElement(tagName, classValue, "plane")  // 4
					) // 7	
		);
		
		return terrains;
	}
	
	public final static List<List<Map<String, String>>> getUnitsPlaced1PlayerTerrainDefault() {
		final List<List<Map<String, String>>> terrainsDefault;
		
		final String tagName = "svg";
		final String classValue = "unitGraphicOnGameMap playerColorRed";
		
		terrainsDefault = Arrays.asList(
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 1
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 2
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								getReplacementForUnitElement(tagName, classValue, "infantry"), // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 3
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 4
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 5
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 6
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 7
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 8
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				),// 9
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								getReplacementForUnitElement(tagName, classValue, "lightTank"), // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 10
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 11
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 12
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 13
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 14
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 15
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 16
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 17
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 18
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				), // 19
				Arrays.asList(	null, // 1
								null,
								null,
								null,
								null, // 5
								null,
								null, 
								null,
								null, 
								null, // 10
								null, 
								null,
								null,
								null,
								null, // 15
								null,
								null, 
								null,
								null, 
								null  // 20
				) // 20		
		);
		
		return terrainsDefault;
	}
	
	public final static List<List<Map<String, String>>> getUnitsPlaced1PlayerTerrainSmall() {
		final List<List<Map<String, String>>> unitsPlaced;
		
		final String tagName = "svg";
		final String classValue = "unitGraphicOnGameMap playerColorRed";
		
		unitsPlaced = Arrays.asList(
				Arrays.asList(	null, // 1
								getReplacementForUnitElement(tagName, classValue, "infantry"),
								null,
								null  // 4
				), // 1
				Arrays.asList(	null, // 1
								null,
								getReplacementForUnitElement(tagName, classValue, "lightTank"),
								null  // 4
				), // 2
				Arrays.asList(	null, // 1
								null,
								null,
								null  // 4
				)  // 3
				
		);
		
		return unitsPlaced;
	}
	
	/** Get the empty unit placement for scenario terrain small.
	 * @return empty unit placement for scenario terrain small
	 */
	public final static List<List<Map<String, String>>> getUnitsEmptyTerrainSmall() {
		final List<List<Map<String, String>>> unitsPlaced;
		
		unitsPlaced = Arrays.asList(
				Arrays.asList(	null, // 1
								null,
								null,
								null  // 4
				), // 1
				Arrays.asList(	null, // 1
								null,
								null,
								null  // 4
				), // 2
				Arrays.asList(	null, // 1
								null,
								null,
								null  // 4
				)  // 3
				
		);
		
		return unitsPlaced;
	}

	/** Get the placed units for the test scenario "Linesville".
	 * @return placed units for the test scenario "Linesville". The return type work around the problem of creating new WebElement from scratch!
	 */
	public final static List<List<Map<String, String>>> getUnitsPlacedLinesville() {
		final List<List<Map<String, String>>> unitsPlaced;
		
		final String tagName = "svg";
		final String classValuePlayer1 = "unitGraphicOnGameMap playerColorBlack";
		final String classValuePlayer2 = "unitGraphicOnGameMap playerColorRed";
		final String classValuePlayer3 = "unitGraphicOnGameMap playerColorYellow";
		
//		getReplacementForUnitElement(tagName, classValuePlayer1, "infantry"),
		unitsPlaced = Arrays.asList(
				Arrays.asList(	getReplacementForUnitElement(tagName, classValuePlayer1, "heavyInfantry"), // 0
								null,
								null,
								null,
								null  // 4
				), // 0
				Arrays.asList(	null, // 0
								null,
								null,
								null,
								getReplacementForUnitElement(tagName, classValuePlayer1, "lightTank")  // 4
				), // 1
				Arrays.asList(	getReplacementForUnitElement(tagName, classValuePlayer2, "lightTank"), // 0
								null,
								null,
								null,
								null  // 4
				), // 2
				Arrays.asList(	null, // 0
								null,
								null,
								getReplacementForUnitElement(tagName, classValuePlayer3, "lightTank"),
								null  // 4
				), // 3
				Arrays.asList(	null, // 0
								getReplacementForUnitElement(tagName, classValuePlayer3, "infantry"),
								null,
								getReplacementForUnitElement(tagName, classValuePlayer1, "infantry"),
								null  // 4
				), // 4
				Arrays.asList(	null, // 0
								null,
								null,
								getReplacementForUnitElement(tagName, classValuePlayer3, "heavyInfantry"),
								null  // 4
				), // 5
				Arrays.asList(	null, // 0
								null,
								null,
								getReplacementForUnitElement(tagName, classValuePlayer2, "infantry"),
								null  // 4
				), // 6
				Arrays.asList(	null, // 0
								null,
								null,
								getReplacementForUnitElement(tagName, classValuePlayer2, "heavyInfantry"),
								null  // 4
				) // 7
		);
		
		return unitsPlaced;
	}
	
	private final static Map<String, String> getReplacementForTerrainElement(final String tagName, final String classValue, final String terrainAttributeValue) {
		Map<String, String> result = new TreeMap<String, String>();
		
		result.put("tagName", tagName);
		result.put("classValue", classValue);
		result.put("terrainAttributeValue", terrainAttributeValue);
		
		return result;
	}
	
	private final static Map<String, String> getReplacementForUnitElement(final String tagName, final String classValue, final String unitAttributeValue) {
		Map<String, String> result = new TreeMap<String, String>();
		
		result.put("tagName", tagName);
		result.put("classValue", classValue);
		result.put("unitAttributeValue", unitAttributeValue);
		
		return result;
	}
}
