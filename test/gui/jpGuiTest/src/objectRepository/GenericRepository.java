package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Abstraktes generisches Repository. Stellt als Vorlage vereinfachtes Handling von Drivern und Suchfunktionen bereit.
 */
public abstract class GenericRepository {

	private static WebDriver driver;
	
	/**
	 * Setter-Funktion. Setzte den aktuellen WebDriver fuer das Repository.
	 * @param driver der zu setzende WebDriver
	 */
	public final static void setWebDriver(final WebDriver driver) {
		GenericRepository.driver = driver;
	}
	
	/**
	 * Getter-Funktion. Gibt ein Element zurueck, welches ueber seine ID gefunden wird.
	 * @param id die ID des Elements
	 * @return das gefundene Element
	 */
	protected final static WebElement getElement(final String id) {
		final WebDriver driver = GenericRepository.driver;
		final WebElement element;
		
		try {
			element = driver.findElement(By.id(id));
		} catch(NoSuchElementException e) {
			return null;
		}
			
		return element;
	}
	
	/**
	 * Getter-Funktion. Gibt das ElternElement (parent) zurueck.
	 * @param childElement das element, von dem aus gesucht werden soll (child)
	 * @return das gefundene Element
	 */
	public final static WebElement getParentElement(final WebElement childElement) {	
		return childElement.findElement(By.xpath("./.."));
	}
}
