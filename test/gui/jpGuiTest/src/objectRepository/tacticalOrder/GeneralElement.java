package objectRepository.tacticalOrder;

import objectRepository.GenericRepository;
import testEngine.WebAction;

public class GeneralElement extends GenericRepository {
	public final static String getTabName() {
		final String javascript = "return document.title";
		
		return (String)WebAction.executeJavascript(javascript);
	}
}
