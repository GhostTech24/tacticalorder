package objectRepository.tacticalOrder.dialog.leveleditor;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import objectRepository.tacticalOrder.dialog.GenericDialog;

public class SetScenarioDialog extends GenericDialog {
	
	public final static WebElement getSetScenarioDialog() {
		final String id = "setScenarioDialog";
		
		return getElement(id);
	}
	
	public final static WebElement getSetScenarioDialogTitle() {
		final String id = "setScenarioDialogTitle";
		
		return getElement(id);
	}
		
	public final static WebElement getScenarioCurrentTitle() {
		final String id = "scenarioCurrentTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getScenarioNameInput() {
		final String id = "scenarioNameInput";
		
		return getElement(id);
	}
	
	public final static WebElement getScenarioSaveButton() {
		final String id = "scenarioSaveButton";
		
		return getElement(id);
	}
	
	public final static WebElement getScenarioResetButton() {
		final String id = "scenarioResetButton";
		
		return getElement(id);
	}

	public final static WebElement getScenariosTitle() {
		final String id = "scenariosTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getScenarioLoadButton() {
		final String id = "scenarioLoadButton";
		
		return getElement(id);
	}
	
	public final static WebElement getScenarioDeleteButton() {
		final String id = "scenarioDeleteButton";
		
		return getElement(id);
	}
	
	public final static WebElement getScenarioSelectionContainer() {
		final String id = "scenarioSelectionContainer";
		
		return getElement(id);
	}
	
	// dynamic Elements
	/** Get the entries of loadable scenarios.
	 * @return entries of loadable scenarios; every entry has a checkboxElement and a labelElement
	 */
	public final static List<List<WebElement>> getScenarioEntries() { // List<List<String, WebElement>>
		final List<List<WebElement>> result = new ArrayList<List<WebElement>>();
		
		final String entryContainersCssSelector = ".scenarioList";
		final List<WebElement> entryContainers = SetScenarioDialog.getScenarioSelectionContainer().findElements(By.cssSelector(entryContainersCssSelector));
		
		final String checkboxElementTagName = "input";
		final String labelElementTagName = "label";
		
		for(WebElement entryContainerCurrent: entryContainers) {
			final WebElement checkboxElement = entryContainerCurrent.findElements(By.tagName(checkboxElementTagName)).get(0);
			final WebElement labelElement = entryContainerCurrent.findElements(By.tagName(labelElementTagName)).get(0);
			
			final List<WebElement> entryElements = new ArrayList<WebElement>();
			entryElements.add(checkboxElement);
			entryElements.add(labelElement);
			
			result.add(entryElements);		
		}
		
		
		return result;
	}
	/*
	 *  
	 * 
	 * 
	 * scenarioSelectionContainer
	 * -> dynamic saves from 'scenarioSelectionContainer' by class .scenarioList
	 */
}
