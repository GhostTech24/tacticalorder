package objectRepository.tacticalOrder.dialog.leveleditor;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.GenericDialog;

public class SetSizeDialog extends GenericDialog {
	
	public final static WebElement getXSizeLetter() {
		final String id = "XSizeLetter";
		
		return getElement(id);
	}
	
	public final static WebElement getYSizeLetter() {
		final String id = "YSizeLetter";
		
		return getElement(id);
	}
	
	public final static WebElement getCurrentXsizeValue() {
		final String id = "currentXsizeValue";
		
		return getElement(id);
	}
	
	public final static WebElement getCurrentYsizeValue() {
		final String id = "currentYsizeValue";
		
		return getElement(id);
	}
	
	public final static WebElement getArrowUpXButton() {
		final String id = "arrowUpXButton";
		
		return getElement(id);
	}
	
	public final static WebElement getArrowUpYButton() {
		final String id = "arrowUpYButton";
		
		return getElement(id);
	}
	
	public final static WebElement getArrowDownXButton() {
		final String id = "arrowDownXButton";
		
		return getElement(id);
	}
	
	public final static WebElement getArrowDownYButton() {
		final String id = "arrowDownYButton";
		
		return getElement(id);
	}	
}
