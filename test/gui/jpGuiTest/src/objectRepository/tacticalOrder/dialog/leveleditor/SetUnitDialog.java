package objectRepository.tacticalOrder.dialog.leveleditor;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.GenericDialog;

public class SetUnitDialog extends GenericDialog {
	
	public final static WebElement getSetUnitDialog() {
		final String id = "setUnitDialog";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayerColorUnitBevoreButton() {
		final String id = "playerColorUnitBevoreButton";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayerColorUnitNextButton() {
		final String id = "playerColorUnitNextButton";
		
		return getElement(id);
	}
	
	public final static WebElement getTerrainSelectionButton() {
		final String id = "terrainSelectionButton";
		
		return getElement(id);
	}
		
	// structured Elements
	public final static Map<String, WebElement> getUnitButtons() {
		final Map<String, WebElement> result;
		final String classNameTerrainOrUnitSelectionGraphic = "terrainOrUnitSelectionGraphic";
		final List<WebElement> imgElements;
		final int imgElementsSize;
		final String attributeNameUnit = "unit";
		int i;
		String terrainTypeCurrent;
		WebElement imgElementCurrent, terrainButton;
		
		// execution / Ausfuehrung
		result = new TreeMap<String, WebElement>();
		
		imgElements = getSetUnitDialog().findElements(By.className(classNameTerrainOrUnitSelectionGraphic));
		imgElementsSize = imgElements.size();
		
		for(i = 0; i < imgElementsSize; i++) {
			imgElementCurrent = imgElements.get(i);
			terrainTypeCurrent = imgElementCurrent.getAttribute(attributeNameUnit);
			terrainButton = getParentElement(imgElementCurrent);
			
			result.put(terrainTypeCurrent, terrainButton);
		}
		
		return result;
	}
	
	
}
