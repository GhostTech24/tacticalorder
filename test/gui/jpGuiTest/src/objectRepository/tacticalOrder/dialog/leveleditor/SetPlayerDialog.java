package objectRepository.tacticalOrder.dialog.leveleditor;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.GenericDialog;

public class SetPlayerDialog extends GenericDialog {
	
	public final static WebElement getSetPlayerDialog() {
		final String id = "setPlayerDialog";
		
		return getElement(id);
	}
	
	public final static WebElement getSetPlayerDialogTitle() {
		final String id = "setPlayerDialogTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayersTitle() {
		final String id = "playersTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getArrowUpPlayerButton() {
		final String id = "arrowUpPlayerButton";
		
		return getElement(id);
	}
	
	public final static WebElement getArrowDownPlayerButton() {
		final String id = "arrowDownPlayerButton";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayerValueCurrent() {
		final String id = "playerValueCurrent";
		
		return getElement(id);
	}
	
		
	// structured Elements
	public final static WebElement getColorBevoreButton(int playerId) {
		final String id = "player" + playerId + "ColorBevoreButton";
	
		return getElement(id);
	}
	
	public final static WebElement getColorValue(int playerId) {
		final String id = "player" + playerId + "ColorValue";
		return getElement(id);
	}
	
	public final static WebElement getColorNextButton(int playerId) {
		final String id = "player" + playerId + "ColorNextButton";
		
		return getElement(id);
	}
	
	
}
