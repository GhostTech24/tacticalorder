package objectRepository.tacticalOrder.dialog.leveleditor;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.GenericDialog;

public class SetTerrainDialog extends GenericDialog {
	
	public final static WebElement getSetTerrainDialog() {
		final String id = "setTerrainDialog";
		
		return getElement(id);
	}
	
	public final static WebElement getUnitSelectionButton() {
		final String id = "unitSelectionButton";
		
		return getElement(id);
	}
		
	// structured Elements
	public final static Map<String, WebElement> getTerrainButtons() {
		final Map<String, WebElement> result;
		final String classNameTerrainOrUnitSelectionGraphic = "terrainOrUnitSelectionGraphic";
		final List<WebElement> imgElements;
		final int imgElementsSize;
		final String attributeNameTerrain = "terrain";
		int i;
		String terrainTypeCurrent;
		WebElement imgElementCurrent, terrainButton;
		
		// execution / Ausfuehrung
		result = new TreeMap<String, WebElement>();
		
		imgElements = getSetTerrainDialog().findElements(By.className(classNameTerrainOrUnitSelectionGraphic));
		imgElementsSize = imgElements.size();
		
		for(i = 0; i < imgElementsSize; i++) {
			imgElementCurrent = imgElements.get(i);
			terrainTypeCurrent = imgElementCurrent.getAttribute(attributeNameTerrain);
			terrainButton = getParentElement(imgElementCurrent);
			result.put(terrainTypeCurrent, terrainButton);
		}
		return result;
	}
	
	
}
