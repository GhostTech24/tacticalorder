package objectRepository.tacticalOrder.dialog.leveleditor;

import org.openqa.selenium.WebElement;

import objectRepository.tacticalOrder.dialog.GenericDialog;

public class SetLosingConditionDialog extends GenericDialog {
	
	// dialog specific
	public final static WebElement getSetLosingConditionDialog() {
		final String id = "setLosingConditionDialog";
		
		return getElement(id);
	}
	
	public final static WebElement getSetLosingConditionDialogTitle() {
		final String id = "setLosingConditionDialogTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayerColorLosingConditionBevoreButton() {
		final String id = "playerColorLosingConditionBevoreButton";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayerColorLosingConditionValue() {
		final String id = "playerColorLosingConditionValue";
		
		return getElement(id);
	}
	
	public final static WebElement getPlayerColorLosingConditionNextButton() {
		final String id = "playerColorLosingConditionNextButton";
		
		return getElement(id);
	}
	
	// allUnitsLost
	public final static WebElement getAllUnitsLostContainer() {
		final String id = "allUnitsLostContainer";
		
		return getElement(id);
	}
	
	public final static WebElement getAllUnitsLostContainerTitle() {
		final String id = "allUnitsLostContainerTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getAllUnitsLostCheckbox() {
		final String id = "allUnitsLostCheckbox";
		
		return getElement(id);
	}
	
	// unitReachedField
	public final static WebElement getUnitReachedFieldContainer() {
		final String id = "unitReachedFieldContainer";
		
		return getElement(id);
	}
	
	public final static WebElement getUnitReachedFieldContainerTitle() {
		final String id = "unitReachedFieldContainerTitle";
		
		return getElement(id);
	}
	
	public final static WebElement getUnitReachedFieldCheckbox() {
		final String id = "unitReachedFieldCheckbox";
		
		return getElement(id);
	}
	
	public final static WebElement getUnitReachedFieldXCoordinateValue() {
		final String id = "unitReachedFieldXCoordinateValue";
		
		return getElement(id);
	}
	
	public final static WebElement getUnitReachedFieldYCoordinateValue() {
		final String id = "unitReachedFieldYCoordinateValue";
		
		return getElement(id);
	}
	
	public final static WebElement getUnitReachedFieldSelectFieldButton() {
		final String id = "unitReachedFieldSelectFieldButton";
		
		return getElement(id);
	}
}
