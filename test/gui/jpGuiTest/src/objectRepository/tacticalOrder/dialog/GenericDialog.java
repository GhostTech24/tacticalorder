package objectRepository.tacticalOrder.dialog;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import objectRepository.GenericRepository;

public abstract class GenericDialog extends GenericRepository {
	
	
	public final static WebElement getOverlayElement() {
		final String id = "overlay";
		
		return getElement(id);
	}
	
	public final static WebElement getDialogContainer() {
		final String id = "dialogContainer";
		
		return getElement(id);
	}
	
	public final static WebElement getCloseDialogButton() {
		final String id = "closeDialogButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSetUnitOrTerrainButton() {
		final String id = "setUnitOrTerrainButton";
		
		return getElement(id);
	}
	
	// interactive Elements
	public final static WebElement getActiveDialog() {
		final WebElement result;
		final String classNameActiveDialog = "activeDialog";
		final List<WebElement> elementsFound;
		
		// execution / Ausfuehrung
		elementsFound = getDialogContainer().findElements(By.className(classNameActiveDialog));
		result = elementsFound.get(0);
		
		return result;
	}
	
	
}
