package objectRepository.tacticalOrder.view;

import org.openqa.selenium.WebElement;


import objectRepository.GenericRepository;

public class MainMenu extends GenericRepository {
	
	
	public final static WebElement getMainMenuView() {
		final String id = "mainMenu";
		
		return getElement(id);
	}
	
	public final static WebElement getLevelEditorButton() {
		final String id = "levelEditorButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSettingsButton() {
		final String id = "settingsButton";
		
		return getElement(id);
	}
}
