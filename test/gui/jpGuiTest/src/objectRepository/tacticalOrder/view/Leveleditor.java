package objectRepository.tacticalOrder.view;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import objectRepository.GenericRepository;

public class Leveleditor extends GenericRepository {
	
	
	public final static WebElement getLeveleditorView() {
		final String id = "leveleditorMenu";
		
		return getElement(id);
	}
	
	public final static WebElement getGameMapElement() {
		final String id = "gameMap";
		
		return getElement(id);
	}
	
	public final static WebElement getBackButton() {
		final String id = "backLeveleditorToMainMenuButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSetUnitOrTerrainButton() {
		final String id = "setUnitOrTerrainButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSetSizeButton() {
		final String id = "setSizeButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSetPlayerButton() {
		final String id = "setPlayerButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSetLosingConditionButton() {
		final String id = "setLosingConditionButton";
		
		return getElement(id);
	}
	
	public final static WebElement getSetScenarioButton() {
		final String id = "setScenarioButton";
		
		return getElement(id);
	}
	
	// structured Elements
	public final static List<List<WebElement>> getTerrainsCurrent() {
		final List<List<WebElement>> result;
		final List<List<List<WebElement>>> gameMap;
		final int gameMapXSize, gameMapYSize;
		List<WebElement> yResult;
		int x, y;
		WebElement terrainCurrent;
		
		// execution / Ausfuehrung
		gameMap = getGameMapInvertedCurrent();
		gameMapXSize = gameMap.get(0).size();
		gameMapYSize = gameMap.size();
		
		result = new ArrayList<List<WebElement>>(); // xCoordinates
		for(x = 0; x < gameMapXSize; x++) {
			yResult = new ArrayList<WebElement>();
			for(y = 0; y < gameMapYSize; y++) {
				terrainCurrent = gameMap.get(y).get(x).get(0);
				yResult.add(terrainCurrent);
			}
			result.add(yResult);
		}
		
		
		return result;
	}
	
	public final static List<List<WebElement>> getUnitsCurrent() {
		final List<List<WebElement>> result;
		final List<List<List<WebElement>>> gameMapInverted;
		final int unitsXSize, unitsYSize;
		List<WebElement> yResult, imgElementsCurrent;
		int x, y, imgElementsSize;
		boolean isContainingUnit;
		WebElement unitCurrent;
		
		// execution / Ausfuehrung
		gameMapInverted = getGameMapInvertedCurrent();
		unitsXSize = gameMapInverted.get(0).size();
		unitsYSize = gameMapInverted.size();
		
		result = new ArrayList<List<WebElement>>(); // xCoordinates
		for(x = 0; x < unitsXSize; x++) {
			yResult = new ArrayList<WebElement>();
			for(y = 0; y < unitsYSize; y++) {
				imgElementsCurrent  = gameMapInverted.get(y).get(x);
				imgElementsSize = imgElementsCurrent.size();
				isContainingUnit = (imgElementsSize == 2);
				
				if(isContainingUnit) {
					unitCurrent = imgElementsCurrent.get(1);
				} else {
					unitCurrent = null;
				}

				yResult.add(unitCurrent);
			}
			result.add(yResult);
		}
		
		
		return result;
	}
	
	private final static List<List<List<WebElement>>> getGameMapInvertedCurrent() {
		final List<List<List<WebElement>>> result;
		final WebElement gameMapElement;
		final List<WebElement> yFields;
		final int yFieldsSize;
		
		// construct
		result = new ArrayList<List<List<WebElement>>>();
		
		gameMapElement = Leveleditor.getGameMapElement();
		yFields = gameMapElement.findElements(By.tagName("tr"));
		yFieldsSize = yFields.size();
		
		for(int y = 0; y < yFieldsSize; y++) {
			final List<WebElement> xFields = yFields.get(y).findElements(By.tagName("td"));
			final int xFieldsSize = xFields.size();
			
			final List<List<WebElement>> xResult = new ArrayList<List<WebElement>>();
			for(int x = 0; x < xFieldsSize; x++) {
//				final List<WebElement> currentImgElements = xFields.get(x).findElements(By.tagName("svg")); // 0 = terrain; 1 = unit
//				xResult.add(currentImgElements); TODO exchange after terrain picture upgrade
				
				WebElement elementCurrent;
				final List<WebElement> pictureElements = new ArrayList<WebElement>();
				elementCurrent = xFields.get(x).findElements(By.tagName("img")).get(0);
				pictureElements.add(elementCurrent);
				
				final List<WebElement> svgElements = xFields.get(x).findElements(By.tagName("svg"));
				if(svgElements.size() != 0) {
					elementCurrent = xFields.get(x).findElements(By.tagName("svg")).get(0);
					pictureElements.add(elementCurrent);
				}		
		
				xResult.add(pictureElements);
			}
			
			result.add(xResult);
		}
		
		return result;
	}
	
	
}
