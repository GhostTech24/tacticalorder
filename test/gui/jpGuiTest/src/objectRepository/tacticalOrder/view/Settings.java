package objectRepository.tacticalOrder.view;

import org.openqa.selenium.WebElement;


import objectRepository.GenericRepository;

public class Settings extends GenericRepository {
	
	
	public final static WebElement getSettingsView() {
		final String id = "settingsMenu";
		
		return getElement(id);
	}
	
	public final static WebElement getBackButton() {
		final String id = "backSettingsToMainMenuButton";
		
		return getElement(id);
	}
}
