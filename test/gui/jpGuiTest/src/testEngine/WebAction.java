package testEngine;

import java.util.List;
import java.util.Map;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import testEngine.enums.JsSelector;

/**
 * eng: Action class. Provides general test methods like web interactions and checks.
 * ger: Aktionsklasse. Stellt allgemeine Test-Methoden wie Web-Interaktionen und Pr�fungen zur verfuegeung.
 */
public final class WebAction {
	private static WebDriver currentDriver;
	private static boolean isDebugMode = false;
	
	// specific values
	public final static String indexedDbName = "tacticalOrderDb";
	
	// general interaction
	public final static void setDriver(final WebDriver driver) {
		currentDriver = driver;
	}
	public final static void setDebugMode(final boolean isDebugMode) {
		WebAction.isDebugMode = isDebugMode;
	}
	
	public final static void click(final WebElement element) {
		if(element.isDisplayed()) {
			element.click();
			WebAction.wait(100); // give async functions time to operate 
			if(isDebugMode) {
				System.out.println(String.format("It was clicked on the element |%s|", element.toString()));
			}	
		} else {
			System.err.println(String.format("It was not clicked on the element |%s|, because it's not visible!", element.toString()));
		}
	}
	
	public final static void navigateToUrl(final String url) {
			if(isDebugMode) {
				System.out.println(String.format("It was navigated to the url: |%s|", url));		
			}
			
			currentDriver.navigate().to(url); // working example: "https://www.google.de"
	//		driver.get(url); // get = navigateTO and WaitForPageload
			
			try {
				Thread.sleep(500); // Give browser time to init the change; so next actions like waitForPageload can react correctly
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	
	public final static void deleteIndexedDb(String... dbNames) {
		final String javascript = "indexedDB.deleteDatabase(arguments[0])";
		final int lastIndex = dbNames.length - 1;
		
		String formatedDbNames = "";
		
		for(String currentDb: dbNames) {
			executeJavascript(javascript, currentDb);
			
			if(!dbNames[lastIndex].equals(currentDb)) {
				formatedDbNames = formatedDbNames + ", ";
			}		
			formatedDbNames = formatedDbNames + currentDb;
		}
				
		if(isDebugMode) {
			System.out.println(String.format("Following indexDBs were deleted (and offset with a refresh): |%s|", formatedDbNames));
		}	
	}
	
	/**
	 * eng: Wait function. Wait a certain time before program continues. <br>
	 * Primarily only suitable for debugging purposes! It is always better to use conditioned wait methods. <br>
	 * ger: Warte-Funktion. Warte eine bestimmte Zeit, bevor Programm fortgesetzt wird. <br>
	 * Primaer nur fuer Debugging-Zwecke geeignet! Es ist immer besser, konditionierte Warte-Methoden zu verwenden.
	 * @param milliseconds
	 */
	public final static void wait(final int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(isDebugMode) {
			System.out.println(String.format("The text execution waited |%s| milliseconds.", milliseconds));
		}	
	}
	
	public final static void waitForPageLoad() {
		final long toleratedWaitingTime = 3000;
		
		if(!isPageLoaded(toleratedWaitingTime)) {
			System.err.println(String.format("Waiting for pageload failed! The tolerated waiting time of |%s| milliseconds was exceeded!", toleratedWaitingTime));		
			return;
		}
		
		if(isDebugMode) {
			System.out.println("Waiting for pageload was successful.");
		}
	}
	
	/**
	 * TODO check if it really works
	 * @param elementAsStartPoint
	 * @param pixels
	 */
	public final static void swipeLeft(final WebElement elementAsStartPoint, final int pixels) {
		Actions action = new Actions(currentDriver);
		action.dragAndDropBy(elementAsStartPoint, 200, 0).build().perform();
	}
	
	@SuppressWarnings("unchecked")
	public final static void scroll(final JsSelector findBy, final String searchValue, final int xAxisPixels, final int yAxisPixels) {
		final String byId = "getElementById";
		final String byClass = "getElementsByClassName";
		final String byName = "getElementsByName";
		final String byTag = "getElementsByTagName";
		
		final long isUniqueValue = 1;
		
		final String currentSearchMethod;
		final String searchJavascriptWithArgs = "return document.%s('%s').length"; // can't put String as function-name -> TypeError
		final String searchJavascriptScrollElementsWithArgs = "return document.%s('%s')"; // can't put String as function-name -> TypeError
		final Object searchResult;
		final List<WebElement> searchResultList;
		final long resultValue;
		final WebElement scrollElement;
		
		
		
		if(xAxisPixels == 0 && yAxisPixels == 0) {
			System.err.println(String.format("Es wurde nicht gescrollt, da keine der Werte eine Ver�nderung bewirken kann war. Wert: (X: |%s|, Y: |%s|", xAxisPixels, yAxisPixels));
			return;
		}
		
		switch(findBy) {
			case ID:
				currentSearchMethod = byId;
			break;
			
			case CLASS:
				currentSearchMethod = byClass;
			break;
			
			case NAME:
				currentSearchMethod = byName;
			break;
			
			case TAG:
				currentSearchMethod = byTag;
			break;
			
			default:
				System.err.println(String.format("Es wurde nicht gescrollt, da ein ung�ltiger Suchselektor verwendet wurde. Verwendet wurde: |%s|", findBy));	
				return;
		}
		
		searchResult = executeJavascript(String.format(searchJavascriptWithArgs, currentSearchMethod, searchValue));
		
		if(findBy.equals(JsSelector.ID)) { // null or webelement
			
			if(searchResult == null) {
				System.err.println(String.format("Es wurde nicht gescrollt, da mit den Suchkriterien ein falsches Objekt gefunden wurde. Gesuchte wurde mit |%s| , aber es war 'null'.", searchJavascriptWithArgs));
				return;
			}
			
			scrollElement = (WebElement) searchResult;
			
		} else { // List<WebElement> from tag, class or name
			if(!(searchResult instanceof Long)) {
				System.err.println(String.format("Es wurde nicht gescrollt, da mit den Suchkriterien ein falsches Objekt gefunden wurde. Gesuchte wurde mit |%s| nach einem 'Long', aber es war |%s|.", searchJavascriptWithArgs, searchResult.getClass().toString()));
				return;
			}
			
			resultValue = (long) searchResult;
			
			if(isUniqueValue != resultValue) {
				System.err.println(String.format("Es wurde nicht gescrollt, da mit den Suchkriterien kein eindeutiges Ergebnis gefunden wurde. Gesuchte wurde via |%s|, gefunden wurden |%s| Ergebnise.", searchJavascriptWithArgs, resultValue));	
				return;
			}
			
			searchResultList = (List<WebElement>) executeJavascript(String.format(searchJavascriptScrollElementsWithArgs, currentSearchMethod, searchValue));
			scrollElement = searchResultList.get(0);			
		}

		if(!isScrollableElement(scrollElement)) {
			System.err.println(String.format("Es wurde nicht gescrollt, da das Element |%s| nicht gescrollt werden kann!", scrollElement));
			return;
		}
		
		scroll(scrollElement, xAxisPixels, yAxisPixels);
		
		System.out.println(String.format("Das Element |%s| wurde gescrollt um: (X:%s) (Y:%s)", scrollElement, xAxisPixels, yAxisPixels));
	}
	
	
	public final static Object executeJavascript(final String javascript, final Object... args) {
		final JavascriptExecutor jsE;
		final Object result;
		
		jsE = (JavascriptExecutor) currentDriver;  
		result = jsE.executeScript(javascript, args);
		
		if(isDebugMode) {
			System.out.println(String.format("The javascript |%s| with the arguments |%s| was executed. Return value is: |%s|", javascript, args, result));
		}
		
		return result;
	}

	public final static void insertText(final WebElement inputElement, final String text, final boolean isOverwritting) {
		if(isOverwritting) {
			inputElement.clear();
		}
		inputElement.sendKeys(text);
	}
	
	// checks
	public final static void isChecked(final WebElement element, final boolean shouldChecked) {
		final boolean isChecked;
		
		isChecked = element.isSelected();
		
		if(shouldChecked == isChecked) {
			System.out.println(String.format("The check status of the element |%s| is as expected. Check status is:  |%s|", element, shouldChecked));
		} else {
			System.err.println(String.format("The check status of the element |%s| is not as expected! Check status should be: |%s|, but was |%s|", element, shouldChecked, isChecked));
		}
	}
	
	public final static void isVisible(final WebElement element, final boolean shouldVisible) {
		final boolean isVisible;		
		
		if(element == null) {
			isVisible = false;
		}else {
			isVisible = element.isDisplayed();		
		}
		
		if(shouldVisible == isVisible) {
			System.out.println(String.format("The visability of the element |%s| is as expected. Visability is: |%s|", element, shouldVisible));
		} else {
			System.err.println(String.format("The visability of the element |%s| is not as expected! Visability should be: |%s|, but was |%s|", element, shouldVisible, isVisible));
		}
	}
	
	/** Validator. Checks a given element for an expected text. Can handle every classic tagged element (input elements included).
	 * @param element the element to check
	 * @param expectedText the expected text
	 */
	public final static void isText(final WebElement element, final String expectedText) {
		String currentText;
		boolean isFounded;
		currentText = element.getText();
		
		isFounded = (currentText.length() != 0);	
		if(!isFounded) {
			currentText = element.getAttribute("value"); // for input elements
		}
		
		if(expectedText.equals(currentText)) {
			System.out.println(String.format("The text of the element |%s| is as expected. Text is:  |%s|", element, expectedText));
		} else {
			System.err.println(String.format("The text of the element |%s| is not as expected! Text should be: |%s|, but was |%s|", element, expectedText, currentText));
		}
	}
	
	public final static void isText(final String currentText, final String expectedText) {	
		if(expectedText.equals(currentText)) {
			System.out.println(String.format("The text is as expected. Text is: |%s|", expectedText));
		} else {
			System.err.println(String.format("The text is not as expected! Text should be: |%s|, but was |%s|", expectedText, currentText));
		}
	}
	
	public final static void isNumber(final int numberCurrent, final int numberExpected) {	
		if(numberExpected == numberCurrent) {
			System.out.println(String.format("The number is as expected. Number is: |%s|", numberExpected));
		} else {
			System.err.println(String.format("The number is not as expected! Number should be: |%s|, but was: |%s|", numberExpected, numberCurrent));
		}
	}
	
	public final static boolean isFullscreen(final boolean shouldBeFullscreen) {
		final boolean isFullscreen;
		final String javascript = "return document.fullscreen";
		
		isFullscreen = (boolean) executeJavascript(javascript);
		
		if(shouldBeFullscreen == isFullscreen) {
			System.out.println(String.format("The fullscreen mode is as expected. Fullscreen mode is: |%s|", shouldBeFullscreen));
		} else {
			System.err.println(String.format("The fullscreen mode is not as expected! Fullscreen should be: |%s|, but was |%s|", shouldBeFullscreen, isFullscreen));
		}
		
		return isFullscreen;
	}
	
	public final static boolean isScrollableElement(final WebElement element) {
		final String javascriptCssOverflowValue = "return (getComputedStyle(arguments[0]).overflow)";
		final String cssScrollString = "scroll";
		final Object cssOverflowValueObject;
		final String cssOverflowValue;
		
		cssOverflowValueObject = executeJavascript(javascriptCssOverflowValue, element);
		cssOverflowValue = cssOverflowValueObject.toString();
		
		if(cssOverflowValue.contains(cssScrollString)) {
			return true;
		} else {
			return false;
		}
		// TODO check if reporting is needed in this function.
	}
	

	// specific  interaction
	public final static void isEqualTerrains(final List<List<WebElement>> terrainsCurrent, final List<List<Map<String, String>>> terrainsExpected) {
		final int currentXSize, expectedXSize, currentYSize, expectedYSize;
		final String attributNameTerrain = "terrain";
		String currentTag, expectedTag, currentClassValue, expectedClassValue, currentTerrainAttribute, expectedTerrainAttribute;
		
		currentXSize = terrainsCurrent.size();
		expectedXSize = terrainsExpected.size();
		if(expectedXSize != currentXSize) {
			System.err.println(String.format("The X size of the terrains is not as expected! Expected X size should be: |%s|, but was |%s|", expectedXSize, currentXSize));
			return;
		}
		
		currentYSize = terrainsCurrent.get(0).size();
		expectedYSize = terrainsExpected.get(0).size();
		if(expectedYSize != currentYSize) {
			System.err.println(String.format("The Y size of the terrains is not as expected! Expected Y size should be: |%s|, but was |%s|", expectedYSize, currentYSize));
			return;
		}
		
		for(int x = 0; x < expectedXSize; x++){
			for(int y = 0; y < expectedYSize; y++){
				
				expectedTag = terrainsExpected.get(x).get(y).get("tagName");
				currentTag =  terrainsCurrent.get(x).get(y).getTagName();	
				if(!expectedTag.equals(currentTag)) {
					System.err.println(String.format("The tag at terrain position (%s/%s) is not as expected! Expected tag should be: |%s|, but was |%s|", x, y, expectedTag, currentTag));
					return;
				}
				
				expectedClassValue = terrainsExpected.get(x).get(y).get("classValue");
				currentClassValue =  terrainsCurrent.get(x).get(y).getAttribute("class");
				if(!expectedClassValue.equals(currentClassValue)) {
					System.err.println(String.format("The class attribute at terrain position (%s/%s) is not as expected! Expected class attribute should be: |%s|, but was |%s|", x, y, expectedClassValue, currentClassValue));
					return;
				}
				
				expectedTerrainAttribute = terrainsExpected.get(x).get(y).get("terrainAttributeValue");
				currentTerrainAttribute =  terrainsCurrent.get(x).get(y).getAttribute(attributNameTerrain);
				if(!expectedTerrainAttribute.equals(currentTerrainAttribute)) {
					System.err.println(String.format("The terrain attribute at terrain position (%s/%s) is not as expected! Expected terrain attribute should be: |%s|, but was |%s|", x, y, expectedTerrainAttribute, currentTerrainAttribute));
					return;
				}
			}
		}
		
		System.out.println(String.format("The terrains is as expected. Expected x size |%s|, y size |%s| and all tags, class and |%s| attributes are equal.", expectedXSize, expectedYSize, attributNameTerrain));
	}
	
	public final static void isEqualUnits(final List<List<WebElement>> unitsCurrent, final List<List<Map<String, String>>> unitsExpected) {
		final int currentXSize, expectedXSize, currentYSize, expectedYSize;
		final String attributNameUnit = "unit";
		Map<String, String> unitExpected;
		WebElement unitCurrent;
		boolean isNoUnitExpected, isNoUnitCurrent;
		String currentTag, expectedTag, currentClassValue, expectedClassValue, currentTerrainAttribute, expectedTerrainAttribute;
		
		currentXSize = unitsCurrent.size();
		expectedXSize = unitsExpected.size();
		if(expectedXSize != currentXSize) {
			System.err.println(String.format("The X size of the units is not as expected! Expected X size should be: |%s|, but was |%s|", expectedXSize, currentXSize));
			return;
		}
		
		currentYSize = unitsCurrent.get(0).size();
		expectedYSize = unitsExpected.get(0).size();
		if(expectedYSize != currentYSize) {
			System.err.println(String.format("The Y size of the units is not as expected! Expected Y size should be: |%s|, but was |%s|", expectedYSize, currentYSize));
			return;
		}
		
		for(int x = 0; x < expectedXSize; x++){
			for(int y = 0; y < expectedYSize; y++){
				unitExpected = unitsExpected.get(x).get(y);
				unitCurrent = unitsCurrent.get(x).get(y);
				isNoUnitExpected = (unitExpected == null);
				isNoUnitCurrent = (unitCurrent == null);
				
				if(isNoUnitExpected && isNoUnitCurrent) { // both null
					continue;
				}
				if(isNoUnitExpected != isNoUnitCurrent) { // one null, one element
					System.err.println(String.format("The unit at position (%s/%s) is not as expected! Expected unit should be: |%s|, but was |%s|", x, y, unitExpected, unitCurrent));
					return;
				}
				
				// from this point current and expected unit exist
				expectedTag = unitExpected.get("tagName");
				currentTag =  unitCurrent.getTagName();	
				if(!expectedTag.equals(currentTag)) {
					System.err.println(String.format("The tag at units position (%s/%s) is not as expected! Expected tag should be: |%s|, but was |%s|", x, y, expectedTag, currentTag));
					return;
				}
				
				expectedClassValue = unitExpected.get("classValue");
				currentClassValue =  unitCurrent.getAttribute("class");
				if(!expectedClassValue.equals(currentClassValue)) {
					System.err.println(String.format("The class attribute at units position (%s/%s) is not as expected! Expected class attribute is |%s|, but was |%s|", x, y, expectedClassValue, currentClassValue));
					return;
				}
				
				expectedTerrainAttribute = unitExpected.get("unitAttributeValue");
				currentTerrainAttribute =  unitCurrent.getAttribute(attributNameUnit);
				if(!expectedTerrainAttribute.equals(currentTerrainAttribute)) {
					System.err.println(String.format("The unit attribute at units position (%s/%s) does not meet the expectations! Expected terrain attribute is |%s| but was |%s|", x, y, expectedTerrainAttribute, currentTerrainAttribute));
					return;
				}
			}
		}
		
		System.out.println(String.format("The terrains meet the expectation. Expected x size |%s|, y size |%s| and all tags, class and |%s| attributes are equal.", expectedXSize, expectedYSize, attributNameUnit));
	}
	
	public final static void isEqualScenarioToLoad(final List<List<WebElement>> scenariosCurrent, final int entryIndex, final boolean shouldBeChecked, final String scenarioNameExpected) {
		final List<WebElement> entry = scenariosCurrent.get(entryIndex);
		final WebElement checkboxElement = entry.get(0);
		final WebElement labelElement = entry.get(1);
		
		final boolean isChecked = checkboxElement.isSelected();
		final boolean isSelectionExpected = (shouldBeChecked == isChecked);
		
		final String scenarioNameCurrent = labelElement.getText();
		final boolean isScenarioName = (scenarioNameExpected.equals(scenarioNameCurrent));
		
		if(isSelectionExpected) {
			System.out.println(String.format("The scenario to load check status is as expected. Scenario with index |%s| check status is |%s|.", entryIndex, shouldBeChecked));
		} else {
			System.err.println(String.format("The scenario to load check status is not as expected! Scenario with index |%s| check status should be |%s|, but was |%s|.", entryIndex, shouldBeChecked, isChecked));
		}
		
		if(isScenarioName) {
			System.out.println(String.format("The scenario to load name is as expected. Scenario with index |%s| name is |%s|.", entryIndex, scenarioNameExpected));
		} else {
			System.err.println(String.format("The scenario to load name is not as expected! Scenario with index |%s| name  should be |%s|, but had |%s|.", entryIndex, scenarioNameExpected, scenarioNameCurrent));
		}
	}
	
	// class intern	
	private final static boolean isPageLoaded(final long toleratedWaitingTime) {	
		final long askIntervall = 1000;
		long currentWaitedTime = 0;
		String returnValue;
		final String js = "return document.readyState;";
		
		
		while(currentWaitedTime<= toleratedWaitingTime) {		
			returnValue = executeJavascript(js).toString();		
			
			try { // wait first to give a open site time to change after key "return"
				Thread.sleep(askIntervall);
				currentWaitedTime =+ askIntervall;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if(returnValue.equals("complete")) {
				return true;
			}
			
		}
		
		return false;
	}
	
	/**
	 * Scroll-Funktion. Scrollt auf dem uebergebenen Element. <br><br>
	 * 
	 * @param element
	 * @param xAxis
	 * @param yAxis
	 */
	private final static void scroll(final WebElement element, final int xAxis, final int yAxis)   {
		final String scrollJavascript;
		
		scrollJavascript = "arguments[0]"  + String.format(".scrollBy(%s, %s)", xAxis, yAxis);
			
		executeJavascript(scrollJavascript, element);
	}

}
