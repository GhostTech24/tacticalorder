package main;

import test.tacticalOrder.dialog.CheckSetLosingConditionDialogCompleteness;
import test.tacticalOrder.dialog.CheckSetPlayerDialogCompleteness;
import test.tacticalOrder.dialog.CheckSetScenarioDialogCompleteness;
import test.tacticalOrder.general.CheckAppName;
import test.tacticalOrder.view.leveleditor.gameMapSize.CheckInvalidSizeShrinkGameMapXY;
import test.tacticalOrder.view.leveleditor.gameMapSize.CheckSetSizeGameMapXY;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckAddLosingConditionNew;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckDeselectLosingConditionFirst1Condition;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckDeselectLosingConditionFirst2Conditions;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckDeselectLosingConditionSecond1Condition;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckDeselectLosingConditionSecond2Conditions;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckGetLosingConditionDefault;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckUpdateLosingConditionUnitReachedField2Conditions;
import test.tacticalOrder.view.leveleditor.losingCondition.CheckUpdateLosingConditionUnitReachedFieldOnly;
import test.tacticalOrder.view.leveleditor.player.CheckInvalidAddPlayer;
import test.tacticalOrder.view.leveleditor.player.CheckInvalidDeletePlayer;
import test.tacticalOrder.view.leveleditor.player.CheckValidAddPlayer;
import test.tacticalOrder.view.leveleditor.player.CheckValidDeletePlayer;
import test.tacticalOrder.view.leveleditor.playerColor.CheckInvalidColorChangeBevorePlayerMax;
import test.tacticalOrder.view.leveleditor.playerColor.CheckInvalidColorChangeNextPlayerMax;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeBevore0FreeBevore0FreeAfter2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeBevore0FreeBevore3Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeBevore1FreeBevore0FreeAfter2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeBevore1FreeBevore1FreeAfter2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeBevoreFirstColor2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeBevoreSecondColor2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeNext0FreeBevore0FreeAfter2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeNext0FreeAfter3Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeNext0FreeBevore1FreeAfter2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeNext1FreeBevore2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeNextLastColor2Player;
import test.tacticalOrder.view.leveleditor.playerColor.CheckValidColorChangeNextSecondToLastColor2Player;
import test.tacticalOrder.view.leveleditor.playerColorUnit.CheckUnitColorBevoreColorChange;
import test.tacticalOrder.view.leveleditor.saveAndLoad.CheckSaveAndLoadOfScenarioAfterRefresh;
import test.tacticalOrder.view.leveleditor.saveAndLoad.CheckSaveAndLoadOfScenarioBevoreRefresh;
import test.tacticalOrder.view.leveleditor.saveAndLoad.CheckSaveAndLoadOfScenarioLinesville;
import test.tacticalOrder.view.leveleditor.saveAndLoad.CheckSaveAndLoadOfScenarioNukeTown;
import test.tacticalOrder.view.leveleditor.saveAndLoad.CheckSaveAndLoadOfScenariosAfterRefresh;
import test.tacticalOrder.view.leveleditor.saveAndLoad.CheckSaveAndLoadOfScenariosBevoreRefresh;
import test.tacticalOrder.view.leveleditor.scenario.CheckGetScenarioName;
import test.tacticalOrder.view.leveleditor.scenario.CheckSetScenarioNameAfterDialogClose;
import test.tacticalOrder.view.leveleditor.scenario.CheckSetScenarioNameBevoreDialogClose;
import test.tacticalOrder.view.leveleditor.playerColorUnit.CheckUnitColorAfterColorChange;
import test.tacticalOrder.view.leveleditor.terrain.CheckSetAndGetTerrainSmall;
import test.tacticalOrder.view.leveleditor.unit.CheckTerrainSwitchInvalidForPlacedUnitTerrainSmall;
import test.tacticalOrder.view.leveleditor.unit.CheckUnitPlacementInvalidTerrainSmall;
import test.tacticalOrder.view.leveleditor.unit.CheckUnitPlacementValidAfterUnitAutoRemoval;
import test.tacticalOrder.view.leveleditor.unit.CheckUnitPlacementValidTerrainDefault;
import test.tacticalOrder.view.leveleditor.unit.CheckUnitPlacementValidTerrainSmall;
import test.tacticalOrder.view.leveleditor.CheckLevelEditorCompleteness;
import test.tacticalOrder.view.leveleditor.CheckLevelEditorNavigation;
import test.tacticalOrder.view.mainMenu.CheckMainMenuCompleteness;
import test.tacticalOrder.view.mainMenu.CheckMainMenuNavigation;
import test.wikipedia.WikipediaDemo;
import testEngine.TestController;

public class Main {
	
	public final static void main(String[] args) {
//		executeWikipediaDemo();
		
		TestController.getDriver("firefox", false);
		executeTests();
		TestController.closeDriver();
		
		TestController.getDriver("chrome", false);
		executeTests();
		TestController.closeDriver();
	
	}
		
	private final static void executeTests() {
		// Dialogs
		CheckSetPlayerDialogCompleteness.execute();
		CheckSetScenarioDialogCompleteness.execute();
		
		// General
		CheckAppName.execute();
		// Main Menu
		CheckMainMenuCompleteness.execute();
		CheckMainMenuNavigation.execute();

		// Leveleditor
		CheckLevelEditorCompleteness.execute();
		CheckLevelEditorNavigation.execute();
		
		CheckSetSizeGameMapXY.execute();
		CheckInvalidSizeShrinkGameMapXY.execute();
		
		CheckSetLosingConditionDialogCompleteness.execute();
		CheckGetLosingConditionDefault.execute();
		CheckAddLosingConditionNew.execute();
		CheckUpdateLosingConditionUnitReachedFieldOnly.execute();
		CheckUpdateLosingConditionUnitReachedField2Conditions.execute();
		CheckDeselectLosingConditionFirst1Condition.execute();
		CheckDeselectLosingConditionFirst2Conditions.execute();
		CheckDeselectLosingConditionSecond1Condition.execute();
		CheckDeselectLosingConditionSecond2Conditions.execute();
		
		CheckValidAddPlayer.execute();
		CheckInvalidAddPlayer.execute();
		CheckValidDeletePlayer.execute();
		CheckInvalidDeletePlayer.execute();
		
		CheckValidColorChangeBevore0FreeBevore0FreeAfter2Player.execute();
		CheckValidColorChangeBevore0FreeBevore3Player.execute();
		CheckValidColorChangeBevore1FreeBevore0FreeAfter2Player.execute();
		CheckValidColorChangeBevore1FreeBevore1FreeAfter2Player.execute();
		CheckValidColorChangeBevoreFirstColor2Player.execute();
		CheckValidColorChangeBevoreSecondColor2Player.execute();
		CheckInvalidColorChangeBevorePlayerMax.execute();
		
		CheckValidColorChangeNext0FreeBevore0FreeAfter2Player.execute();
		CheckValidColorChangeNext0FreeAfter3Player.execute();
		CheckValidColorChangeNext0FreeBevore1FreeAfter2Player.execute();
		CheckValidColorChangeNext1FreeBevore2Player.execute();
		CheckValidColorChangeNextLastColor2Player.execute();
		CheckValidColorChangeNextSecondToLastColor2Player.execute();
		CheckInvalidColorChangeNextPlayerMax.execute();
		
		CheckUnitColorBevoreColorChange.execute();
		CheckUnitColorAfterColorChange.execute();	
		
		CheckSaveAndLoadOfScenarioAfterRefresh.execute();
		CheckSaveAndLoadOfScenarioBevoreRefresh.execute();
		CheckSaveAndLoadOfScenarioLinesville.execute();
		CheckSaveAndLoadOfScenarioNukeTown.execute();
		CheckSaveAndLoadOfScenariosAfterRefresh.execute();
		CheckSaveAndLoadOfScenariosBevoreRefresh.execute();
		
		CheckGetScenarioName.execute();
		CheckSetScenarioNameBevoreDialogClose.execute();
		CheckSetScenarioNameAfterDialogClose.execute();
		
		CheckSetAndGetTerrainSmall.execute();
		
		CheckUnitPlacementValidTerrainDefault.execute();
		CheckUnitPlacementInvalidTerrainSmall.execute();
		CheckUnitPlacementValidAfterUnitAutoRemoval.execute();
		CheckUnitPlacementValidTerrainSmall.execute();
		CheckTerrainSwitchInvalidForPlacedUnitTerrainSmall.execute();	
	}
	
	@SuppressWarnings("unused")
	private final static void executeWikipediaDemo() {
		final String url = "https://wikipedia.de";
		
		TestController.getDriver("firefox", false);
		TestController.setAppUrl(url); // getting Driver fetch the app url as a param and set the attribute
		WikipediaDemo.execute();
		TestController.closeDriver();
		
		TestController.getDriver("chrome", false);
		TestController.setAppUrl(url); // getting Driver fetch the app url as a param and set the attribute
		WikipediaDemo.execute();
		TestController.closeDriver();
	}
}
