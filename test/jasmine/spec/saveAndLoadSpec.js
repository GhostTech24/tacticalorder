/**
 * eng: test class. Contains unit-tests for the save and load module.
 * https://creativecommons.org/licenses/by-sa/4.0/
 * by Rudolf Geist
 */
// required TestData.js
describe('eng: Testsuite for the save and load module. ger: Testsuite fuer das SaveAndLoad-Modul.', function() {
    /** Localy gotten scenarios to check.
     * @type {Map<string, {}>} current scenarios.
     */
    let scenariosCurrent;

    beforeEach(function() {
        SaveAndLoad.deleteSaveData();
        SaveAndLoad.loadScenarios();
    });

    it('Should get local default Scenarios.', async function() {
        // variables / Variablen
        const scenariosExpected = new Map();
        
        // execution / Ausfuehrung
        scenariosCurrent = await SaveAndLoad.loadScenarios();

        expect(scenariosExpected).toEqual(scenariosCurrent);
    });

    it('Should set local Scenarios.', async function() {
        // variables / Variablen
        const scenariosExpected = new Map();
        scenariosExpected.set(TestData.getScenarioNuketown2PlayersEmpty().name, TestData.getScenarioNuketown2PlayersEmpty());

        // execution / Ausfuehrung
        await SaveAndLoad.saveScenarios(scenariosExpected);

        scenariosCurrent = await SaveAndLoad.loadScenarios();

        expect(scenariosExpected).toEqual(scenariosCurrent);
    });

    it('Should delete local Scenarios.', async function() {
        // variables / Variablen
        const scenariosBevore = new Map();
        scenariosBevore.set(TestData.getScenarioNuketown2PlayersEmpty().name, TestData.getScenarioNuketown2PlayersEmpty());
        const scenariosExpected = new Map();

        // execution / Ausfuehrung
        await SaveAndLoad.saveScenarios(scenariosBevore);
        await SaveAndLoad.deleteSaveData();

        scenariosCurrent = await SaveAndLoad.loadScenarios();

        expect(scenariosExpected).toEqual(scenariosCurrent);
    });
});