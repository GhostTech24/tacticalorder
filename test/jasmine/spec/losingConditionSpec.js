/**
 * eng: test class. Contains unit-tests for losing conditions.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// required losingCondition.js, scenario.js, unit.js, testData.js
describe('eng: Testsuite for the losing condition module. ger: Testsuite fuer das LosingCondition-Modul.', function() {
   

    beforeEach(function() {
        Scenario.setGameMaps();
        Scenario.resetPlayersStart();
        Scenario.setPlayerStart(1, TestData.getPlayer1Default());
        Scenario.setPlayerStart(2, TestData.getPlayer2Default());
    });
    
    it('Should get losing condition \'allUnitsLost\'.', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'allUnitsLost';
        const affectedPlayerIdExpected = 0;
        const conditionCheckTypeOfExpected = 'function';
        const losingConditionXCoordinateExpected = null;
        const losingConditionYCoordinateExpected = null;
        let losingConditionCurrent;

        // execution / Ausfuehrung
        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);

        expect(losingConditionIdExpected).toBe(losingConditionCurrent.id);
        expect(affectedPlayerIdExpected).toBe(losingConditionCurrent.affectedPlayerId);
        expect(conditionCheckTypeOfExpected).toBe(typeof losingConditionCurrent.conditionCheck);
        expect(losingConditionXCoordinateExpected).toBe((losingConditionCurrent.xCoordinate));
        expect(losingConditionYCoordinateExpected).toBe((losingConditionCurrent.yCoordinate));
    });

    it('Should get positiv response for losing condition \'allUnitsLost\'.', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'allUnitsLost';
        const affectedPlayerId = 1;
        const hasLostExpected = true;
        let losingConditionCurrent, hasLostCurrent;

        // execution / Ausfuehrung
        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);
        losingConditionCurrent.affectedPlayerId = affectedPlayerId;

        hasLostCurrent = losingConditionCurrent.conditionCheck();
        

        expect(hasLostExpected).toBe(hasLostCurrent, 'Check vor positiv response of losing condition "allUnitsLost" is not as expected! Player with id |' + Scenario.getPlayerStart(affectedPlayerId).id + '| and units count of |' + Scenario.getPlayerStart(affectedPlayerId).units + '| should be: |' + hasLostExpected + '|, but was |' + hasLostCurrent + '|');
    });

    it('Should get negative response for losing condition "allUnitsLost".', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'allUnitsLost';
        const affectedPlayerId = 1;
        const xCoordinate = 0;
        const yCoordinate = 0;
        const hasLostExpected = false;
        let losingConditionCurrent, hasLostCurrent;

        // execution / Ausfuehrung
        const unit = Unit.getUnit('infantry');
        unit.ownerCurrent = affectedPlayerId;
        Scenario.setUnitStart(xCoordinate, yCoordinate, unit);
        
        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);
        losingConditionCurrent.affectedPlayerId = affectedPlayerId;

        hasLostCurrent = losingConditionCurrent.conditionCheck();
        

        expect(hasLostExpected).toBe(hasLostCurrent, 'Check vor negative response of losing condition "allUnitsLost" is not as expected! Player with id |' + Scenario.getPlayerStart(affectedPlayerId).id + '| and units count of |' + Scenario.getPlayerStart(affectedPlayerId).units + '| should be: |' + hasLostExpected + '|, but was |' + hasLostCurrent + '|');
    });

    it('Should get losing condition "unitReachedField".', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'unitReachedField';
        const affectedPlayerIdExpected = 0;
        const conditionCheckTypeOfExpected = 'function';
        const losingConditionXCoordinateExpected = null;
        const losingConditionYCoordinateExpected = null;
        let losingConditionCurrent;

        // execution / Ausfuehrung
        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);

        expect(losingConditionIdExpected).toEqual(losingConditionCurrent.id);
        expect(affectedPlayerIdExpected).toEqual(losingConditionCurrent.affectedPlayerId);
        expect(conditionCheckTypeOfExpected).toEqual(typeof losingConditionCurrent.conditionCheck);
        expect(losingConditionXCoordinateExpected).toBe((losingConditionCurrent.xCoordinate));
        expect(losingConditionYCoordinateExpected).toBe((losingConditionCurrent.yCoordinate));
    });

    it('Should get positiv response for losing condition "unitReachedField".', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'unitReachedField';
        const affectedPlayerId = 1;
        const playerIdEnemy = 2;
        const xCoordinate = 0;
        const yCoordinate = 0;   
        const hasLostExpected = true;
        let losingConditionCurrent, hasLostCurrent;

        // execution / Ausfuehrung
        const unit = Unit.getUnit('infantry');
        unit.ownerCurrent = playerIdEnemy;
        Scenario.setUnitStart(xCoordinate, yCoordinate, unit);

        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);
        losingConditionCurrent.affectedPlayerId = affectedPlayerId;
        losingConditionCurrent.xCoordinate = xCoordinate;
        losingConditionCurrent.yCoordinate = yCoordinate;

        hasLostCurrent = losingConditionCurrent.conditionCheck();
        

        expect(hasLostExpected).toBe(hasLostCurrent, 'Check vor positiv response of losing condition "unitReachedField" is not as expected! Player with id |' + Scenario.getPlayerStart(affectedPlayerId).id + '| and units count of |' + Scenario.getPlayerStart(affectedPlayerId).units + '| should be: |' + hasLostExpected + '|, but was |' + hasLostCurrent + '|');
    });

    it('Should get negative response for losing condition "unitReachedField" for no unit on the field.', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'unitReachedField';
        const affectedPlayerId = 1;
        const xCoordinate = 0;
        const yCoordinate = 0;   
        const hasLostExpected = false;
        let losingConditionCurrent, hasLostCurrent;

        // execution / Ausfuehrung
        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);
        losingConditionCurrent.affectedPlayerId = affectedPlayerId;
        losingConditionCurrent.xCoordinate = xCoordinate;
        losingConditionCurrent.yCoordinate = yCoordinate;

        hasLostCurrent = losingConditionCurrent.conditionCheck();
        

        expect(hasLostExpected).toBe(hasLostCurrent, 'Check vor negative response of losing condition "unitReachedField" with no unit is not as expected! Player with id |' + Scenario.getPlayerStart(affectedPlayerId).id + '| and units count of |' + Scenario.getPlayerStart(affectedPlayerId).units + '| should be: |' + hasLostExpected + '|, but was |' + hasLostCurrent + '|');
    });

    it('Should get negative response for losing condition "unitReachedField" for friendly unit on the field.', function() {
        // variables / Variablen
        const losingConditionIdExpected = 'unitReachedField';
        const affectedPlayerId = 1;
        const xCoordinate = 0;
        const yCoordinate = 0;   
        const hasLostExpected = false;
        let losingConditionCurrent, hasLostCurrent;

        // execution / Ausfuehrung
        const unit = Unit.getUnit('infantry');
        unit.ownerCurrent = affectedPlayerId;
        Scenario.setUnitStart(xCoordinate, yCoordinate, unit);

        losingConditionCurrent = LosingCondition.getLosingCondition(losingConditionIdExpected);
        losingConditionCurrent.affectedPlayerId = affectedPlayerId;
        losingConditionCurrent.xCoordinate = xCoordinate;
        losingConditionCurrent.yCoordinate = yCoordinate;

        hasLostCurrent = losingConditionCurrent.conditionCheck();
        

        expect(hasLostExpected).toBe(hasLostCurrent, 'Check vor negativ response of losing condition "unitReachedField" with friendly unit is not as expected! Player with id |' + Scenario.getPlayerStart(affectedPlayerId).id + '| and units count of |' + Scenario.getPlayerStart(affectedPlayerId).units + '| should be: |' + hasLostExpected + '|, but was |' + hasLostCurrent + '|');
    });
});