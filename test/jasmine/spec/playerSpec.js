/**
 * eng: test class. Contains unit-tests for players.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// required -
describe('eng: Testsuite for the player module. ger: Testsuite fuer das Player-Modul.', function() {
    /**
     * @type {[[object]]} 2D array of the game map terrain
     */
    let terrainsCurrent;
    /**
     * @type {[[object]]} 2D array of the game map units
     */
    let unitsCurrent;

    beforeEach(function() {
        Scenario.setGameMaps();
        Scenario.resetPlayersStart();
        terrainsCurrent = undefined;
        unitsCurrent = undefined;
    });
    
    it('Should get player template.', function() {
        // variables / Variablen
        const playerTemplateExpected = TestData.getPlayerTemplate();
        let playerTemplateCurrent

        // execution / Ausfuehrung
        playerTemplateCurrent = Player.getPlayerTemplate();

        expect(playerTemplateExpected).toEqual(playerTemplateCurrent);
    });

    it('Should get player colors.', function() {
        // variables / Variablen
        const playerColorsExpected = TestData.getColors();
        let playerColorsCurrent

        // execution / Ausfuehrung
        playerColorsCurrent = Player.getColors();

        expect(playerColorsExpected).toEqual(playerColorsCurrent);
    });

    it('Should get neutral color.', function() {
        // variables / Variablen
        const neutralColorExpected = 'Grey';
        let neutralColorCurrent

        // execution / Ausfuehrung
        neutralColorCurrent = Player.getNeutralColor();

        expect(neutralColorExpected).toEqual(neutralColorCurrent);
    });
});