/**
 * eng: test class. Contains unit-tests for the game map.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// required gameMap.js, TestData.js, unit.js, terrain.js
describe('eng: Testsuite for the game map module. ger: Testsuite fuer das GameMap-Modul.', function() {
    /**
     * @type {[[[object]]]} container array for 2D array of the game map
     */
    let mapsCurrent;
    /**
     * @type {[[object]]} 2D array of the game map terrain
     */
    let terrainsCurrent;
    /**
     * @type {[[object]]} 2D array of the game map units
     */
    let unitMapCurrent;

    beforeEach(function() {
        mapsCurrent = GameMap.createGameMaps(); // reset default map for other test specs (like modelSpec.js)
        terrainsCurrent = undefined;
        unitMapCurrent = undefined;
    });

    it('Should create game map with default size / terrain.', function() {
        // variables / Variablen
        const terrainsExpected = TestData.getTerrainsDefault();
        terrainsCurrent = mapsCurrent[0];

        // execution / Ausfuehrung
        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should check x fields and y fields of an created game map with fixed size.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        let xFieldsCurrent, yFieldsCurrent;

        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected);
        terrainsCurrent = mapsCurrent[0];

        // execution / Ausfuehrung
        xFieldsCurrent = terrainsCurrent.length;
        yFieldsCurrent = terrainsCurrent[0].length;

        expect(xFieldsExpected).toBe(xFieldsCurrent, 'Invalid X field counter! Should be |' + xFieldsExpected + '| but was |' + xFieldsCurrent + '|');
        expect(yFieldsExpected).toBe(yFieldsCurrent, 'Invalid Y field counter! Should be |' + yFieldsExpected + '| but was |' + yFieldsCurrent + '|');
    });

    it('Should get specific terrain from custom game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();   
        const terrainXCoordinate = 0;
        const terrainYCoordinate = 1;
        const terrainExpected = Terrain.getTerrain('city');
        let terrainCurrent;

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        terrainsCurrent = mapsCurrent[0];

        terrainCurrent = terrainsCurrent[terrainXCoordinate][terrainYCoordinate];

        expect(terrainExpected).toEqual(terrainCurrent);
    });

    it('Should set specific terrain for game map with custom size / terrain.', function() {
        // variables / Variablen
        const xFieldsExpected = 4;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSquaredChanged();
        const mapBeginning = TestData.getTerrainsSquared();
        const terrainXCoordinate = 3;
        const terrainYCoordinate = 2;
        const terrainNew = Terrain.getTerrain('city');     

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected, mapBeginning);
        terrainsCurrent = mapsCurrent[0];
        terrainsCurrent[terrainXCoordinate][terrainYCoordinate] =  terrainNew;
        

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should create game map with custom size / terrain.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSmall();
        let terrainCurrent

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected);
        terrainsCurrent = mapsCurrent[0];
        
        for(let x = 0; x < xFieldsExpected; x++) {
            for(let y = 0; y < yFieldsExpected; y++) {
                terrainCurrent = terrainsExpected[x][y];
                
                terrainsCurrent[x][y] = terrainCurrent;
            }
        }

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should expand a custom game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const expandLaneCounter = 2;
        const terrainsExpected = TestData.getTerrainsSmallExtendedXY();

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        terrainsCurrent = mapsCurrent[0];

        mapsCurrent = GameMap.createGameMaps(xFieldsExpected + expandLaneCounter, yFieldsExpected + expandLaneCounter, terrainsCurrent);
        terrainsCurrent = mapsCurrent[0];
        
        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should shrink a custom game map.', function() {
        // variables / Variablen
        const expectedXFields = 8;
        const expectedYFields = 6;
        const terrainsMedium = TestData.getTerrainsMedium();
        const shrinkLaneCounter = 2;      
        const expectedMap = TestData.getTerrainsMediumShrinkedXY();

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(expectedXFields, expectedYFields, terrainsMedium);
        terrainsCurrent = mapsCurrent[0];
        mapsCurrent = GameMap.createGameMaps(expectedXFields - shrinkLaneCounter, expectedYFields - shrinkLaneCounter, terrainsCurrent);
        terrainsCurrent = mapsCurrent[0];

        expect(expectedMap).toEqual(terrainsCurrent);
    });

    it('Should set and get a unit of an specific field from game map.', function() {
        // variables / Variablen 
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const unitXCoordinate = 1;
        const unitYCoordinate = 2;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        unitMapCurrent = mapsCurrent[1];
        
        unitMapCurrent[unitXCoordinate][unitYCoordinate] = unitExpected;
        unitCurrent = unitMapCurrent[unitXCoordinate][unitYCoordinate];
        
        expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should replace and get the setted unit of an specific field from game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const replacedUnit = Unit.getUnit('infantry');
        const expectedUnit = Unit.getUnit('heavyInfantry');
        let unitCurrent;

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        unitMapCurrent = mapsCurrent[1];
        
        unitMapCurrent[unitXCoordinate][unitYCoordinate] = replacedUnit;
        unitMapCurrent[unitXCoordinate][unitYCoordinate] = expectedUnit;
        unitCurrent = unitMapCurrent[unitXCoordinate][unitYCoordinate];
        
        expect(expectedUnit).toEqual(unitCurrent);
    });

    it('Should get a unit of an specific field after a previous smaller game map was enlarged at X and Y.', function() {
        // variables / Variablen 
        const xFieldsBevore = 3;
        const yFieldsBevore = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const counterExpand = 2;
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent;

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsBevore, yFieldsBevore, mapSmall);
        unitMapCurrent = mapsCurrent[1];
        unitMapCurrent[unitXCoordinate][unitYCoordinate] =  unitExpected;

        mapsCurrent = GameMap.createGameMaps(xFieldsBevore + counterExpand, yFieldsBevore + counterExpand, mapSmall, unitMapCurrent);
        unitMapCurrent = mapsCurrent[1];
        unitCurrent = unitMapCurrent[unitXCoordinate][unitYCoordinate]
        
        expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should get a unit of an specific field after a previous smaller game map was enlarged at X.', function() {
        // variables / Variablen
        const xFieldsBevore = 3;
        const yFieldsBevore = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const expandXCounter = 2;
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent;

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsBevore, yFieldsBevore, mapSmall);
        unitMapCurrent = mapsCurrent[1];
        unitMapCurrent[unitXCoordinate][unitYCoordinate] = unitExpected;

        mapsCurrent = GameMap.createGameMaps(xFieldsBevore + expandXCounter, yFieldsBevore, mapSmall, unitMapCurrent);
        unitMapCurrent = mapsCurrent[1];
        unitCurrent = unitMapCurrent[unitXCoordinate][unitYCoordinate];

        expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should get a unit of an specific field after a previous smaller game map was enlarged at Y.', function() {
        // variables / Variablen
        const xFieldsBevore = 3;
        const yFieldsBevore = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const expandYCounter = 2;
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent;

        // execution / Ausfuehrung
        mapsCurrent = GameMap.createGameMaps(xFieldsBevore, yFieldsBevore, mapSmall);
        unitMapCurrent = mapsCurrent[1];
        unitMapCurrent[unitXCoordinate][unitYCoordinate] = unitExpected;

        mapsCurrent = GameMap.createGameMaps(xFieldsBevore, yFieldsBevore + expandYCounter, mapSmall, unitMapCurrent);
        unitMapCurrent = mapsCurrent[1];
        unitCurrent = unitMapCurrent[unitXCoordinate][unitYCoordinate];

        expect(unitExpected).toEqual(unitCurrent);
    });
});