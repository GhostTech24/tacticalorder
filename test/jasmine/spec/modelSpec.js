/** eng: test class. Contains unit-tests for the model.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// required model.js, scenario.js, players.js, TestData.js
describe('eng: Testsuite for the model module. ger: Testsuite fuer das Modell-Modul.', function() {
    /** The default scenario name.
     * @type {string}
     */
    const scenarioNameDefault = 'newScenario';
    
    beforeEach(function() {    
        Model.resetModel();
    }); 
    
    // scenario save and load
    it('Should load saved scenario \'Nuke Town\' without units after a reset.', async function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioNuketown2PlayersEmpty();
        const scenarioName = scenarioExpected.name;
        let scenarioCurrent;

        // execution / Ausfuehrung
        Model.setScenario(scenarioExpected);
        await Model.saveScenario();
        Model.resetScenario();
        await Model.loadScenarios();
        Model.loadScenario(scenarioName);

        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should load saved scenarios default and \'Nuke Town\' without units after a reset.', async function() {
        // variables / Variablen
        const scenarioDefaultExpected = TestData.getScenarioDefault2Player1LosingCondition();
        const scenarioNukeTownExpected = TestData.getScenarioNuketown2PlayersEmpty();
        const scenarioNameNukeTown = scenarioNukeTownExpected.name;
        let scenarioCurrent;

        // execution / Ausfuehrung
        await Model.saveScenario(); // save default

        Model.setScenario(scenarioNukeTownExpected);
        await Model.saveScenario();

        Model.resetScenario();
        await Model.loadScenarios();


        Model.loadScenario(scenarioNameNukeTown);
        scenarioCurrent = Scenario.getScenario();
        expect(scenarioNukeTownExpected).toEqual(scenarioCurrent);

        Model.loadScenario(scenarioNameDefault);
        scenarioCurrent = Scenario.getScenario();
        expect(scenarioDefaultExpected).toEqual(scenarioCurrent);
    });

    it('Should delete scenario \'Nuke Town\' so no saved scenario remain.', async function() {
        // variables / Variablen
        const scenarioNamesExpected = [];
        const scenarioNukeTown =  TestData.getScenarioNuketown2PlayersEmpty();
        const scenarioNameNukeTown = scenarioNukeTown.name;
        let scenarioNamesCurrent;

        // execution / Ausfuehrung
        Model.setScenario(scenarioNukeTown);
        await Model.saveScenario();

        await Model.deleteScenario(scenarioNameNukeTown);
        
        scenarioNamesCurrent = Model.getScenarioNames();
        expect(scenarioNamesExpected).toEqual(scenarioNamesCurrent);
    });

    it('Should delete default scenario so only scenario \'Nuke Town\' remain.', async function() {
        // variables / Variablen
        const scenarioNukeTown = TestData.getScenarioNuketown2PlayersEmpty();
        const scenarioNameNukeTown = scenarioNukeTown.name;
        const scenarioNamesExpected = [scenarioNameNukeTown];
        let scenarioNamesCurrent, scenarioCurrent;

        // execution / Ausfuehrung
        await Model.saveScenario(); // save default

        Model.setScenario(scenarioNukeTown);
        await Model.saveScenario();

        await Model.deleteScenario(scenarioNameDefault);

        scenarioNamesCurrent = Model.getScenarioNames();
        expect(scenarioNamesExpected).toEqual(scenarioNamesCurrent);

        Model.loadScenario(scenarioNameNukeTown);
        scenarioCurrent = Scenario.getScenario();
        expect(scenarioNukeTown).toEqual(scenarioCurrent);       
    });

    it('Should get scenario after 1 player was added to default scenario.', function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioDefault3Player1LosingCondition();
        let isAddSuccessful, scenarioCurrent

        // execution / Ausfuehrung
        isAddSuccessful = Model.addPlayerStart();
        scenarioCurrent = Scenario.getScenario();

        expect(isAddSuccessful).toBe(true, 'TODO good description.');
        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should set scenario \'Nuke Town\' without units.', function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioNuketown2PlayersEmpty();
        let scenarioCurrent

        // execution / Ausfuehrung
        Model.setScenario(scenarioExpected);

        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should set scenario \'Nuke Town\' with units.', function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioNuketown2PlayersPlaced();
        let scenarioCurrent

        // execution / Ausfuehrung
        Model.setScenario(scenarioExpected);

        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    // scenario names
    it('Should get no scenario names as default.', function() {
        // variables / Variablen
        const scenarioNamesExpected = [];
        let scenarioNamesCurrent

        // execution / Ausfuehrung
        scenarioNamesCurrent = Model.getScenarioNames();

        expect(scenarioNamesExpected).toEqual(scenarioNamesCurrent);
    });

    it('Should get scenario names after saving the default scenario.', async function() {
        // variables / Variablen
        const scenarioNamesExpected = [scenarioNameDefault];
        let scenarioNamesCurrent

        // execution / Ausfuehrung
        await Model.saveScenario();
        scenarioNamesCurrent = Model.getScenarioNames();

        expect(scenarioNamesExpected).toEqual(scenarioNamesCurrent);
    });

    // scenario name
    it('Should get default scenario name.', function() {
        let scenarioNameCurrent;

        // execution / Ausfuehrung
        scenarioNameCurrent = Model.getScenarioName();
        expect(scenarioNameDefault).toBe(scenarioNameCurrent, 'Scenario name is not as expected! Name should be: |' + scenarioNameDefault + '| but was: |' + scenarioNameCurrent + '|');
    });

    it('Should set scenario name without spaces numbers or special letters.', function() {
        const scenarioNameExpected = 'myNewMap';
        let scenarioNameCurrent;

        // execution / Ausfuehrung
        Model.setScenarioName(scenarioNameExpected);

        scenarioNameCurrent = Model.getScenarioName();
        expect(scenarioNameExpected).toBe(scenarioNameCurrent, 'Scenario name is not as expected! Name should be: |' + scenarioNameExpected + '| but was: |' + scenarioNameCurrent + '|');
    });

    it('Should set scenario name with spaces and without numbers or special letters.', function() {
        const scenarioNameExpected = 'my new Map';
        let scenarioNameCurrent;

        // execution / Ausfuehrung
        Model.setScenarioName(scenarioNameExpected);

        scenarioNameCurrent = Model.getScenarioName();
        expect(scenarioNameExpected).toBe(scenarioNameCurrent, 'Scenario name is not as expected! Name should be: |' + scenarioNameExpected + '| but was: |' + scenarioNameCurrent + '|');
    });

    it('Should set scenario name with numbers and without spaces or special letters.', function() {
        const scenarioNameExpected = 'myNewMap2';
        let scenarioNameCurrent;

        // execution / Ausfuehrung
        Model.setScenarioName(scenarioNameExpected);

        scenarioNameCurrent = Model.getScenarioName();
        expect(scenarioNameExpected).toBe(scenarioNameCurrent, 'Scenario name is not as expected! Name should be: |' + scenarioNameExpected + '| but was: |' + scenarioNameCurrent + '|');
    });

    it('Should set scenario name with special letters and without spaces or numbers.', function() {
        const scenarioNameExpected = 'my!New%Map2';
        let scenarioNameCurrent;

        // execution / Ausfuehrung
        Model.setScenarioName(scenarioNameExpected);

        scenarioNameCurrent = Model.getScenarioName();
        expect(scenarioNameExpected).toBe(scenarioNameCurrent, 'Scenario name is not as expected! Name should be: |' + scenarioNameExpected + '| but was: |' + scenarioNameCurrent + '|');
    });

    it('Should set scenario name with spaces, numbers and special letters.', function() {
        const scenarioNameExpected = 'my new !Map! 2';
        let scenarioNameCurrent;

        // execution / Ausfuehrung
        Model.setScenarioName(scenarioNameExpected);

        scenarioNameCurrent = Model.getScenarioName();
        expect(scenarioNameExpected).toBe(scenarioNameCurrent, 'Scenario name is not as expected! Name should be: |' + scenarioNameExpected + '| but was: |' + scenarioNameCurrent + '|');
    });

    // player
    it('Should succed to add player at start of scenario.', function() {
        const playersStartSizeExpected = 3;
        const isExecutedExpected = true;
        let isExecutedCurrent, playersStartSizeCurrent;

        // execution / Ausfuehrung
        isExecutedCurrent = Model.addPlayerStart();
        expect(isExecutedExpected).toEqual(isExecutedCurrent);
        
        playersStartSizeCurrent = Model.getPlayersStartSize();
        expect(playersStartSizeExpected).toEqual(playersStartSizeCurrent);
    });

    it('Should fail to add player at start of scenario at maximum players size.', function() {
        const playersStartSizeExpected = Player.getColors().length;
        const runCounterUntilMaxPlayerSize = playersStartSizeExpected - 2;  
        const isExecutedExpected = false;
        let isExecutedCurrent, playersStartSizeCurrent;

        // execution / Ausfuehrung
        for(let i = 0; i < runCounterUntilMaxPlayerSize; i++) {
            Model.addPlayerStart();
        }
        isExecutedCurrent = Model.addPlayerStart();
        expect(isExecutedExpected).toEqual(isExecutedCurrent);
        
        playersStartSizeCurrent = Model.getPlayersStartSize();
        expect(playersStartSizeExpected).toEqual(playersStartSizeCurrent);
    });

    it('Should succed to delete player at start of scenario.', function() {
        const playersStartSizeExpected = 2;
        const isExecutedExpected = true;
        let isExecutedCurrent, playersStartSizeCurrent;

        // execution / Ausfuehrung
        Model.addPlayerStart();
        isExecutedCurrent = Model.deletePlayerStart();
        expect(isExecutedExpected).toEqual(isExecutedCurrent);
        
        playersStartSizeCurrent = Model.getPlayersStartSize();
        expect(playersStartSizeExpected).toEqual(playersStartSizeCurrent);
    });

    it('Should fail to delete player at start of scenario at minimum players size.', function() {
        const playersStartSizeExpected = 2;
        const isExecutedExpected = false;
        let isExecutedCurrent, playersStartSizeCurrent;

        // execution / Ausfuehrung
        isExecutedCurrent = Model.deletePlayerStart();
        expect(isExecutedExpected).toEqual(isExecutedCurrent);
        
        playersStartSizeCurrent = Model.getPlayersStartSize();
        expect(playersStartSizeExpected).toEqual(playersStartSizeCurrent);
    });

    it('Should get specific player at start of scenario.', function() {
        const playerExpected = TestData.getPlayer2Default();
        const playerId = 2;
        let playerCurrent;

        // execution / Ausfuehrung
        Model.addPlayerStart();
        
        playerCurrent = Model.getPlayerStart(playerId);
        expect(playerExpected).toEqual(playerCurrent);
    });

    it('Should get players size', function() {
        const playersSizeExpected = 2;
        let playersSizeCurrent;

        // execution / Ausfuehrung
        playersSizeCurrent = Model.getPlayersStartSize();
        expect(playersSizeExpected).toEqual(playersSizeCurrent);
    });
    // player color change bevore
    it('Should change to color bevore for player at start of scenario when no previous color is taken and no color after is free.', function() {
        const player1Color = Player.getColors()[Player.getColors().length - 2];
        const player2Color = Player.getColors()[Player.getColors().length - 1];
        const player1Id = 1;
        const player2Id = 2;
        const changeDirectionValue = -1;
        const playerColorExpected = Player.getColors()[Player.getColors().length - 3];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        const player2Prepared = Model.getPlayerStart(player2Id);
        player2Prepared.color = player2Color;
        Scenario.setPlayerStart(player2Id, player2Prepared);

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(player1Id).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color bevore for player at start of scenario when no previous color is taken and one color after is free.', function() {
        const player1Color = Player.getColors()[Player.getColors().length - 3];
        const player2Color = Player.getColors()[Player.getColors().length - 2];
        const player1Id = 1;
        const player2Id = 2;
        const changeDirectionValue = -1;
        const playerColorExpected = Player.getColors()[Player.getColors().length - 4];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        const player2Prepared = Model.getPlayerStart(player2Id);
        player2Prepared.color = player2Color;
        Scenario.setPlayerStart(player2Id, player2Prepared);

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(player1Id).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color bevore for player at start of scenario when one previous color is taken.', function() {
        const player1Color = Player.getColors()[Player.getColors().length - 2];
        const player2Color = Player.getColors()[Player.getColors().length - 1];
        const player1Id = 1;
        const player2Id = 2;
        const changeDirectionValue = -1;
        const playerColorExpected = Player.getColors()[Player.getColors().length - 3];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        const player2Prepared = Model.getPlayerStart(player2Id);
        player2Prepared.color = player2Color;
        Scenario.setPlayerStart(player2Id, player2Prepared);

        Model.changePlayerStartColor(player2Id, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(player2Id).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color bevore for player at start of scenario when two previous colors are taken.', function() {
        const player1Id = 1;
        const player2Id = 2;
        const player3Id = 3;
        const changeDirectionValue = -1;
        const player1Color = Player.getColors()[Player.getColors().length - 3];
        const player2Color = Player.getColors()[Player.getColors().length - 2];
        const player3Color = Player.getColors()[Player.getColors().length - 1];
        const playerColorExpected = Player.getColors()[Player.getColors().length - 4];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        const player2Prepared = Model.getPlayerStart(player2Id);
        player2Prepared.color = player2Color;
        Scenario.setPlayerStart(player2Id, player2Prepared);

        const player3Prepared = Scenario.getPlayerStart(player1Id);
        player3Prepared.id = player3Id;
        player3Prepared.color = player3Color;
        Scenario.setPlayerStart(player3Id, player3Prepared);

        Model.changePlayerStartColor(player3Color, changeDirectionValue);
        
        playerColorCurrent = Model.getPlayerStart(player3Color).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color bevore for player at start of scenario when no previous color is taken and current color is first one.', function() {
        const playerId = 1;
        const changeDirectionValue = -1;
        const playerColorExpected = Player.getColors()[Player.getColors().length - 1];
        let playerColorCurrent;

        // execution / Ausfuehrung
        Model.changePlayerStartColor(playerId, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(playerId).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color bevore for player at start of scenario when one following color is taken and current color is second one.', function() {
        const playerId = 2;
        const changeDirectionValue = -1;
        const playerColorExpected = Player.getColors()[Player.getColors().length - 1];
        let playerColorCurrent;

        // execution / Ausfuehrung
        Model.changePlayerStartColor(playerId, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(playerId).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should not change to color bevore for player at start of scenario when maximum players are used.', function() {
        const player1Id = 1;
        const playerIdFirstToAdd = 3;
        const playerIdMax = 6;
        const playerColorExpected = Player.getColors()[0];
        const changeDirectionValue = -1;
        let playerColorCurrent;

        // execution / Ausfuehrung
        for(let i = playerIdFirstToAdd; i<= playerIdMax; i++) {
            const playerPrepared = Player.getPlayerTemplate();
            playerPrepared.id = i;
            playerPrepared.color = Player.getColors()[i - 1];
            Scenario.setPlayerStart(i, playerPrepared);
        }

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        
        playerColorCurrent = Model.getPlayerStart(player1Id).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });
    // player color change next
    it('Should change to color next for player at start of scenario when no following color is taken and no color bevore is free.', function() {
        const playerId = 2;
        const changeDirectionValue = 1;
        const playerColorExpected = Player.getColors()[2];
        let playerColorCurrent;

        // execution / Ausfuehrung
        Model.changePlayerStartColor(playerId, changeDirectionValue);
        
        playerColorCurrent = Model.getPlayerStart(playerId).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color next for player at start of scenario when no following color is taken and one color bevore is free.', function() {
        const player1Color = Player.getColors()[1];
        const player2Color = Player.getColors()[2];
        const player1Id = 1;
        const player2Id = 2;
        const changeDirectionValue = 1;
        const playerColorExpected = Player.getColors()[3];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        const player2Prepared = Model.getPlayerStart(player2Id);
        player2Prepared.color = player2Color;
        Scenario.setPlayerStart(player2Id, player2Prepared);

        Model.changePlayerStartColor(player2Id, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(player2Id).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color next for player at start of scenario when one following color is taken.', function() {
        const playerId = 1;
        const changeDirectionValue = 1;
        const playerColorExpected = Player.getColors()[2];
        let playerColorCurrent;

        // execution / Ausfuehrung
        Model.changePlayerStartColor(playerId, changeDirectionValue);
        
        playerColorCurrent = Model.getPlayerStart(playerId).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });
    
    it('Should change to color next for player at start of scenario when two following colors are taken.', function() {
        const player1Id = 1;
        const player3Id = 3;
        const changeDirectionValue = 1;
        const player3Color = Player.getColors()[2];
        const playerColorExpected = Player.getColors()[3];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player3Prepared = Scenario.getPlayerStart(player1Id);
        player3Prepared.id = player3Id;
        player3Prepared.color = player3Color;
        Scenario.setPlayerStart(player3Id, player3Prepared);

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        
        playerColorCurrent = Model.getPlayerStart(player1Id).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color next for player at start of scenario when no following color is taken and current color is last one.', function() {
        const player1Color = Player.getColors()[Player.getColors().length - 1];
        const player1Id = 1;
        const changeDirectionValue = 1;
        const playerColorExpected = Player.getColors()[0];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(player1Id).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should change to color next for player at start of scenario when one following color is taken and current color is second to last one.', function() {
        const player1Color = Player.getColors()[Player.getColors().length - 2];
        const player2Color = Player.getColors()[Player.getColors().length - 1];
        const player1Id = 1;
        const player2Id = 2;
        const changeDirectionValue = 1;
        const playerColorExpected = Player.getColors()[0];
        let playerColorCurrent;

        // execution / Ausfuehrung
        const player1Prepared = Model.getPlayerStart(player1Id);
        player1Prepared.color = player1Color;
        Scenario.setPlayerStart(player1Id, player1Prepared);

        const player2Prepared = Model.getPlayerStart(player2Id);
        player2Prepared.color = player2Color;
        Scenario.setPlayerStart(player2Id, player2Prepared);

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        playerColorCurrent = Model.getPlayerStart(player1Id).color;

        expect(playerColorExpected).toEqual(playerColorCurrent);
    });

    it('Should not change to color next for player at start of scenario when maximum players are used.', function() {
        const player1Id = 1;
        const playerIdFirstToAdd = 3;
        const playerIdMax = 6;
        const playerColorExpected = Player.getColors()[0];
        const changeDirectionValue = 1;
        let playerColorCurrent;

        // execution / Ausfuehrung
        for(let i = playerIdFirstToAdd; i<= playerIdMax; i++) {
            const playerPrepared = Player.getPlayerTemplate();
            playerPrepared.id = i;
            playerPrepared.color = Player.getColors()[i - 1];
            Scenario.setPlayerStart(i, playerPrepared);
        }

        Model.changePlayerStartColor(player1Id, changeDirectionValue);
        
        playerColorCurrent = Model.getPlayerStart(player1Id).color;
        expect(playerColorExpected).toEqual(playerColorCurrent);
    });
    // player color class name
    it('Should get color class name from placed unit on cordinates.', function() {
        const xCoordinate = 0;
        const yCoordinate = 1;
        const unitId = 'infantry';
        const playerId = 2;
        const colorClassNameExpected = 'playerColorBlue';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        Model.setUnitStart(xCoordinate, yCoordinate, unitId, playerId)
        colorClassNameCurrent = Model.getPlayerColorClassNameOfUnit(xCoordinate, yCoordinate);

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });

    it('Should get color class name from no unit on cordinates.', function() {
        const xCoordinate = 0;
        const yCoordinate = 1;
        const colorClassNameExpected = '';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        colorClassNameCurrent = Model.getPlayerColorClassNameOfUnit(xCoordinate, yCoordinate);

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });

    it('Should get color class name from neutral view selection.', function() {
        const colorClassNameExpected = 'playerColorGrey';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        colorClassNameCurrent = Model.getPlayerColorClassNameOfUnit();

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });

    it('Should get color class name from player2 view selection without color change.', function() {     
        const colorClassNameExpected = 'playerColorBlue';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(1);
        Model.changePlayerIdSelection(1);
        colorClassNameCurrent = Model.getPlayerColorClassNameOfUnit();

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });

    it('Should get color class name from player2 view selection after color change.', function() {     
        const playerId = 2;
        const changeDirectionValue = 1;
        const colorClassNameExpected = 'playerColorGreen';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        Model.changePlayerStartColor(playerId, changeDirectionValue);

        Model.changePlayerIdSelection(1);
        Model.changePlayerIdSelection(1);
        colorClassNameCurrent = Model.getPlayerColorClassNameOfUnit();

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });

    it('Should get color class name from player2 id bevore color change.', function() {     
        const playerId = 2;
        const colorClassNameExpected = 'playerColorBlue';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        colorClassNameCurrent = Model.getPlayerColorClassNameOfPlayer(playerId);

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });

    it('Should get color class name from player2 id after color change.', function() {     
        const playerId = 2;
        const changeDirectionValue = 1;
        const colorClassNameExpected = 'playerColorGreen';
        let colorClassNameCurrent;

        // execution / Ausfuehrung
        Model.changePlayerStartColor(playerId, changeDirectionValue);

        colorClassNameCurrent = Model.getPlayerColorClassNameOfPlayer(playerId);

        expect(colorClassNameExpected).toEqual(colorClassNameCurrent);
    });
    // player id selection
    it('Should get valid player id selection bevor a change.', function() {     
        const playerIdExpected = 0;
        let playerIdCurrent;

        // execution / Ausfuehrung
        playerIdCurrent = Model.getPlayerIdSelection();

        expect(playerIdExpected).toEqual(playerIdCurrent);
    });

    it('Should get valid player id selection after a change.', function() {     
        const changeDirectionValue = 1;
        const playerIdExpected = 1;
        let playerIdCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);

        playerIdCurrent = Model.getPlayerIdSelection();

        expect(playerIdExpected).toEqual(playerIdCurrent);
    });

    it('Should get first player id selection after a change for the next one from the last possible one.', function() {     
        const playerIdSelectionCurrent = Model.getPlayerIdSelection();
        const playersLength = Model.getPlayersStartSize();
        const playersIdSelectionLast = playersLength;
        const changeDirectionValue = 1;
        const playerIdExpected = 0;
        let playerIdCurrent;

        // execution / Ausfuehrung
        for(let i = playerIdSelectionCurrent; i < playersIdSelectionLast ; i++) {
            Model.changePlayerIdSelection(changeDirectionValue);
        }
        Model.changePlayerIdSelection(changeDirectionValue);
        playerIdCurrent = Model.getPlayerIdSelection();

        expect(playerIdExpected).toEqual(playerIdCurrent);
    });

    it('Should get last player id selection after a change for the preview one from the first possible one.', function() {     
        const changeDirectionValue = -1;
        const playerIdExpected = 2;
        let playerIdCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);
        playerIdCurrent = Model.getPlayerIdSelection();

        expect(playerIdExpected).toEqual(playerIdCurrent);
    });

    // loosing condition
    it('Should get default losing condition.', function() {
        // variables / Variablen
        const playerId = 1;
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent,  losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should add losing condition as new entry.', function() {
        // variables / Variablen
        const changeDirectionValue = 1;
        const playerId = 1;
        const losingConditionIdExpected = 'unitReachedField';
        const losingConditionsLengthExpected = 2;
        let losingConditionsCurrent, losingConditionIdCurrent,  losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);
        Model.changeLosingCondition(losingConditionIdExpected);

        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[1].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should add losing condition as overwriting entry.', function() {
        // variables / Variablen
        const changeDirectionValue = 1;
        const playerId = 1;
        const losingConditionIdExpected = 'unitReachedField';
        const xCoordinateBevore = 0;
        const xCoordinateExpected = 3;
        const yCoordinateBevore = 0;
        const yCoordinateExpected = 4;
        const losingConditionsLengthExpected = 2;
        let losingConditionsCurrent, losingConditionIdCurrent, xCoordinateCurrent, yCoordinateCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);
        Model.changeLosingCondition(losingConditionIdExpected, xCoordinateBevore, yCoordinateBevore);

        Model.changeLosingCondition(losingConditionIdExpected, xCoordinateExpected, yCoordinateExpected, true);

        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[1].id;
        xCoordinateCurrent = losingConditionsCurrent[1].xCoordinate;
        yCoordinateCurrent = losingConditionsCurrent[1].yCoordinate;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toBe(losingConditionIdCurrent);
        expect(xCoordinateExpected).toBe(xCoordinateCurrent);
        expect(yCoordinateExpected).toBe(yCoordinateCurrent);
        expect(losingConditionsLengthExpected).toBe(losingConditionsLengthCurrent);
    });

    it('Should delete first losing condition from player with two losing conditions.', function() {
        // variables / Variablen
        const changeDirectionValue = 1;
        const playerId = 1;
        const losingConditionIdExpected = 'unitReachedField';
        const losingConditionIdDelete = 'allUnitsLost'; 
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);
        Model.changeLosingCondition(losingConditionIdExpected);

        Model.changeLosingCondition(losingConditionIdDelete);

        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should delete second losing condition from player with two losing conditions.', function() {
        // variables / Variablen
        const changeDirectionValue = 1;
        const playerId = 1;
        const losingConditionIdExpected = 'unitReachedField';
        const losingConditionIdDelete = 'allUnitsLost'; 
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);
        Model.changeLosingCondition(losingConditionIdExpected);
        
        Model.changeLosingCondition(losingConditionIdDelete);

        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should delete losing condition from player with default losing condition (get default losing condition).', function() {
        // variables / Variablen
        const changeDirectionValue = 1;
        const playerId = 1;
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);

        Model.changeLosingCondition(losingConditionIdExpected);

        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should delete losing condition from player with one different losing condition (get default losing condition).', function() {
        // variables / Variablen
        const changeDirectionValue = 1;
        const playerId = 1;
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionIdBevore = 'unitReachedField'; 
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Model.changePlayerIdSelection(changeDirectionValue);
        Model.changeLosingCondition(losingConditionIdBevore);   
        Model.changeLosingCondition(losingConditionIdExpected);

        Model.changeLosingCondition(losingConditionIdBevore);

        losingConditionsCurrent = Model.getLosingConditions(playerId);
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    // terrains
    it('Should get all terrain ids.', function() {
        const terrainIdsExpected = ['city', 'plane', 'street', 'woods', 'sea', 'mountain'];
        let terrainIdsCurrent;

        // execution / Ausfuehrung
        terrainIdsCurrent = Model.getTerrainIds();
        

        expect(terrainIdsExpected).toEqual(terrainIdsCurrent);
    });

    it('Should get all unit ids.', function() {
        const unitIdsExpected = ['infantry', 'heavyInfantry', 'lightTank'];

        // execution / Ausfuehrung
        unitIdsCurrent = Model.getUnitIds();

        expect(unitIdsExpected).toEqual(unitIdsCurrent);
    });

    it('Should get specific terrain id.', function() {
        const xCoordinate = 3;
        const yCoordinate = 2;
        const terrainIdExpected = 'city';
        let terrainIdCurrent;

        // execution / Ausfuehrung
        Model.setTerrain(xCoordinate, yCoordinate, terrainIdExpected);
        terrainIdCurrent = Model.getTerrainId(xCoordinate, yCoordinate);

        expect(terrainIdExpected).toEqual(terrainIdCurrent);
    });

    it('Should get specific unit id.', function() {
        const xCoordinate = 3;
        const yCoordinate = 2;
        const unitIdExpected = 'lightTank';
        let unitIdCurrent;

        // execution / Ausfuehrung
        Model.setUnitStart(xCoordinate, yCoordinate, unitIdExpected);
        unitIdCurrent = Model.getUnitIdStart(xCoordinate, yCoordinate);

        expect(unitIdExpected).toEqual(unitIdCurrent);
    });

    it('Should get terrains from initial default game map (size / terrain).', function() {
        // variables / Variablen
        const terrainsExpected = TestData.getTerrainsDefault();
        let terrainCurrent;

        // execution / Ausfuehrung
        terrainCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainCurrent);
    });

    it('Should get units from initial default game map (size / terrain).', function() {
        // variables / Variablen
        const unitsExpected = TestData.getUnitsPlacedMapDefault();
        const unit1XCoordinate = 2;
        const unit1YCoordinate = 4;
        const unit1Id = 'infantry';
        const unit2XCoordinate = 9;
        const unit2YCoordinate = 9;
        const unit2Id = 'lightTank';
        let unitsCurrent;

        // execution / Ausfuehrung
        Model.setUnitStart(unit1XCoordinate, unit1YCoordinate, unit1Id);
        Model.setUnitStart(unit2XCoordinate, unit2YCoordinate, unit2Id);
        unitsCurrent = Model.getUnitsStart();

        expect(unitsExpected).toEqual(unitsCurrent);
    });

    it('Should get success for valid unit movability check on terrain.', function() {
        // variables / Variablen
        const unitId = 'lightTank';
        const terrainId = 'street';
        const resultExpected = true;
        let resultCurrent;

        // execution / Ausfuehrung
        resultCurrent = Model.isMovableOnTerrain(unitId, terrainId);

        expect(resultExpected).toBe(resultCurrent, 'Check vor unit movability on terrain is not as expected! Movability from unit |' + unitId + '| on terrain |' + terrainId + '| should be: |' + resultExpected + '|, but was |' + resultCurrent + '|');
    });

    it('Should get failure for invalid unit movability check on terrain.', function() {
        // variables / Variablen
        const unitId = 'lightTank';
        const terrainId = 'sea';
        const resultExpected = false;
        let resultCurrent;

        // execution / Ausfuehrung
        resultCurrent = Model.isMovableOnTerrain(unitId, terrainId);

        expect(resultExpected).toBe(resultCurrent, 'Check vor unit movability on terrain is not as expected! Movability from unit |' + unitId + '| on terrain |' + terrainId + '| should be: |' + resultExpected + '|, but was |' + resultCurrent + '|');
    });
    
    // set different terrains
    it('Should set specific terrain in game map.', function() {
        const terrainsSquared = TestData.getTerrainsSquared();
        const terrainsExpected = TestData.getTerrainsSquaredChanged();
        const xCoordinate = 3;
        const yCoordinate = 2;
        const terrainIdExpected = 'city';
        let terrainsCurrent;

        // execution / Ausfuehrung
        Model.setGameMaps(terrainsSquared.length, terrainsSquared[0].length, terrainsSquared)

        Model.setTerrain(xCoordinate, yCoordinate, terrainIdExpected);
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should expand game map with 2 x lanes.', function() {
        const terrainsSmall = TestData.getTerrainsSmall();
        const terrainsExpected = TestData.getTerrainsSmallExtendedX();
        const xLaneCounterExpand = 2;
        let terrainsCurrent;

        // execution / Ausfuehrung
        Model.setGameMaps(terrainsSmall.length, terrainsSmall[0].length, terrainsSmall);
        terrainsCurrent = Model.getTerrains();
        Model.setGameMaps(terrainsSmall.length + xLaneCounterExpand, terrainsSmall[0].length, terrainsCurrent);
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should expand game map with 2 y lanes.', function() {
        const terrainsSmall = TestData.getTerrainsSmall();
        const terrainsExpected = TestData.getTerrainsSmallExtendedY();
        const yLaneCounterExpand = 2;
        let terrainsCurrent;
        
        // execution / Ausfuehrung
        Model.setGameMaps(terrainsSmall.length, terrainsSmall[0].length, terrainsSmall);
        terrainsCurrent = Model.getTerrains();
        Model.setGameMaps(terrainsSmall.length, terrainsSmall[0].length+yLaneCounterExpand, terrainsCurrent);   
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should expand game map with 2 x and y lanes.', function() {
        const terrainsSmall = TestData.getTerrainsSmall();
        const terrainsExpected = TestData.getTerrainsSmallExtendedXY();
        const laneCounterExpand = 2;
        let terrainsCurrent;

        // execution / Ausfuehrung
        Model.setGameMaps(terrainsSmall.length, terrainsSmall[0].length, terrainsSmall);
        terrainsCurrent = Model.getTerrains();
        Model.setGameMaps(terrainsSmall.length + laneCounterExpand, terrainsSmall[0].length + laneCounterExpand, terrainsCurrent);   
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should shrink game map with 2 x lanes.', function() {
        const terrainsMedium = TestData.getTerrainsMedium();
        const terrainsExpected = TestData.getTerrainsMediumShrinkedX();
        const xLaneCounterShrink = 2;
        let terrainsCurrent;

        // execution / Ausfuehrung
        Model.setGameMaps(terrainsMedium.length, terrainsMedium[0].length, terrainsMedium);
        terrainsCurrent = Model.getTerrains();
        Model.setGameMaps(terrainsMedium.length - xLaneCounterShrink, terrainsMedium[0].length, terrainsCurrent);
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should shrink game map with 2 y lanes.', function() {
        const terrainsMedium = TestData.getTerrainsMedium();
        const terrainsExpected = TestData.getTerrainsMediumShrinkedY();
        const yLaneCounterShrink = 2;
        let terrainsCurrent;

        // execution / Ausfuehrung
        Model.setGameMaps(terrainsMedium.length, terrainsMedium[0].length, terrainsMedium);
        terrainsCurrent = Model.getTerrains();
        Model.setGameMaps(terrainsMedium.length, terrainsMedium[0].length - yLaneCounterShrink, terrainsCurrent);  
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should shrink game map with 2 x and y lanes.', function() {
        const terrainsMedium = TestData.getTerrainsMedium();
        const terrainsExpected = TestData.getTerrainsMediumShrinkedXY();
        const laneCounterShrink = 2;
        let terrainsCurrent;

        // execution / Ausfuehrung
        Model.setGameMaps(terrainsMedium.length, terrainsMedium[0].length, terrainsMedium);
        terrainsCurrent = Model.getTerrains();
        Model.setGameMaps(terrainsMedium.length - laneCounterShrink, terrainsMedium[0].length - laneCounterShrink, terrainsCurrent);
        terrainsCurrent = Model.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    // stored view selections
    it('Should get setable element id.', function() {
        const elementIdExpected = 'none';
        let elementIdCurrent;

        // execution / Ausfuehrung
        elementIdCurrent = Model.getSetableElement();
        

        expect(elementIdExpected).toEqual(elementIdCurrent);
    });

    it('Should set setable element id.', function() {
        const elementIdExpected = 'city';
        const elementIdDefaultValue = 'none';
        let elementIdCurrent;

        // execution / Ausfuehrung
        Model.setSetableElement(elementIdExpected);
        elementIdCurrent = Model.getSetableElement();
    
        expect(elementIdExpected).toEqual(elementIdCurrent);

        // reset
        Model.setSetableElement(elementIdDefaultValue);
    });

    it('Should get current unit / terrain selection.', function() {
        const selectionIdExpected = 'setTerrainDialog';
        let selectionIdCurrent;

        // execution / Ausfuehrung
        selectionIdCurrent = Model.getUnitOrTerrainDialogSelection();

        expect(selectionIdExpected).toEqual(selectionIdCurrent);
    });

    it('Should set current Unit / Terrain selection.', function() {
        const selectionIdExpected = 'setUnitDialog';
        const selectionIdDefaultValue = 'setTerrainDialog';
        let selectionIdCurrent;

        // execution / Ausfuehrung
        Model.setUnitOrTerrainDialogSelection(selectionIdExpected);
        selectionIdCurrent = Model.getUnitOrTerrainDialogSelection();
        
        expect(selectionIdExpected).toEqual(selectionIdCurrent);

        // reset
        Model.setUnitOrTerrainDialogSelection(selectionIdDefaultValue);
    });
});