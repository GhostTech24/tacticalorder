/**
 * eng: test class. Contains unit-tests for scenarios.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// required gameMap.js, TestData.js, unit.js, terrain.js, player.js
describe('eng: Testsuite for the scenario module. ger: Testsuite fuer das Scenario-Modul.', function() {
    /**
     * @type {[[object]]} 2D array of the game map terrain
     */
    let terrainsCurrent;
    /**
     * @type {[[object]]} 2D array of the game map units
     */
    let unitsCurrent;

    beforeEach(function() {
        Scenario.resetScenario();

        terrainsCurrent = undefined;
        unitsCurrent = undefined;
    });
    
    // scenario
    it('Should get default scenario.', function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioDefault0Player();
        let scenarioCurrent

        // execution / Ausfuehrung  
        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should get scenario after 1 player was added to default scenario.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const scenarioExpected = TestData.getScenarioDefault1Player1LosingCondition();
        let scenarioCurrent

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);  
        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should get scenario after 1 player and second losing condition was added to default scenario.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const scenarioExpected = TestData.getScenarioDefault1Player2LosingConditions();
        const losingCondition2 = scenarioExpected.losingConditions.get(player1.id)[1];
        const losingCondition2Id = losingCondition2.id;
        const losingCondition2XCoordinate = losingCondition2.xCoordinate;
        const losingCondition2YCoordinate = losingCondition2.yCoordinate;
        let scenarioCurrent

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);  
        Scenario.addLosingCondition(player1.id, losingCondition2Id, losingCondition2XCoordinate, losingCondition2YCoordinate);
        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should set scenario \'Nuke Town\'.', function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioNuketown2PlayersEmpty();
        let scenarioCurrent

        // execution / Ausfuehrung
        Scenario.setScenario(scenarioExpected);
        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    it('Should reset scenario.', function() {
        // variables / Variablen
        const scenarioExpected = TestData.getScenarioDefault0Player();
        const scenarioBevore = TestData.getScenarioNuketown2PlayersEmpty();
        let scenarioCurrent

        // execution / Ausfuehrung
        Scenario.setScenario(scenarioBevore);

        Scenario.resetScenario();
        scenarioCurrent = Scenario.getScenario();

        expect(scenarioExpected).toEqual(scenarioCurrent);
    });

    // name
    it('Should get scenario name.', function() {
        // variables / Variablen
        const nameExpected = 'newScenario';
        let nameCurrent

        // execution / Ausfuehrung
        Scenario.setName();
        nameCurrent = Scenario.getName();

        expect(nameExpected).toEqual(nameCurrent);
    });

    it('Should set scenario name.', function() {
        // variables / Variablen
        const nameExpected = 'The First Encounter';
        let nameCurrent

        // execution / Ausfuehrung
        Scenario.setName(nameExpected);
        nameCurrent = Scenario.getName();

        expect(nameExpected).toEqual(nameCurrent);
    });

    // player
    it('Should reset players at start.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const player2 = TestData.getPlayer2Default();
        const playersStartSizeExpected = 0;
        let playerStartSizeCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(player2.id, player2);

        Scenario.resetPlayersStart();
        playerStartSizeCurrent = Scenario.getPlayersStartSize();

        expect(playersStartSizeExpected).toEqual(playerStartSizeCurrent);
    });

    it('Should get players size at start.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const player2 = TestData.getPlayer2Default();
        const player3 = TestData.getPlayer3Default();
        const playersStartSizeExpected = 3;
        let playerStartSizeCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(player2.id, player2);
        Scenario.setPlayerStart(player3.id, player3);

        playerStartSizeCurrent = Scenario.getPlayersStartSize();

        expect(playersStartSizeExpected).toEqual(playerStartSizeCurrent);
    });

    it('Should get players at start.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const player2 = TestData.getPlayer2Default();
        const player3 = TestData.getPlayer3Default();
        const playersExpected = TestData.getPlayersStartWith3Default();
        let playersCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(player2.id, player2);
        Scenario.setPlayerStart(player3.id, player3);

        playersCurrent = Scenario.getPlayersStart();

        expect(playersExpected).toEqual(playersCurrent);
    });

    it('Should get player at start.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const playerExpected = TestData.getPlayer2Default();
        const player3 = TestData.getPlayer3Default();
        let playerCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(playerExpected.id, playerExpected);
        Scenario.setPlayerStart(player3.id, player3);

        playerCurrent = Scenario.getPlayerStart(playerExpected.id);

        expect(playerExpected).toEqual(playerCurrent);
    });

    it('Should set player initial at start.', function() {
        // variables / Variablen
        const playerExpected = TestData.getPlayer1Default();
        let playerCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(playerExpected.id, playerExpected);

        playerCurrent = Scenario.getPlayerStart(playerExpected.id);

        expect(playerExpected).toEqual(playerCurrent);
    });

    it('Should set player overwriting at start.', function() {
        // variables / Variablen
        const playerId = 1;
        const playerToOverwrite = TestData.getPlayer1Default();
        const playerExpected = TestData.getPlayer2Default();
        let playerCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(playerId, playerToOverwrite);
        Scenario.setPlayerStart(playerId, playerExpected);

        playerCurrent = Scenario.getPlayerStart(playerId);

        expect(playerExpected).toEqual(playerCurrent);
    });

    it('Should delete player at start (focus on players, editation change).', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const player2 = TestData.getPlayer2Default();
        const player3 = TestData.getPlayer3Default();
        const player4 = TestData.getPlayer4Default();
        const playersExpected = TestData.getPlayersStartWith3Default();
        let playersCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(player2.id, player2);
        Scenario.setPlayerStart(player3.id, player3);
        Scenario.setPlayerStart(player4.id, player4);

        Scenario.deletePlayerStart(player4.id);
        playersCurrent = Scenario.getPlayersStart();

        expect(playersExpected).toEqual(playersCurrent);
    });

    it('Should delete player at start (focus on players, defeated player).', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const player2Defeated = TestData.getPlayer2Default();
        const player3 = TestData.getPlayer3Default();
        const player4 = TestData.getPlayer4Default();
        const playersExpected = TestData.getPlayersStartWith3Player2Defeated();
        let playersCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(player2Defeated.id, player2Defeated);
        Scenario.setPlayerStart(player3.id, player3);
        Scenario.setPlayerStart(player4.id, player4);

        Scenario.deletePlayerStart(player2Defeated.id);
        playersCurrent = Scenario.getPlayersStart();

        expect(playersExpected).toEqual(playersCurrent);
    });

    it('Should delete player at start (focus on owned units).', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const player2Defeated = TestData.getPlayer2Default();
        const unit1 = Unit.getUnit('infantry');
        const unit1XCoordinate = 2;
        const unit1YCoordinate = 4;
        const unit2 = Unit.getUnit('lightTank');
        const unit2XCoordinate = 9;
        const unit2YCoordinate = 9;
        const unit3 = Unit.getUnit('infantry');
        const unit3XCoordinate = 0;
        const unit3YCoordinate = 0;
        const unitsExpected = TestData.getUnitsPlacedMapDefault();
        let unitsCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.setPlayerStart(player2Defeated.id, player2Defeated);

        Scenario.setUnitStart(unit1XCoordinate, unit1YCoordinate, unit1);
        Scenario.setUnitStart(unit2XCoordinate, unit2YCoordinate, unit2);
        unit3.ownerCurrent = 2;
        Scenario.setUnitStart(unit3XCoordinate, unit3YCoordinate, unit3);

        Scenario.deletePlayerStart(player2Defeated.id);
        unitsCurrent = Scenario.getUnitsStart();

        expect(unitsExpected).toEqual(unitsCurrent);
    });

    // losing condition
    it('Should get default losing condition.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionXCoordinateExpected = null;
        const losingConditionYCoordinateExpected = null;
        const losingConditionsLengthExpected = 1;
        let losingConditions, losingConditionCurrent, losingConditionIdCurrent, losingConditionXCoordinateCurrent, losingConditionYCoordinateCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        
        losingConditions = Scenario.getLosingConditions(player1.id);
        losingConditionCurrent = losingConditions[0];
        losingConditionIdCurrent = losingConditionCurrent.id;
        losingConditionXCoordinateCurrent = losingConditionCurrent.xCoordinate;
        losingConditionYCoordinateCurrent = losingConditionCurrent.yCoordinate;
        losingConditionsLengthCurrent = losingConditions.length;

        expect(losingConditionIdExpected).toBe(losingConditionIdCurrent);
        expect(losingConditionXCoordinateExpected).toBe(losingConditionXCoordinateCurrent, 'X coordinate of losing condition is not as expected! Expected was: |'+ losingConditionXCoordinateExpected + '|  but is |' + losingConditionXCoordinateCurrent + '|');
        expect(losingConditionYCoordinateExpected).toBe(losingConditionYCoordinateCurrent, 'X coordinate of losing condition is not as expected! Expected was: |'+ losingConditionYCoordinateExpected + '|  but is |' + losingConditionYCoordinateCurrent + '|');
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should add losing condition as new entry.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'unitReachedField';
        const losingConditionXCoordinateExpected = 3;
        const losingConditionYCoordinateExpected = 4;
        const losingConditionsLengthExpected = 2;
        let losingConditions, losingConditionCurrent, losingConditionIdCurrent, losingConditionXCoordinateCurrent, losingConditionYCoordinateCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);

        Scenario.addLosingCondition(player1.id, losingConditionIdExpected, losingConditionXCoordinateExpected, losingConditionYCoordinateExpected);
        losingConditions = Scenario.getLosingConditions(player1.id);
        losingConditionCurrent = losingConditions[1];
        losingConditionIdCurrent = losingConditionCurrent.id;
        losingConditionXCoordinateCurrent = losingConditionCurrent.xCoordinate;
        losingConditionYCoordinateCurrent = losingConditionCurrent.yCoordinate;
        losingConditionsLengthCurrent = losingConditions.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionXCoordinateExpected).toBe(losingConditionXCoordinateCurrent, 'X coordinate of losing condition is not as expected! Expected was: |'+ losingConditionXCoordinateExpected + '|  but is |' + losingConditionXCoordinateCurrent + '|');
        expect(losingConditionYCoordinateExpected).toBe(losingConditionYCoordinateCurrent, 'X coordinate of losing condition is not as expected! Expected was: |'+ losingConditionYCoordinateExpected + '|  but is |' + losingConditionYCoordinateCurrent + '|');
        expect(losingConditionsLengthExpected).toBe(losingConditionsLengthCurrent);
    });

    it('Should add losing condition as overwriting entry.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'unitReachedField';
        const xCoordinateBevore = 0;
        const xCoordinateExpected = 3;
        const yCoordinateBevore = 0;
        const yCoordinateExpected = 4;
        const losingConditionsLengthExpected = 2;
        let losingConditionsCurrent, losingConditionIdCurrent, xCoordinateCurrent, yCoordinateCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.addLosingCondition(player1.id, losingConditionIdExpected, xCoordinateBevore, yCoordinateBevore);

        Scenario.addLosingCondition(player1.id, losingConditionIdExpected, xCoordinateExpected, yCoordinateExpected);

        losingConditionsCurrent = Scenario.getLosingConditions(player1.id);
        losingConditionIdCurrent = losingConditionsCurrent[1].id;
        xCoordinateCurrent = losingConditionsCurrent[1].xCoordinate;
        yCoordinateCurrent = losingConditionsCurrent[1].yCoordinate;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toBe(losingConditionIdCurrent);
        expect(xCoordinateExpected).toBe(xCoordinateCurrent);
        expect(yCoordinateExpected).toBe(yCoordinateCurrent);
        expect(losingConditionsLengthExpected).toBe(losingConditionsLengthCurrent);
    });

    it('Should add losing condition \'unit reached field\' without given coordinates.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'unitReachedField';
        const losingConditionXCoordinateExpected = 0;
        const losingConditionYCoordinateExpected = 0;
        const losingConditionsLengthExpected = 2;
        let losingConditions, losingConditionCurrent, losingConditionIdCurrent, losingConditionXCoordinateCurrent, losingConditionYCoordinateCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);

        Scenario.addLosingCondition(player1.id, losingConditionIdExpected);
        losingConditions = Scenario.getLosingConditions(player1.id);
        losingConditionCurrent = losingConditions[1];
        losingConditionIdCurrent = losingConditionCurrent.id;
        losingConditionXCoordinateCurrent = losingConditionCurrent.xCoordinate;
        losingConditionYCoordinateCurrent = losingConditionCurrent.yCoordinate;
        losingConditionsLengthCurrent = losingConditions.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionXCoordinateExpected).toBe(losingConditionXCoordinateCurrent, 'X coordinate of losing condition is not as expected! Expected was: |'+ losingConditionXCoordinateExpected + '|  but is |' + losingConditionXCoordinateCurrent + '|');
        expect(losingConditionYCoordinateExpected).toBe(losingConditionYCoordinateCurrent, 'X coordinate of losing condition is not as expected! Expected was: |'+ losingConditionYCoordinateExpected + '|  but is |' + losingConditionYCoordinateCurrent + '|');
        expect(losingConditionsLengthExpected).toBe(losingConditionsLengthCurrent);
    });

    it('Should delete first losing condition from player with two losing conditions.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'unitReachedField';
        const losingConditionIdDelete = 'allUnitsLost'; 
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.addLosingCondition(player1.id, losingConditionIdExpected);
        
        Scenario.deleteLosingCondition(player1.id, losingConditionIdDelete)

        losingConditionsCurrent = Scenario.getLosingConditions(player1.id)
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should delete second losing condition from player with two losing conditions.', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionIdDelete = 'unitReachedField'; 
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.addLosingCondition(player1.id, losingConditionIdDelete);
        
        Scenario.deleteLosingCondition(player1.id, losingConditionIdDelete)

        losingConditionsCurrent = Scenario.getLosingConditions(player1.id)
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should delete losing condition from player with default losing condition (get default losing condition).', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);

        Scenario.deleteLosingCondition(player1.id, losingConditionIdExpected);

        losingConditionsCurrent = Scenario.getLosingConditions(player1.id)
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should delete losing condition from player with one different losing condition (get default losing condition).', function() {
        // variables / Variablen
        const player1 = TestData.getPlayer1Default();
        const losingConditionIdExpected = 'allUnitsLost';
        const losingConditionIdBevore = 'unitReachedField'; 
        const losingConditionsLengthExpected = 1;
        let losingConditionsCurrent, losingConditionIdCurrent, losingConditionsLengthCurrent;

        // execution / Ausfuehrung
        Scenario.setPlayerStart(player1.id, player1);
        Scenario.addLosingCondition(player1.id, losingConditionIdBevore);  
        Scenario.deleteLosingCondition(player1.id, losingConditionIdExpected);

        Scenario.deleteLosingCondition(player1.id, losingConditionIdBevore);

        losingConditionsCurrent = Scenario.getLosingConditions(player1.id)
        losingConditionIdCurrent = losingConditionsCurrent[0].id;
        losingConditionsLengthCurrent = losingConditionsCurrent.length;

        expect(losingConditionIdExpected).toEqual(losingConditionIdCurrent);
        expect(losingConditionsLengthExpected).toEqual(losingConditionsLengthCurrent);
    });

    it('Should get positiv response for losing condition \'allUnitsLost\' after setting the whole scenario.', function() {
        // variables / Variablen
        const scenarioTemplate = TestData.getScenarioDefault1Player1LosingCondition();
        const playerId = 1;
        const hasLostExpected = true;
        const losingConditionId = 'allUnitsLost';
        const losingConditionIdBevore = 'unitReachedField'; 
        const losingConditionsLengthExpected = 1;
        let hasLostCurrent;

        // execution / Ausfuehrung
        Scenario.setScenario(scenarioTemplate);
        
        const losingConditionCurrent = Scenario.getLosingConditions(playerId);
        hasLostCurrent = losingConditionCurrent[0].conditionCheck();

        expect(hasLostExpected).toEqual(hasLostCurrent);
    });
    // TODO setting whole scenario, where units are, than conditionCheck (for unitReached field too)
    // TODO copy checks from losing condition spec to get positive negative response from conditionCheck 

    // terrains / units
    it('Should get terrains from initial default game map (size / terrain).', function() {
        // variables / Variablen
        const terrainsExpected = TestData.getTerrainsDefault();
        terrainsCurrent = Scenario.getTerrains();

        // execution / Ausfuehrung
        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should get units from default game map (size / units) at the start of an scenario.', function() {
        // variables / Variablen
        const unitsStartExpected = TestData.getUnitsEmptyMapDefault();
        unitsCurrent = Scenario.getUnitsStart();

        // execution / Ausfuehrung
        expect(unitsStartExpected).toEqual(unitsCurrent);
    });

    it('Should get terrains from initial custom game map (size / terrain).', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSmall();
        
        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, terrainsExpected);
        terrainsCurrent = Scenario.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should get units (empty) from custom game map (size / units) at the start of an scenario.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSmall();
        const unitsStartExpected = TestData.getUnitsEmptyMapSmall();
        

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, terrainsExpected, unitsStartExpected);
        unitsCurrent = Scenario.getUnitsStart();

        expect(unitsStartExpected).toEqual(unitsCurrent);
    });

    it('Should get units (placed) from custom game map (size / units) at the start of an scenario.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSmall();
        const unitsStartExpected = TestData.getUnitsPlacedMapSmall();
        

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, terrainsExpected, unitsStartExpected);
        unitsCurrent = Scenario.getUnitsStart();

        expect(unitsStartExpected).toEqual(unitsCurrent);
    });

    it('Should get sizes from terrains of an previous created game map with fixed size.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        let xFieldsCurrent, yFieldsCurrent;

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected);
        
        terrainsCurrent = Scenario.getTerrains();
        xFieldsCurrent = terrainsCurrent.length;
        yFieldsCurrent = terrainsCurrent[0].length;
        
        expect(xFieldsExpected).toBe(xFieldsCurrent, 'Invalid X field counter of terrains! Should be |' + xFieldsExpected + '| but was |' + xFieldsCurrent + '|');
        expect(yFieldsExpected).toBe(yFieldsCurrent, 'Invalid Y field counter of terrains! Should be |' + yFieldsExpected + '| but was |' + yFieldsCurrent + '|');
    });

    it('Should get sizes from units of an previous created game map with fixed size.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        let xFieldsCurrent, yFieldsCurrent;

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected);

        unitsCurrent = Scenario.getUnitsStart();
        xFieldsCurrent = unitsCurrent.length;
        yFieldsCurrent = unitsCurrent[0].length;
        
        expect(xFieldsExpected).toBe(xFieldsCurrent, 'Invalid X field counter of units! Should be |' + xFieldsExpected + '| but was |' + xFieldsCurrent + '|');
        expect(yFieldsExpected).toBe(yFieldsCurrent, 'Invalid Y field counter of units! Should be |' + yFieldsExpected + '| but was |' + yFieldsCurrent + '|');
        
    });

    it('Should get specific terrain from custom game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();   
        const terrainXCoordinate = 0;
        const terrainYCoordinate = 1;
        const terrainExpected = Terrain.getTerrain('city');
        let terrainCurrent;

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        terrainCurrent = Scenario.getTerrain(terrainXCoordinate, terrainYCoordinate);

        expect(terrainExpected).toEqual(terrainCurrent);
    });

    it('Should set specific terrain for game map with custom size / terrain.', function() {
        // variables / Variablen
        const xFieldsExpected = 4;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSquaredChanged();
        const terrainsBeginning = TestData.getTerrainsSquared();
        const terrainXCoordinate = 3;
        const terrainYCoordinate = 2;
        const terrainNew = Terrain.getTerrain('city');     

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, terrainsBeginning);
        Scenario.setTerrain(terrainXCoordinate, terrainYCoordinate, terrainNew)
        terrainsCurrent = Scenario.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should get terrains from manual created custom game map (size / terrain).', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const terrainsExpected = TestData.getTerrainsSmall();
        let terrainCurrent

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected);
        
        for(let x = 0; x < xFieldsExpected; x++) {
            for(let y = 0; y < yFieldsExpected; y++) {
                terrainCurrent = terrainsExpected[x][y];
                
                Scenario.setTerrain(x, y, terrainCurrent);
            }
        }

        terrainsCurrent = Scenario.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should get units from manual created custom game map (size / terrain).', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const terrainsUsed = TestData.getTerrainsSmall();
        const unitsExpected = TestData.getUnitsPlacedMapSmall();
        let unitCurrent

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, terrainsUsed);
        
        for(let x = 0; x < xFieldsExpected; x++) {
            for(let y = 0; y < yFieldsExpected; y++) {
                unitCurrent = unitsExpected[x][y];
                
                Scenario.setUnitStart(x, y, unitCurrent);
            }
        }
        unitsCurrent = Scenario.getUnitsStart();

        expect(unitsExpected).toEqual(unitsCurrent);
    });

    it('Should expand a custom game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const expandLaneCounter = 2;
        const terrainsExpected = TestData.getTerrainsSmallExtendedXY();
        

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        terrainsCurrent = Scenario.getTerrains();

        Scenario.setGameMaps(xFieldsExpected + expandLaneCounter, yFieldsExpected + expandLaneCounter, terrainsCurrent);
        terrainsCurrent = Scenario.getTerrains();
        
        
        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should shrink a custom game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 8;
        const yFieldsExpected = 6;
        const terrainsMedium = TestData.getTerrainsMedium();
        const shrinkLaneCounter = 2;      
        const terrainsExpected = TestData.getTerrainsMediumShrinkedXY();

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, terrainsMedium);
        terrainsCurrent = Scenario.getTerrains();

        Scenario.setGameMaps(xFieldsExpected - shrinkLaneCounter, yFieldsExpected - shrinkLaneCounter, terrainsCurrent);
        terrainsCurrent = Scenario.getTerrains();

        expect(terrainsExpected).toEqual(terrainsCurrent);
    });

    it('Should set and get a unit of an specific field from the initial game map.', function() {
        // variables / Variablen 
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const unitXCoordinate = 1;
        const unitYCoordinate = 2;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        Scenario.setUnitStart(unitXCoordinate, unitYCoordinate, unitExpected);
        
        unitCurrent = Scenario.getUnitStart(unitXCoordinate, unitYCoordinate);
        
        expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should replace and get the setted unit of an specific field from the initial game map.', function() {
        // variables / Variablen
        const xFieldsExpected = 3;
        const yFieldsExpected = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const unitReplaced = Unit.getUnit('infantry');
        const unitExpected = Unit.getUnit('heavyInfantry');
        let unitCurrent;

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsExpected, yFieldsExpected, mapSmall);
        Scenario.setUnitStart(unitXCoordinate, unitYCoordinate, unitReplaced)
        Scenario.setUnitStart(unitXCoordinate, unitYCoordinate, unitExpected)
        
        
        unitCurrent = Scenario.getUnitStart(unitXCoordinate, unitYCoordinate);
        
        expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should get a unit of an specific field after a previous smaller game map was enlarged at X and Y.', function() {
        // variables / Variablen 
        const xFieldsBevore = 3;
        const yFieldsBevore = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const counterExpand = 2;
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent;

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsBevore, yFieldsBevore, mapSmall);
        Scenario.setUnitStart(unitXCoordinate, unitYCoordinate, unitExpected);
        unitsCurrent = Scenario.getUnitsStart();

        Scenario.setGameMaps(xFieldsBevore + counterExpand, yFieldsBevore + counterExpand, mapSmall, unitsCurrent);
        unitCurrent = Scenario.getUnitStart(unitXCoordinate, unitYCoordinate);
        
        expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should get a unit of an specific field after a previous smaller game map was enlarged at X.', function() {
         // variables / Variablen 
         const xFieldsBevore = 3;
         const yFieldsBevore = 4;
         const mapSmall = TestData.getTerrainsSmall();
         const expandXCounter = 2;
         const unitXCoordinate = 0;
         const unitYCoordinate = 3;
         const unitExpected = Unit.getUnit('infantry');
         let unitCurrent;
 
         // execution / Ausfuehrung
         Scenario.setGameMaps(xFieldsBevore, yFieldsBevore, mapSmall);
         Scenario.setUnitStart(unitXCoordinate, unitYCoordinate, unitExpected);
         unitsCurrent = Scenario.getUnitsStart();
 
         Scenario.setGameMaps(xFieldsBevore + expandXCounter, yFieldsBevore, mapSmall, unitsCurrent);
         unitCurrent = Scenario.getUnitStart(unitXCoordinate, unitYCoordinate);
         
         expect(unitExpected).toEqual(unitCurrent);
    });

    it('Should get a unit of an specific field after a previous smaller game map was enlarged at Y.', function() {
        // variables / Variablen 
        const xFieldsBevore = 3;
        const yFieldsBevore = 4;
        const mapSmall = TestData.getTerrainsSmall();
        const expandYCounter = 2;
        const unitXCoordinate = 0;
        const unitYCoordinate = 3;
        const unitExpected = Unit.getUnit('infantry');
        let unitCurrent;

        // execution / Ausfuehrung
        Scenario.setGameMaps(xFieldsBevore, yFieldsBevore, mapSmall);
        Scenario.setUnitStart(unitXCoordinate, unitYCoordinate, unitExpected);
        unitsCurrent = Scenario.getUnitsStart();

        Scenario.setGameMaps(xFieldsBevore, yFieldsBevore + expandYCounter, mapSmall, unitsCurrent);
        unitCurrent = Scenario.getUnitStart(unitXCoordinate, unitYCoordinate);
        
        expect(unitExpected).toEqual(unitCurrent);
    });
});