/**
 * eng: test class. Contains unit-tests for terrains.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: terrain.js
describe('eng: Testsuite for the terrain module. ger: Testsuite fuer das Terrain-Modul.', function() {
    
    const terrainCity = {
        id: 'city',
        name: '',
        descripition: '',
        cover: 2,
        hidesAtFogOfWar: false,
        income: 1000,
        costs: 0,
        conquerable: true,
        buildableList: null,
        produces: null,
        repairsList: ['ground'],
        specialFeature: ['fowSightByCapture'],
        movementCosts: {infantry: 1,
                        tireA: 1,
                        tireB: 1,
                        track: 1,
                        air:1,
                        ship:0,
                        ferry:0
                    }
    };
    const terrainPlane = {
        id: 'plane',
        name: '',
        descripition: '',
        cover: 1,
        hidesAtFogOfWar: false,
        income: 0,
        costs: 0,
        conquerable: false,
        buildableList: ['mobileBase', 'mobileAirport'],
        produces: null,
        repairsList: null,
        specialFeature: null,
        movementCosts: {infantry: 1,
                        tireA: 2,
                        tireB: 1,
                        track: 1,
                        air:1,
                        ship:0,
                        ferry:0
                    }
    };
    const terrainStreet = {
        id: 'street',
        name: '',
        descripition: '',
        cover: 0,
        hidesAtFogOfWar: false,
        income: 0,
        costs: 0,
        conquerable: false,
        buildableList: null,
        produces: null,
        repairsList: null,
        specialFeature: null,
        movementCosts: {infantry: 1,
                        tireA: 1,
                        tireB: 1,
                        track: 1,
                        air:1,
                        ship:0,
                        ferry:0
                    }
    };
    const terrainWoods = {
        id: 'woods',
        name: '',
        descripition: '',
        cover: 3,
        hidesAtFogOfWar: true,
        income: 0,
        costs: 0,
        conquerable: false,
        buildableList: null,
        produces: null,
        repairsList: null,
        specialFeature: null,
        movementCosts: {infantry: 1,
                        tireA: 3,
                        tireB: 3,
                        track: 2,
                        air:1,
                        ship:0,
                        ferry:0
                    }
    };
    const terrainSea = {
        id: 'sea',
        name: '',
        descripition: '',
        cover: 0,
        hidesAtFogOfWar: false,
        income: 0,
        costs: 0,
        conquerable: false,
        buildableList: null,
        produces: null,
        repairsList: null,
        specialFeature: null,
        movementCosts: {infantry: 0,
                        tireA: 0,
                        tireB: 0,
                        track: 0,
                        air: 1,
                        ship: 1,
                        ferry: 1
                    }
    };
    const terrainMountain = {
        id: 'mountain',
        name: '',
        descripition: '',
        cover: 4,
        hidesAtFogOfWar: false,
        income: 0,
        costs: 0,
        conquerable: false,
        buildableList: null,
        produces: null,
        repairsList: null,
        specialFeature: null,
        movementCosts: {infantry: 2,
                        tireA: 0,
                        tireB: 0,
                        track: 0,
                        air: 1,
                        ship: 0,
                        ferry: 0
                    }
    };

    it('Should get city terrain.', function() {
        // variables / Variablen
        const terrainExpected = terrainCity;
        const idExpected = 'city';     

        // execution / Ausfuehrung
        const terrainCurrent = Terrain.getTerrain(idExpected);

        expect(terrainExpected.id).toEqual(terrainCurrent.id);
        expect(terrainExpected.cover).toEqual(terrainCurrent.cover);
        expect(terrainExpected.hidesAtFogOfWar).toEqual(terrainCurrent.hidesAtFogOfWar);
        expect(terrainExpected.income).toEqual(terrainCurrent.income);
        expect(terrainExpected.costs).toEqual(terrainCurrent.costs);
        expect(terrainExpected.conquerable).toEqual(terrainCurrent.conquerable);
        expect(terrainExpected.buildableList).toEqual(terrainCurrent.buildableList);
        expect(terrainExpected.produces).toEqual(terrainCurrent.produces);
        expect(terrainExpected.repairsList).toEqual(terrainCurrent.repairsList);
        expect(terrainExpected.specialFeature).toEqual(terrainCurrent.specialFeature);
        expect(terrainExpected.movementCostsList).toEqual(terrainCurrent.movementCostsList);
    });

    it('Should get plane terrain.', function() {
        // variables / Variablen
        const terrainExpected = terrainPlane;
        const idExpected = 'plane';

        // execution / Ausfuehrung
        const terrainCurrent = Terrain.getTerrain(idExpected);

        expect(terrainExpected.id).toEqual(terrainCurrent.id);
        expect(terrainExpected.cover).toEqual(terrainCurrent.cover);
        expect(terrainExpected.hidesAtFogOfWar).toEqual(terrainCurrent.hidesAtFogOfWar);
        expect(terrainExpected.income).toEqual(terrainCurrent.income);
        expect(terrainExpected.costs).toEqual(terrainCurrent.costs);
        expect(terrainExpected.conquerable).toEqual(terrainCurrent.conquerable);
        expect(terrainExpected.buildableList).toEqual(terrainCurrent.buildableList);
        expect(terrainExpected.produces).toEqual(terrainCurrent.produces);
        expect(terrainExpected.repairsList).toEqual(terrainCurrent.repairsList);
        expect(terrainExpected.specialFeature).toEqual(terrainCurrent.specialFeature);
        expect(terrainExpected.movementCostsList).toEqual(terrainCurrent.movementCostsList);
    });

    it('Should get street terrain.', function() {
        // variables / Variablen
        const terrainExpected = terrainStreet;
        const idExpected = 'street';   

        // execution / Ausfuehrung
        const terrainCurrent = Terrain.getTerrain(idExpected);

        expect(terrainExpected.id).toEqual(terrainCurrent.id);
        expect(terrainExpected.cover).toEqual(terrainCurrent.cover);
        expect(terrainExpected.hidesAtFogOfWar).toEqual(terrainCurrent.hidesAtFogOfWar);
        expect(terrainExpected.income).toEqual(terrainCurrent.income);
        expect(terrainExpected.costs).toEqual(terrainCurrent.costs);
        expect(terrainExpected.conquerable).toEqual(terrainCurrent.conquerable);
        expect(terrainExpected.buildableList).toEqual(terrainCurrent.buildableList);
        expect(terrainExpected.produces).toEqual(terrainCurrent.produces);
        expect(terrainExpected.repairsList).toEqual(terrainCurrent.repairsList);
        expect(terrainExpected.specialFeature).toEqual(terrainCurrent.specialFeature);
        expect(terrainExpected.movementCostsList).toEqual(terrainCurrent.movementCostsList);
    });

    it('Should get woods terrain.', function() {
        // variables / Variablen
        const terrainExpected = terrainWoods;
        const idExpected = 'woods';   

        // execution / Ausfuehrung
        const terrainCurrent = Terrain.getTerrain(idExpected);

        expect(terrainExpected.id).toEqual(terrainCurrent.id);
        expect(terrainExpected.cover).toEqual(terrainCurrent.cover);
        expect(terrainExpected.hidesAtFogOfWar).toEqual(terrainCurrent.hidesAtFogOfWar);
        expect(terrainExpected.income).toEqual(terrainCurrent.income);
        expect(terrainExpected.costs).toEqual(terrainCurrent.costs);
        expect(terrainExpected.conquerable).toEqual(terrainCurrent.conquerable);
        expect(terrainExpected.buildableList).toEqual(terrainCurrent.buildableList);
        expect(terrainExpected.produces).toEqual(terrainCurrent.produces);
        expect(terrainExpected.repairsList).toEqual(terrainCurrent.repairsList);
        expect(terrainExpected.specialFeature).toEqual(terrainCurrent.specialFeature);
        expect(terrainExpected.movementCostsList).toEqual(terrainCurrent.movementCostsList);
    });

    it('Should get sea terrain.', function() {
        // variables / Variablen
        const terrainExpected = terrainSea;
        const idExpected = 'sea';   

        // execution / Ausfuehrung
        const terrainCurrent = Terrain.getTerrain(idExpected);

        expect(terrainExpected.id).toEqual(terrainCurrent.id);
        expect(terrainExpected.cover).toEqual(terrainCurrent.cover);
        expect(terrainExpected.hidesAtFogOfWar).toEqual(terrainCurrent.hidesAtFogOfWar);
        expect(terrainExpected.income).toEqual(terrainCurrent.income);
        expect(terrainExpected.costs).toEqual(terrainCurrent.costs);
        expect(terrainExpected.conquerable).toEqual(terrainCurrent.conquerable);
        expect(terrainExpected.buildableList).toEqual(terrainCurrent.buildableList);
        expect(terrainExpected.produces).toEqual(terrainCurrent.produces);
        expect(terrainExpected.repairsList).toEqual(terrainCurrent.repairsList);
        expect(terrainExpected.specialFeature).toEqual(terrainCurrent.specialFeature);
        expect(terrainExpected.movementCostsList).toEqual(terrainCurrent.movementCostsList);
    });

    it('Should get all terrains.', function() {
        // variables / Variablen
        const terrainIds =          ['city',        'plane',        'street',       'woods',        'sea',      'mountain'      ];
        const expectedTerrains =    [terrainCity,   terrainPlane,   terrainStreet,  terrainWoods,   terrainSea, terrainMountain ];
        const expectedTerrainsSize = expectedTerrains.length;   

        // execution / Ausfuehrung
        const terrainCurrents = Terrain.getAllTerrains();
        const terrainCurrentsSize = terrainCurrents.size;
        let terrainExpected, terrainCurrentId, terrainCurrent;
        
        expect(expectedTerrainsSize).toEqual(terrainCurrentsSize);

        for(let i = 0; i < expectedTerrainsSize; i++) {
            terrainExpected = expectedTerrains[i];
            terrainCurrentId = terrainIds[i];

            terrainCurrent = terrainCurrents.get(terrainCurrentId);

            expect(terrainExpected).toEqual(terrainCurrent);
        }
    });
});