/**
 * eng: test class. Contains unit-tests for units.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// required unit.js
describe('eng: Testsuite for the unit module. ger: Testsuite fuer das Unit-Modul.', function() {
    
    const unitInfantry = {
        id: 'infantry',
        unitType: 'ground',
        name: '',
        descripition: '',
        armorType: 'lightBodyArmor',
        visability: 2,
        costs: 1500,
        activeAbilitys: ['Conquer'],
        passiveAbilitys: ['MountainSight'],
        movement: 3,
        movementType: 'infantry',
        fuel: 60,
        weapon1: {
            id: 'assaultRifle',
            name: '',
            descripition: '',
            ammo: 9,
            rangeMin: 1,
            rangeMax: 1,
            damageList: [55, 50, 45,  8,  4,  2, 20,  8,  4,  0,  0,  0,  0,  0],
            currentAmmo: 9
        },
        weapon2: null,
        weapon3: null,
        currentHP: 100,
        currentVisability: 2,
        currentCosts: 1500,
        currentMovement: 3,
        currentFuel: 60,
        ownerCurrent: 1
    };

    const unitHeavyInfantry = {
        id: 'heavyInfantry',
        unitType: 'ground',
        name: '',
        descripition: '',
        armorType: 'heavyBodyArmor',
        visability: 2,
        costs: 2500,
        activeAbilitys: ['Conquer'],
        passiveAbilitys: ['MountainSight'],
        movement: 2,
        movementType: 'infantry',
        fuel: 40,
        weapon1: {
            id: 'bazooka',
            name: '',
            descripition: '',
            ammo: 3,
            rangeMin: 1,
            rangeMax: 1,
            damageList: [65, 65, 65, 55, 25, 15,  0,  0,  0,  0,  0, 20, 15, 10],
            currentAmmo: 3
        },
        weapon2: {
            id: 'lightMG',
            name: '',
            descripition: '',
            ammo: 6,
            rangeMin: 1,
            rangeMax: 1,
            damageList: [65, 55, 50, 12,  6,  3, 30, 12,  4,  0,  0,  0,  0,  0],
            currentAmmo: 6
        },
        weapon3: null,
        currentHP: 100,
        currentVisability: 2,
        currentCosts: 2500,
        currentMovement: 2,
        currentFuel: 40,
        ownerCurrent: 1
    };
    
    const unitLightTank = {
        id: 'lightTank',
        unitType: 'ground',
        name: '',
        descripition: '',
        armorType: 'lightArmorPlates',
        visability: 1,
        costs: 7000,
        activeAbilitys: [],
        passiveAbilitys: [],
        movement: 6,
        movementType: 'track',
        fuel: 60,
        weapon1: {
            id: 'lightArmorCannon',
            name: '',
            descripition: '',
            ammo: 6,
            rangeMin: 1,
            rangeMax: 1,
            damageList: [65, 65, 65, 55, 25, 15,  0,  0,  0,  0,  0, 20, 15, 10],
            currentAmmo: 6
        },
        weapon2: {
            id: 'lightMG',
            name: '',
            descripition: '',
            ammo: 9,
            rangeMin: 1,
            rangeMax: 1,
            damageList: [65, 55, 50, 12,  6,  3, 30, 12,  4,  0,  0,  0,  0,  0],
            currentAmmo: 9
        },
        weapon3: null,
        currentHP: 100,
        currentVisability: 1,
        currentCosts: 7000,
        currentMovement: 6,
        currentFuel: 60,
        ownerCurrent: 1
    };
    

    it('Should get infantry unit.', function() {
        // variables / Variablen
        const exptecedUnit = unitInfantry;
        const expectedId = 'infantry'; 

        // execution / Ausfuehrung
        const currentUnit = Unit.getUnit(expectedId);

        expect(exptecedUnit.id).toBe(currentUnit.id);
        expect(exptecedUnit.unitType).toBe(currentUnit.unitType);
        expect(exptecedUnit.armorType).toBe(currentUnit.armorType);
        expect(exptecedUnit.visability).toBe(currentUnit.visability);
        expect(exptecedUnit.costs).toBe(currentUnit.costs);
        expect(exptecedUnit.activeAbilitys).toEqual(currentUnit.activeAbilitys);
        expect(exptecedUnit.passiveAbilitys).toEqual(currentUnit.passiveAbilitys);
        expect(exptecedUnit.movement).toBe(currentUnit.movement);
        expect(exptecedUnit.movementType).toBe(currentUnit.movementType);
        expect(exptecedUnit.fuel).toBe(currentUnit.fuel);
        expect(exptecedUnit.weapon1).toEqual(currentUnit.weapon1);
        expect(exptecedUnit.weapon2).toEqual(currentUnit.weapon2);
        expect(exptecedUnit.weapon3).toEqual(currentUnit.weapon3);
        expect(exptecedUnit.currentHP).toBe(currentUnit.currentHP);
        expect(exptecedUnit.currentFuel).toBe(currentUnit.currentFuel);
        expect(exptecedUnit.ownerCurrent).toBe(currentUnit.ownerCurrent);
    });

    it('Should get heavy infantry unit.', function() {
        // variables / Variablen
        const exptecedUnit = unitHeavyInfantry;
        const expectedId = 'heavyInfantry'; 

        // execution / Ausfuehrung
        const currentUnit = Unit.getUnit(expectedId);

        expect(exptecedUnit.id).toBe(currentUnit.id);
        expect(exptecedUnit.unitType).toBe(currentUnit.unitType);
        expect(exptecedUnit.armorType).toBe(currentUnit.armorType);
        expect(exptecedUnit.visability).toBe(currentUnit.visability);
        expect(exptecedUnit.costs).toBe(currentUnit.costs);
        expect(exptecedUnit.activeAbilitys).toEqual(currentUnit.activeAbilitys);
        expect(exptecedUnit.passiveAbilitys).toEqual(currentUnit.passiveAbilitys);
        expect(exptecedUnit.movement).toBe(currentUnit.movement);
        expect(exptecedUnit.movementType).toBe(currentUnit.movementType);
        expect(exptecedUnit.fuel).toBe(currentUnit.fuel);
        expect(exptecedUnit.weapon1).toEqual(currentUnit.weapon1);
        expect(exptecedUnit.weapon2).toEqual(currentUnit.weapon2);
        expect(exptecedUnit.weapon3).toEqual(currentUnit.weapon3);
        expect(exptecedUnit.currentHP).toBe(currentUnit.currentHP);
        expect(exptecedUnit.currentFuel).toBe(currentUnit.currentFuel);
        expect(exptecedUnit.ownerCurrent).toBe(currentUnit.ownerCurrent);
    });

    it('Should get light tank unit.', function() {
        // variables / Variablen
        const exptecedUnit = unitLightTank;
        const expectedId = 'lightTank'; 

        // execution / Ausfuehrung
        const currentUnit = Unit.getUnit(expectedId);

        expect(exptecedUnit.id).toBe(currentUnit.id);
        expect(exptecedUnit.unitType).toBe(currentUnit.unitType);
        expect(exptecedUnit.armorType).toBe(currentUnit.armorType);
        expect(exptecedUnit.visability).toBe(currentUnit.visability);
        expect(exptecedUnit.costs).toBe(currentUnit.costs);
        expect(exptecedUnit.activeAbilitys).toEqual(currentUnit.activeAbilitys);
        expect(exptecedUnit.passiveAbilitys).toEqual(currentUnit.passiveAbilitys);
        expect(exptecedUnit.movement).toBe(currentUnit.movement);
        expect(exptecedUnit.movementType).toBe(currentUnit.movementType);
        expect(exptecedUnit.fuel).toBe(currentUnit.fuel);
        expect(exptecedUnit.weapon1).toEqual(currentUnit.weapon1);
        expect(exptecedUnit.weapon2).toEqual(currentUnit.weapon2);
        expect(exptecedUnit.weapon3).toEqual(currentUnit.weapon3);
        expect(exptecedUnit.currentHP).toBe(currentUnit.currentHP);
        expect(exptecedUnit.currentFuel).toBe(currentUnit.currentFuel);
        expect(exptecedUnit.ownerCurrent).toBe(currentUnit.ownerCurrent);
    });

    it('Should get all units.', function() {
        // variables / Variablen
        const unitIds =          ['infantry',   'heavyInfantry',   'lightTank' ];
        const expectedUnits =    [unitInfantry, unitHeavyInfantry,  unitLightTank   ];
        const unitsSize = expectedUnits.length;
        let exptecedUnit, currentUnitId, currentUnit;

        // execution / Ausfuehrung
        const currentUnits = Unit.getAllUnits();

        for(let i = 0; i < unitsSize; i++) {
            exptecedUnit = expectedUnits[i];
            currentUnitId = unitIds[i];

            currentUnit = currentUnits.get(currentUnitId);

            expect(exptecedUnit).toEqual(currentUnit);
        }
    });
});