/**
 * eng: test class. Contains test data for different unit-tests.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: terrain.js, unit.js, player.js
const TestData = (function () { // encapsulation / Kapselung
    // scenarios
    const scenarioDefault = function() {
        const scenario = {
            name: 'newScenario',
            playersStart: new Map(),
            losingConditions: new Map(),
            terrains: terrainsDefault(),
            unitsStart: unitsEmptyMapDefault()
        }

        return scenario;
    };
    const scenarioDefault1Player1LosingCondition = function() {
        const player1 = player1Default();

        const scenario = {
            
            name: 'newScenario',
            playersStart: new Map([[player1.id, player1]]),
            losingConditions: new Map([[player1.id, [allUnitsLostLosingConditionSimple(player1.id)]]]),
            terrains: terrainsDefault(),
            unitsStart: unitsEmptyMapDefault()
        }

        return scenario;
    };
    const scenarioDefault2Player1LosingCondition = function() {
        const player1 = player1Default();
        const player2 = player2Default();

        const scenario = {
            
            name: 'newScenario',
            playersStart: new Map([[player1.id, player1], [player2.id, player2]]),
            losingConditions: new Map([[player1.id, [allUnitsLostLosingConditionSimple(player1.id)]], [player2.id, [allUnitsLostLosingConditionSimple(player2.id)]]]),
            terrains: terrainsDefault(),
            unitsStart: unitsEmptyMapDefault()
        }

        return scenario;
    };
    const scenarioDefault3Player1LosingCondition = function() {
        const player1 = player1Default();
        const player2 = player2Default();
        const player3 = player3Default();

        const scenario = {
            
            name: 'newScenario',
            playersStart: new Map([[player1.id, player1], [player2.id, player2], [player3.id, player3]]),
            losingConditions: new Map([
                [player1.id, [allUnitsLostLosingConditionSimple(player1.id)]],
                [player2.id, [allUnitsLostLosingConditionSimple(player2.id)]],
                [player3.id, [allUnitsLostLosingConditionSimple(player3.id)]]
            ]),
            terrains: terrainsDefault(),
            unitsStart: unitsEmptyMapDefault()
        }

        return scenario;
    };
    const scenarioDefault1Player2LosingConditions = function() {
        const player1 = player1Default();

        const scenario = {
            
            name: 'newScenario',
            playersStart: new Map([[player1.id, player1]]),
            losingConditions: new Map([[player1.id, [allUnitsLostLosingConditionSimple(player1.id), unitReachedFieldLosingConditionSimple(player1.id, 2, 3)]]]),
            terrains: terrainsDefault(),
            unitsStart: unitsEmptyMapDefault()
        }

        return scenario;
    };
    const scenarioNuketown2PlayersEmpty = function() {
        const player1 = player1Default();
        const player2 = player2Default();

        const scenario = {
            name: 'Nuke Town',
            playersStart: new Map([[player1.id, player1], [player2.id, player2]]),
            losingConditions: new Map([[player1.id, [allUnitsLostLosingConditionSimple(player1.id)]], [player2.id, [allUnitsLostLosingConditionSimple(player2.id)]]]),
            terrains: terrainsSmall(),
            unitsStart: unitsEmptyMapSmall()
        }

        return scenario;
    };
    const scenarioNuketown2PlayersPlaced = function() {
        const player1 = player1Default();
        const player2 = player2Default();

        const scenario = {
            name: 'Nuke Town',
            playersStart: new Map([[player1.id, player1], [player2.id, player2]]),
            losingConditions: new Map([[player1.id, [allUnitsLostLosingConditionSimple(player1.id)]], [player2.id, [allUnitsLostLosingConditionSimple(player2.id)]]]),
            terrains: terrainsSmall(),
            unitsStart: unitsPlacedMapSmall()
        }

        return scenario;
    };
    
    // players
    const playerTemplate = function() {
        const player = Player.getPlayerTemplate();
        const playerReferenceless = Object.assign({}, player);

        return playerReferenceless;
    };
    const player1Default = function() { // color red
        const player = Player.getPlayerTemplate();
        const playerReferenceless = Object.assign({}, player);
        playerReferenceless.id = 1;
        playerReferenceless.color = 'Red';

        return playerReferenceless;
    };
    const player2Default = function() {  // color blue
        const player = Player.getPlayerTemplate();
        const playerReferenceless = Object.assign({}, player);
        playerReferenceless.id = 2;
        playerReferenceless.color = 'Blue';

        return playerReferenceless;
    };
    const player3Default = function() {  // color green
        const player = Player.getPlayerTemplate();
        const playerReferenceless = Object.assign({}, player);
        playerReferenceless.id = 3;
        playerReferenceless.color = 'Green';

        return playerReferenceless;
    };
    const player4Default = function() {  // color green
        const player = Player.getPlayerTemplate();
        const playerReferenceless = Object.assign({}, player);
        playerReferenceless.id = 4;
        playerReferenceless.color = 'Yellow';

        return playerReferenceless;
    };
    /**
     * TODO
     * @returns {Map<string, {}>}
     */
    const playersStartWith3Default = function() {
         return new Map(
            [
                [player1Default().id, player1Default()],
                [player2Default().id, player2Default()],
                [player3Default().id, player3Default()]
            ]
        );
    };
    /**
     * TODO
     * @returns {Map<string, {}>}
     */
    const playersStartWith3Player2Defeated = function() {
        return new Map(
           [
               [player1Default().id, player1Default()],
               [player3Default().id, player3Default()],
               [player4Default().id, player4Default()]
           ]
       );
    };

    // colors
    const colors = function() {
        return ['Red', 'Blue', 'Green', 'Yellow', 'Black', 'White'];
    };

    // losing conditions
    const allUnitsLostLosingConditionSimple = function(affectedPlayerId) {
        return {id: 'allUnitsLost',  affectedPlayerId: affectedPlayerId, xCoordinate: null, yCoordinate: null};
    };
    const unitReachedFieldLosingConditionSimple = function(affectedPlayerId, xCoordinate, yCoordinate) {
        return {id: 'unitReachedField',  affectedPlayerId: affectedPlayerId, xCoordinate: xCoordinate, yCoordinate: yCoordinate}  
    };

    // terrains
    const terrainsDefault = function() { // x: 20, y: 20 ; all planes
        return [
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], // 01
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')],
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], // 05
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')],
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], // 10
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')],
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')],
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], // 15
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')],
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')],
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')], 
            [Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane'), Terrain.getTerrain('plane')] // 20
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()], // 05
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()], // 10
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()], // 15
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane(), Terrain.getPlane()]  // 20
        ]
    };
    const terrainsSmall = function() { // x: 3, y: 4 ; custom terrain
        return [ 
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('plane'),    Terrain.getTerrain('woods')     ], // 1
            [Terrain.getTerrain('street'),  Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street')    ],
            [Terrain.getTerrain('woods'),   Terrain.getTerrain('sea'),      Terrain.getTerrain('sea'),      Terrain.getTerrain('plane')     ]  // 3
        ]
    };
    const terrainsSmallExtendedX = function() { // x: 5, y: 4 ; custom terrain + 2 lanes of plane on x
        return [ 
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('plane'),    Terrain.getTerrain('woods'),    ], // 1
            [Terrain.getTerrain('street'),  Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   ],
            [Terrain.getTerrain('woods'),   Terrain.getTerrain('sea'),      Terrain.getTerrain('sea'),      Terrain.getTerrain('plane'),    ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    ]  // 5
        ]
    };
    const terrainsSmallExtendedY = function() { // x: 3, y: 6 ; custom terrain + 2 lanes of plane on y
        return [ 
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('plane'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ], // 1
            [Terrain.getTerrain('street'),  Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ],
            [Terrain.getTerrain('woods'),   Terrain.getTerrain('sea'),      Terrain.getTerrain('sea'),      Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ]
        ]
    };
    const terrainsSmallExtendedXY = function() { // x: 5, y: 6 ; custom terrain + 2 lanes of plane
        return [ 
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('plane'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ], // 1
            [Terrain.getTerrain('street'),  Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ],
            [Terrain.getTerrain('woods'),   Terrain.getTerrain('sea'),      Terrain.getTerrain('sea'),      Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ]  // 5
            // [Terrain.getPlane(),    Terrain.getCity(),      Terrain.getPlane(),     Terrain.getWoods(),     Terrain.getPlane(), Terrain.getPlane()], // 1
            // [Terrain.getStreet(),   Terrain.getStreet(),    Terrain.getStreet(),    Terrain.getStreet(),    Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getWoods(),    Terrain.getWoods(),     Terrain.getPlane(),     Terrain.getPlane(),     Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(),    Terrain.getPlane(),     Terrain.getPlane(),     Terrain.getPlane(),     Terrain.getPlane(), Terrain.getPlane()],
            // [Terrain.getPlane(),    Terrain.getPlane(),     Terrain.getPlane(),     Terrain.getPlane(),     Terrain.getPlane(), Terrain.getPlane()]  // 5
        ]
    };
    const terrainsSquared = function() { // x: 4, y: 4; each line equal terrain
        return [          
            [Terrain.getTerrain('city'),    Terrain.getTerrain('city'),     Terrain.getTerrain('city'),     Terrain.getTerrain('city')  ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ],
            [Terrain.getTerrain('street'),  Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street')],
            [Terrain.getTerrain('woods'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('woods') ]
        ]
    };
    const terrainsSquaredChanged = function() { // x: 4, y: 4; each line equal terrain, but one differnce on 3/2
        return [          
            [Terrain.getTerrain('city'),    Terrain.getTerrain('city'),     Terrain.getTerrain('city'),     Terrain.getTerrain('city')  ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane') ],
            [Terrain.getTerrain('street'),  Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street')],
            [Terrain.getTerrain('woods'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('city'),    Terrain.getTerrain('woods') ]
        ]
    };
    const terrainsMedium = function() {  // x: 8, y: 6 ; custom terrain
        return [
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ], // 1
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('street'),   Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('city'),     Terrain.getTerrain('street'),   Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ], // 5
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('city'),     Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street')    ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('city'),     Terrain.getTerrain('plane')     ] // 8
        ]
    };
    const terrainsMediumShrinkedX = function() {  // x: 6, y: 6 ; custom terrain - 2 lanes of plane on x
        return [
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ], // 1
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('street'),   Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('city'),     Terrain.getTerrain('street'),   Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('street'),   Terrain.getTerrain('plane')     ], // 5
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('city'),     Terrain.getTerrain('street'),   Terrain.getTerrain('street'),   Terrain.getTerrain('street')    ]
        ]
    };
    const terrainsMediumShrinkedY = function() {  // x: 8, y: 4 ; custom terrain - 2 lanes of plane on y
        return [
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('woods'),    Terrain.getTerrain('woods')     ], // 1
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('city')      ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane')     ], // 5
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('city'),     Terrain.getTerrain('street')    ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane')     ]  // 8
        ]
    };
    const terrainsMediumShrinkedXY = function() { // x: 6, y: 4 ; custom terrain - 2 lanes
        return [ 
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('city'),     Terrain.getTerrain('woods'),    Terrain.getTerrain('woods')     ], // 1
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('woods'),    Terrain.getTerrain('woods'),    Terrain.getTerrain('city')      ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('woods')     ],
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('plane'),    Terrain.getTerrain('plane'),    Terrain.getTerrain('plane')     ], // 5
            [Terrain.getTerrain('plane'),   Terrain.getTerrain('street'),   Terrain.getTerrain('city'),     Terrain.getTerrain('street')    ]
        ]
    };

    // units
    const unitsEmptyMapDefault = function() { // x: 20, y: 20 ; no units on all planes terrain
        return [ 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 1
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 5
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 10
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 15
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ]  // 20
        ]
    };
    const unitsPlacedMapDefault = function() { // x: 20, y: 20 ; units on all planes terrain
        return [ 
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 1
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  Unit.getUnit('infantry'),   undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], 
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], //5
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  Unit.getUnit('lightTank'),   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 10
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ], // 15
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ],
            [undefined,   undefined,    undefined,  undefined,  undefined,                  undefined,  undefined,  undefined,  undefined,  undefined,                   undefined,   undefined,    undefined,  undefined,  undefined,  undefined,   undefined,    undefined,  undefined,  undefined  ]  // 20
        ]
    };
    const unitsEmptyMapSmall = function() { // x: 3, y: 4 ; no units on custom terrain
        return [ 
            [undefined,   undefined,     undefined,    undefined     ], // 1
            [undefined,   undefined,     undefined,    undefined     ],
            [undefined,   undefined,     undefined,    undefined     ]  // 3
        ]
    };
    const unitsPlacedMapSmall = function() { // x: 3, y: 4 ; units on custom terrain
        return [ 
            [undefined,   Unit.getUnit('infantry'),     undefined,                      undefined     ], // 1
            [undefined,   undefined,                    Unit.getUnit('lightTank'),      undefined     ],
            [undefined,   undefined,                    undefined,                      undefined     ]  // 3
        ]
    };


    // public API / oeffentliche API
    return {
        // scenarios
        /** Get the default scenario without players or units.
         * @returns {{}} default scenario without players or units
         */
        getScenarioDefault0Player: function() {
            return scenarioDefault();
        },

        /** Get the scenario after 1 player was added to default scenario.
         * @returns {{}} scenario after 1 player was added to default scenario
         */
        getScenarioDefault1Player1LosingCondition: function() {
            return scenarioDefault1Player1LosingCondition();
        },
        /** Get the scenario after 2 player were added to default scenario.
         * @returns {{}} scenario after 2 player were added to default scenario
         */
        getScenarioDefault2Player1LosingCondition: function() {
            return scenarioDefault2Player1LosingCondition();
        },
        /** Get the scenario after 3 player were added to default scenario.
         * @returns {{}} scenario after 3 player were added to default scenario
         */
        getScenarioDefault3Player1LosingCondition: function() {
            return scenarioDefault3Player1LosingCondition();
        },
        /** Get the scenario after 1 player was added to default scenario.
         * @returns {{}} scenario after 1 player and second losing condition was added to default scenario
         */
        getScenarioDefault1Player2LosingConditions: function() {
            return scenarioDefault1Player2LosingConditions();
        },
        /** Get the scenario 'Nuke Town' without units.
         * @returns {{}} scenario 'Nuke Town' without units
         */
        getScenarioNuketown2PlayersEmpty: function() {
            return scenarioNuketown2PlayersEmpty();
        },
        /** Get the scenario 'Nuke Town' with units.
         * @returns {{}} scenario 'Nuke Town' with units
         */
        getScenarioNuketown2PlayersPlaced: function() {
            return scenarioNuketown2PlayersPlaced();
        },


        // players
        /**
         * Get the default player template.
         * @returns {{}} default player template
         */
        getPlayerTemplate: function() {
            return playerTemplate();
        },
        /**
         * Get the default player 1.
         * @returns {{}} default player 1
         */
        getPlayer1Default: function() {
            return player1Default();
        },
        /**
         * Get the default player 2.
         * @returns {{}} default player 2
         */
        getPlayer2Default: function() {
            return player2Default();
        },
        /**
         * Get the default player 3.
         * @returns {{}} default player 3
         */
        getPlayer3Default: function() {
            return player3Default();
        },
        /**
         * Get the default player 4.
         * @returns {{}} default player 4
         */
        getPlayer4Default: function() {
            return player4Default();
        },
        /**
         * Get the players at start filled with the 3 default players from 1 until 3.
         * @returns {Map<string, {}>} players at start with 3 default players
         */
        getPlayersStartWith3Default: function() {
            return playersStartWith3Default();
        },
        /**
         * Get the players at start filled with 3 players: 1, 3 and 4. Simulate a scenario where Player 2 was defeated.
         * @returns {Map<string, {}>} players at start with 3 default players
         */
        getPlayersStartWith3Player2Defeated: function() {
            return playersStartWith3Player2Defeated();
        },

        // colors
        /**
         * Get all possible colors for players
         * @returns {string[]>} colors for players
         */
        getColors: function() {
            return colors();
        },

        // terrains
        /**
         * Get the default game map.
         * @return {[[Object]]} default game map (all default terrain objects)
         */
        getTerrainsDefault: function() {
            return terrainsDefault();
        },
        /**
         * Get a small game map.
         * @return {[[Object]]} small game map (custom terrain objects)
         */
        getTerrainsSmall: function() {
            return terrainsSmall();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsSmallExtendedX: function() {
            return terrainsSmallExtendedX();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsSmallExtendedY: function() {
            return terrainsSmallExtendedY();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsSmallExtendedXY: function() {
            return terrainsSmallExtendedXY();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsSquared: function() {
            return terrainsSquared();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsSquaredChanged: function() {
            return terrainsSquaredChanged();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsMedium: function() {
            return terrainsMedium();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsMediumShrinkedX: function() {
            return terrainsMediumShrinkedX();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsMediumShrinkedY: function() {
            return terrainsMediumShrinkedY();
        },
        /**
         * TODO
         * @returns 
         */
        getTerrainsMediumShrinkedXY: function() {
            return terrainsMediumShrinkedXY();
        },

        // units
        /**
         * @returns {[[undefined]]} no units on default game map.
         */
        getUnitsEmptyMapDefault: function() {
            return unitsEmptyMapDefault();
        },

        /**
         * @returns {[[any?]]} units (or undefinded when no unit at coordinates) on default game map.
         */
         getUnitsPlacedMapDefault: function() {
            return unitsPlacedMapDefault();
        },

        /**
         * @returns {[[undefined]]} no units on game map with sizes x: 3, y: 4
         */
        getUnitsEmptyMapSmall: function() {
            return unitsEmptyMapSmall();
        },

        /**
         * @returns {[[undefined]]} units on game map with sizes x: 3, y: 4
         */
        getUnitsPlacedMapSmall: function() {
            return unitsPlacedMapSmall();
        }
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
    console.log('GameMapData-Modul is ready. |' + Date(Date.now()).slice(4,24));
}, false)
