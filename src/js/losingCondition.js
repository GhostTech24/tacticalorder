/** eng: losing condition class. Contains all data for losing conditions.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: scenario.js
const LosingCondition = (function () { // encapsulation / Kapselung
  const losingConditions = new Map(
    [
      ['allUnitsLost',   {id: 'allUnitsLost',  affectedPlayerId: 0, xCoordinate: null, yCoordinate: null, conditionCheck: function(affectedPlayerId=this.affectedPlayerId) {
            const player = Scenario.getPlayerStart(affectedPlayerId);
            const unitSizeCurrent = player.units;

            const isAllUnitsLost = (unitSizeCurrent === 0);
            return isAllUnitsLost
          }
        }  
      ],
      ['unitReachedField', {id: 'unitReachedField', affectedPlayerId: 0,  xCoordinate: null, yCoordinate: null, conditionCheck: function(affectedPlayerId=this.affectedPlayerId, xCoordinate=this.xCoordinate, yCoordinate=this.yCoordinate) {
            const unit = Scenario.getUnitStart(xCoordinate, yCoordinate);
            if(xCoordinate === null || yCoordinate === null) {
              return null;
            }
            if(unit) {
              const unitOwner = unit.ownerCurrent;
              const isReachedByEnemyUnit = (unitOwner !== affectedPlayerId);
              
              return isReachedByEnemyUnit
            } else {
              return false;
            }
          }
        }
      ]
    ]
  );

  /**
   * Get all loosing conditions with referenceless Objects.
   * The Map prototype return it values referenced when they are objects.
   */
  getLosingConditionsReferenceless =  function() {
    const losingConditionsReferenceless = new Map();

    for (let [key, value] of losingConditions) {
      losingConditionsReferenceless.set(key, Object.assign({}, value))
    }

    return losingConditionsReferenceless;
  }

  // public API / oeffentliche API
  return {
    /**  Get a specific losing condition.
     * @param {String} id the id of the losing condition
     * @return {{}} losing condition
     */
    getLosingCondition(id) {
      const losingConditionsReferenceless = getLosingConditionsReferenceless();
      const losingCondition = losingConditionsReferenceless.get(id);

      return losingCondition;
    }
  }
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
  console.log('LosingCondition-Module is ready. |' + Date(Date.now()).slice(4,24));
}, false)