/** eng: view class. Contains all data for the view.
 * @name test a test
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: -
const View = (function () { // encapsulation / Kapselung
    let isFullscreen = false;

    /** TODO */
    const classNameRepository = {
        hidden: 'hidden',
        activeView: 'activeView',
        activeDialog: 'activeDialog',
        playerColorPart: 'playerColor',
        unitGraphicOnGameMap: 'unitGraphicOnGameMap',
        terrainGraphicOnGameMap: 'terrainGraphicOnGameMap',
        terrainOrUnitSelectionGraphic: 'terrainOrUnitSelectionGraphic',
        playerSelectedInfo: 'playerSelectedInfo',
        scenarioList: 'scenarioList'
    }
    /** TODO */
    const idNameRepository = {
        mapRow: 'mapRow',
        column: 'Column',
        playerColorPart1: 'player',
        playerColorPart2Value: 'ColorValue',
        playerColorPart2NextButton: 'ColorNextButton',
        playerColorPart2BevoreButton: 'ColorBevoreButton',
        loadScenarioPart: 'loadScenario'
    }
    /** All name attributes used in the frontend. */
    const nameAttributeRepository = {
        checkboxGroup: 'checkboxGroup'
    }
    /** All custom attributes in the frontend. */
    const attributeCustomRepository = {
        scenarioName: 'scenarioName'
    }

    /** All used and / or interactive HTML Elements.
     */
    const objectRepository = {
        // general
        overlayElement: function() {return document.getElementById('overlay')},
        closeDialogButton: function() {return document.getElementById('closeDialogButton')},

        // main menu
        levelEditorButton: function() {return document.getElementById('levelEditorButton')},
        settingsButton: function() {return document.getElementById('settingsButton')},

        // level editor
        levelEditorView: function() {return document.getElementById('leveleditorMenu')},
        backLeveleditorToMainMenuButton: function() {return document.getElementById('backLeveleditorToMainMenuButton')},
        setUnitOrTerrainButton: function() {return document.getElementById('setUnitOrTerrainButton')},
        setSizeButton: function() {return document.getElementById('setSizeButton')},
        setPlayerButton: function() {return document.getElementById('setPlayerButton')},
        setLosingConditionButton: function() {return document.getElementById('setLosingConditionButton')},
        setScenarioButton: function() {return document.getElementById('setScenarioButton')},
 
        // level editor -  terrain dialog
        setTerrainDialog: function() {return document.getElementById('setTerrainDialog')},
        setTerrainDialogTitle: function() {return document.getElementById('setTerrainDialogTitle')}, 
        unitSelectionButton: function() {return document.getElementById('unitSelectionButton')},
        terrainsContainer: function() {return document.getElementById('terrainsContainer')},

        // level editor -  unit dialog
        setUnitDialog: function() {return document.getElementById('setUnitDialog')},
        setUnitDialogTitle: function() {return document.getElementById('setUnitDialogTitle')},
        unitsContainer: function() {return document.getElementById('unitsContainer')},
        playerColorUnitBevoreButton: function() {return document.getElementById('playerColorUnitBevoreButton')},
        playerColorUnitValue: function() {return document.getElementById('playerColorUnitValue')},
        playerColorUnitNextButton: function() {return document.getElementById('playerColorUnitNextButton')},
        terrainSelectionButton: function() {return document.getElementById('terrainSelectionButton')},
        
        // level editor - size dialog
        setSizeDialogTitle: function() {return document.getElementById('setSizeDialogTitle')},
        setSizeDialog: function() {return document.getElementById('setSizeDialog')},
        arrowUpXButton: function() {return document.getElementById('arrowUpXButton')},
        arrowDownXButton: function() {return document.getElementById('arrowDownXButton')},
        currentXsizeValue: function() {return document.getElementById('currentXsizeValue')},
        arrowUpYButton: function() {return document.getElementById('arrowUpYButton')},
        arrowDownYButton: function() {return document.getElementById('arrowDownYButton')},
        currentYsizeValue: function() {return document.getElementById('currentYsizeValue')},

        // level editor - player dialog
        setPlayerDialog: function() {return document.getElementById('setPlayerDialog')},
        setPlayerDialogTitle: function() {return document.getElementById('setPlayerDialogTitle')},
        playerSelectionContainer: function() {return document.getElementById('playerSelectionContainer')},
        playersTitle: function() {return document.getElementById('playersTitle')},
        arrowUpPlayerButton: function() {return document.getElementById('arrowUpPlayerButton')},
        arrowDownPlayerButton: function() {return document.getElementById('arrowDownPlayerButton')},
        playerValueCurrent: function() {return document.getElementById('playerValueCurrent')},
        playerContainer: function() {return document.getElementById('playerContainer')},
        playerColorValue: function(playerId) {return document.getElementById(idNameRepository.playerColorPart1 + playerId + idNameRepository.playerColorPart2Value)},

        // level editor - losing condition dialog
        setLosingConditionDialog: function() {return document.getElementById('setLosingConditionDialog')},
        setLosingConditionDialogTitle: function() {return document.getElementById('setLosingConditionDialogTitle')},

        allUnitsLostContainer: function() {return document.getElementById('allUnitsLostContainer')},
        allUnitsLostCheckbox: function() {return document.getElementById('allUnitsLostCheckbox')},

        unitReachedFieldContainer: function() {return document.getElementById('unitReachedFieldContainer')},
        unitReachedFieldCheckbox: function() {return document.getElementById('unitReachedFieldCheckbox')},
        unitReachedFieldXCoordinateValue: function() {return document.getElementById('unitReachedFieldXCoordinateValue')},
        unitReachedFieldYCoordinateValue: function() {return document.getElementById('unitReachedFieldYCoordinateValue')},
        unitReachedFieldSelectFieldButton: function() {return document.getElementById('unitReachedFieldSelectFieldButton')},

        playerColorLosingConditionBevoreButton: function() {return document.getElementById('playerColorLosingConditionBevoreButton')},
        playerColorLosingConditionValue: function() {return document.getElementById('playerColorLosingConditionValue')},
        playerColorLosingConditionNextButton: function() {return document.getElementById('playerColorLosingConditionNextButton')},

        // level editor - scenario dialog
        scenarioNameInput: function() {return document.getElementById('scenarioNameInput')},
        scenarioSaveButton: function() {return document.getElementById('scenarioSaveButton')},
        scenarioResetButton: function() {return document.getElementById('scenarioResetButton')},
        scenarioImportButton: function() {return document.getElementById('scenarioImportButton')},
        scenarioExportButton: function() {return document.getElementById('scenarioExportButton')},
        scenariosTitle: function() {return document.getElementById('scenariosTitle')},
        scenarioLoadButton: function() {return document.getElementById('scenarioLoadButton')},
        scenarioDeleteButton: function() {return document.getElementById('scenarioDeleteButton')},
        scenarioSelectionContainer: function() {return document.getElementById('scenarioSelectionContainer')},

        // settings
        backSettingsToMainMenuButton: function() {return document.getElementById('backSettingsToMainMenuButton')}
    }
    
    // initialize at start of the app
    /** Initializes all Elements with events and dialogs. */
    initView = function() {
        closeDialog();
        
        initGeneralElementEvents();
        initMainMenuEvents();
        initLeveleditorMenuEvents();
        initSettingsMenuEvents();

        initUnitOrTerrainDialog();
        initSetSizeEvents();
        initSetPlayerDialog();
        initSetLosingConditionDialog();
        initSetScenarioDialog();
    },

    // initialize views at start of the app
    /** Initializes all general Elements with events. */
    initGeneralElementEvents = function() {
        objectRepository.closeDialogButton().addEventListener('click', function() {closeDialog()});
    }
    /** Initializes all Elements of the view main menu with events. */
    initMainMenuEvents = function() {
        objectRepository.levelEditorButton().addEventListener('click', function() {changeToLevelEditorView()});
        objectRepository.settingsButton().addEventListener('click', function() {changeActiveView('settingsMenu')});
    },
    /** Initializes all Elements of the view leveleditor menu with events. */
    initLeveleditorMenuEvents = function() {
        objectRepository.backLeveleditorToMainMenuButton().addEventListener('click', function() {changeToMainMenuViewFromLeveleditorView()});
        objectRepository.setUnitOrTerrainButton().addEventListener('click', function() {openUnitOrTerrainDialog()});
        objectRepository.setSizeButton().addEventListener('click', function() {openSizeDialog()});  
        objectRepository.setPlayerButton().addEventListener('click', function() {openSetPlayerDialog()});
        objectRepository.setLosingConditionButton().addEventListener('click', function() {openSetLosingConditionDialog()});
        objectRepository.setScenarioButton().addEventListener('click', function() { openSetScenarioDialog()  });
    },
    /** Initializes all Elements of the view settings menu with events. */
    initSettingsMenuEvents = function() {
        objectRepository.backSettingsToMainMenuButton().addEventListener('click', function() {changeActiveView('mainMenu')});
    },

    // initialize dialogs at start of the app
    /** Initializes all Elements of the set terrain and set unit dialog with events. */
    initUnitOrTerrainDialog = function() {
        // terrain dialog - events
        objectRepository.terrainSelectionButton().addEventListener('click', function() {changeUnitOrTerrainDialog()});
        // TODO add event for playerColorTerrainBevoreButton
        // TODO add event for playerColorTerrainNextButton

        // terrain dialog - dynamic terrain elements
        const terrainElementsContainer = objectRepository.terrainsContainer();

        const terrainIds = Controler.requestTerrainIds();
        let currentElementFileString, currentElement, currentButton;

        for (let terrainId of terrainIds) {
            currentElementFileString = getTerrainFileString(terrainId);

            currentElement = document.createElement('img');
            currentElement.classList.add(classNameRepository.terrainOrUnitSelectionGraphic);
            currentElement.src = currentElementFileString;
            currentElement.setAttribute('terrain', terrainId); // set custom element attribute

            currentButton = document.createElement('button');
            currentButton.addEventListener('click', function() {
                setSetableElement(terrainId);
                closeDialog();
            });
            currentButton.appendChild(currentElement);

            terrainElementsContainer.appendChild(currentButton);
        }

        // unit dialog - events  
        objectRepository.unitSelectionButton().addEventListener('click', function() {changeUnitOrTerrainDialog()}); 
        objectRepository.playerColorUnitBevoreButton().addEventListener('click', function() {changePlayerIdSelection(-1)}); 
        objectRepository.playerColorUnitNextButton().addEventListener('click', function() {changePlayerIdSelection(1)}); 

        // unit dialog - dynamic unit elements
        const terrainUnitsContainer = objectRepository.unitsContainer();
        const unitIds = Controler.requestUnitIds();
        for (let unitId of unitIds.values()) {   
            const buttonCurrent = document.createElement('button');

            buttonCurrent.addEventListener('click', function() {
                setSetableElement(unitId);
                closeDialog();
            });

            createAndAppendUnitPictureElement(unitId, classNameRepository.terrainOrUnitSelectionGraphic, buttonCurrent);

            terrainUnitsContainer.appendChild(buttonCurrent);
        }
    },
    /** Initializes all Elements of the set size dialog with events. */
    initSetSizeEvents = function() {
        objectRepository.arrowUpXButton().addEventListener('click', function() {changeGameMapSize(1, 0)});
        objectRepository.arrowDownXButton().addEventListener('click', function() {changeGameMapSize(-1, 0)});
        objectRepository.arrowUpYButton().addEventListener('click', function() {changeGameMapSize(0, 1)});
        objectRepository.arrowDownYButton().addEventListener('click', function() {changeGameMapSize(0, -1)});
    },
    /** Initializes all Elements of the set player dialog with events. */
    initSetPlayerDialog = function() {
        objectRepository.arrowUpPlayerButton().addEventListener('click', function() {changePlayerStartSize(true)});
        objectRepository.arrowDownPlayerButton().addEventListener('click', function() {changePlayerStartSize(false)});
        
        updateSetPlayerDialogForColors();
    },
    /** Initializes all Elements of the set losing condition dialog with events. */
    initSetLosingConditionDialog = function() {
        objectRepository.allUnitsLostCheckbox().addEventListener('click', function() {changeLosingConditionStatus('allUnitsLost')}); 
        objectRepository.unitReachedFieldCheckbox().addEventListener('click', function() {changeLosingConditionStatus('unitReachedField')}); 
        objectRepository.unitReachedFieldSelectFieldButton().addEventListener('click', function() {setSetableElementUnitReachedFieldPosition()}); 

        objectRepository.playerColorLosingConditionBevoreButton().addEventListener('click', function() {changePlayerIdSelection(-1)});
        objectRepository.playerColorLosingConditionNextButton().addEventListener('click', function() {changePlayerIdSelection(1)});  
    },
    /** Initializes all Elements of the set scenario dialog with events. */
    initSetScenarioDialog = function() {
        objectRepository.scenarioNameInput().addEventListener('focusout', function() { updateScenarioName(true) });
        
        objectRepository.scenarioSaveButton().addEventListener('click', function() { saveScenario() });  
        objectRepository.scenarioResetButton().addEventListener('click', function() { resetScenario() });  
        objectRepository.scenarioImportButton().addEventListener('click', function() { importScenario() });
        objectRepository.scenarioExportButton().addEventListener('click', function() { exportScenario() });

        const scenarioLoadButton = objectRepository.scenarioLoadButton();
        scenarioLoadButton.addEventListener('click', function() { loadAndUpdateScenario() });
        scenarioLoadButton.disabled = true; // disable every init; static as html value is not enough -> page refresh after scenario load without closing dialog

        const scenarioDeleteButton = objectRepository.scenarioDeleteButton(); // TODO
        scenarioDeleteButton.addEventListener('click', function() { deleteScenario() });
        scenarioDeleteButton.disabled = true;
    },

    // switch view
    /** Changes to a given view. */
    changeActiveView = function(newActiveViewId) {       
        const currentActiveViewClassList = document.getElementsByClassName(classNameRepository.activeView)[0].classList;
        const newActiveViewClassList = document.getElementById(newActiveViewId).classList;     

        currentActiveViewClassList.remove(classNameRepository.activeView);
        currentActiveViewClassList.add(classNameRepository.hidden);

        newActiveViewClassList.add(classNameRepository.activeView);
        newActiveViewClassList.remove(classNameRepository.hidden);
    },
    /** TODO */
    changeToMainMenuViewFromLeveleditorView = function() {
        setSetableElement('none');
        changeActiveView('mainMenu');
    },
    /** Async! TODO */
    changeToLevelEditorView = async function() {
        await Controler.requestLoadScenarios();
        updateGameMap();
        changeActiveView('leveleditorMenu');  
    },

    // switch dialog
    /** Open a given dialog. */
    openDialog = function(dialogId) {
        // overlay - display
        const overlayElementClassList = objectRepository.overlayElement().classList;
        if(overlayElementClassList.contains(classNameRepository.hidden)) {
            overlayElementClassList.remove(classNameRepository.hidden)
        }

        // preview active dialog - hide and remove mark
        const currentActiveDialogs = document.getElementsByClassName(classNameRepository.activeDialog);
        if(currentActiveDialogs.length !== 0) {
            const currentActiveDialogClassList = currentActiveDialogs[0].classList;
            currentActiveDialogClassList.remove(classNameRepository.activeDialog);
            currentActiveDialogClassList.add(classNameRepository.hidden);
        }

        // wanted dialog - display and add mark
        const currentDialogClassList = document.getElementById(dialogId).classList;
        if(!currentDialogClassList.contains(classNameRepository.activeDialog)) {
            currentDialogClassList.add(classNameRepository.activeDialog);
        }
        if(currentDialogClassList.contains(classNameRepository.hidden)) {
            currentDialogClassList.remove(classNameRepository.hidden);
        }

        // close Button - display
        const closeDialogButtonClassList = objectRepository.closeDialogButton().classList;
        if(closeDialogButtonClassList.contains(classNameRepository.hidden)) {
            closeDialogButtonClassList.remove(classNameRepository.hidden);
        }
    },
    /** Open the set terrain or set unit dialog. */
    openUnitOrTerrainDialog = function() {
        const dialogId = Controler.requestUnitOrTerrainDialogSelection();

        updateUnitOrTerrainColorElements();
        openDialog(dialogId);
    },
    /** Open the set size dialog. */
    openSizeDialog = function() {
        const dialogId = 'setSizeDialog';

        updateSizeDialogCounter();
        openDialog(dialogId);
    },
    /** Open the set player dialog. */
    openSetPlayerDialog = function() {
        const dialogId = 'setPlayerDialog';

        updatePlayerCounter();
        openDialog(dialogId);
    },
    /** Open the set losing condition dialog. */
    openSetLosingConditionDialog = function() {
        const dialogId = 'setLosingConditionDialog';

        updateSetLosingConditionColorElements();
        updateSetLosingConditionLosingConditions();
        openDialog(dialogId);
    },
    /** Open the set losing condition dialog. */
    openSetScenarioDialog = function() {
        const dialogId = 'setScenarioDialog';

        updateScenariosLoadable();
        updateAfterSelectionScenarioLoadable(null);
        updateScenarioName(false);
        openDialog(dialogId);
    },
    /** Switch between terrain or unit dialog. */
    changeUnitOrTerrainDialog = function() {
        const currentDialogId = Controler.requestUnitOrTerrainDialogSelection();
        let dialogId;

        if(currentDialogId === 'setTerrainDialog') {
            dialogId = 'setUnitDialog';
        } else { // was 'setUnitDialog'
            dialogId = 'setTerrainDialog';
        }
        
        // model
        Controler.reportUnitOrTerrainDialogSelection(dialogId);
        // view
        openUnitOrTerrainDialog();
    },
    /** Close the currently active dialog. */
    closeDialog = function() {
        objectRepository.overlayElement().classList.toggle(classNameRepository.hidden);


        const currentActiveDialogClassList = document.getElementsByClassName(classNameRepository.activeDialog)[0].classList;
        currentActiveDialogClassList.add(classNameRepository.hidden);
        currentActiveDialogClassList.remove(classNameRepository.activeDialog);

        objectRepository.closeDialogButton().classList.add(classNameRepository.hidden);

        /*
         * dev hint: solution with elementId of element to close as param from this function was bad.
         * Would need repeated additation and removal from click EventListeners, which broke often.
         */
    },

    // changes in general dialogs
    /** Change the current player id selection in a given direction. Also updates all affected elements.
     * @param changeDirectionValue the direction for the change
     */
    changePlayerIdSelection = function(changeDirectionValue) {
        
        Controler.reportPlayerIdChange(changeDirectionValue);

        updateUnitOrTerrainColorElements();
        updateSetLosingConditionLosingConditions();
    },
    /** TODO */
    getTerrainFileString = function(terrainId) {
        const mediaPath = './media/pic/terrain/';
        const fileExtension = '.jpg'
        
        if(terrainId === 'city') {
            return mediaPath + 'city' + fileExtension;
        }
        if(terrainId === 'plane') {
            return mediaPath + 'plane' + fileExtension;
        }
        if(terrainId === 'street') {
            return mediaPath + 'street' + fileExtension;
        }
        if(terrainId === 'woods') {
            return mediaPath + 'woods' + fileExtension;
        }
        if(terrainId === 'sea') {
            return mediaPath + 'sea' + fileExtension;
        }
        if(terrainId === 'mountain') {
            return mediaPath + 'mountain' + fileExtension;
        }
        
        console.error('invalid terrainId! No matching found for terrainId: |' + terrainId + '|');
    },
    
    
    // changes in specific dialogs
    /** TODO */
    changeLosingConditionStatus = function(losingConditionId, xCoordinate, yCoordinate) {
        Controler.reportLosingConditionStatusChange(losingConditionId, xCoordinate, yCoordinate)
        updateSetLosingConditionLosingConditions();
    }
    /** Update all elements affected by player color from set terrain / set units dialog. */
    updateUnitOrTerrainColorElements = function() {
        // get color
        const playerIdCurrent = Controler.requestPlayerIdSelection();
        const colorClassName = Controler.requestPlayerColorClassNameOfUnit();
        // get playerSelectionValueNew 
        const playerSelectionValueNew = 'P' + playerIdCurrent;

        // collect all affected elements
        const elementsAffected = [];

        // collect - terrains dialog
        // TODO

        // collect - units dialog
        const valueElementUnit = objectRepository.playerColorUnitValue();
        valueElementUnit.innerHTML = playerSelectionValueNew;
        elementsAffected.push(valueElementUnit);

        // update unit buttons (exeptional handling; cause they can be hidden too)
        const unitDialogButtons = objectRepository.setUnitDialog().getElementsByTagName('button');
        let buttonCurrent
        for(buttonCurrent of unitDialogButtons) {
            const pictureElement = buttonCurrent.children[0];
            const isContainingUnitPicture = pictureElement.hasAttribute('unit');

            if(isContainingUnitPicture) {
                const buttonCurrentClassList = buttonCurrent.classList;
                
                if(playerIdCurrent === 0) { // neutral, so no player
                    if(!buttonCurrentClassList.contains(classNameRepository.hidden)) {
                        buttonCurrentClassList.add(classNameRepository.hidden);
                    }
                } else {
                    if(buttonCurrentClassList.contains(classNameRepository.hidden)) {
                        buttonCurrentClassList.remove(classNameRepository.hidden);
                    }

                    const pictureElementClassList = pictureElement.classList;
                    const pictureElementClassListSize = pictureElementClassList.length;

                    for(let i = 0; i < pictureElementClassListSize; i++) {
                        const classNameCurrent = pictureElementClassList[i];
                        if(classNameCurrent.includes(classNameRepository.playerColorPart)) {
                            pictureElementClassList.remove(classNameCurrent);
                            break;
                        }
                    }
                    pictureElementClassList.add(colorClassName);
                } 
            }
        }

        // collect - losing conditions dialog
        const valueElementLosingCondition =  objectRepository.playerColorLosingConditionValue();
        valueElementLosingCondition.innerHTML = playerSelectionValueNew;
        elementsAffected.push(valueElementLosingCondition);

        // update all affected elements
        let elementCurrent
        for(elementCurrent of elementsAffected) {
            const elementCurrentClassList = elementCurrent.classList;
            for(classNameCurrent of elementCurrentClassList) {
                if(classNameCurrent.includes(classNameRepository.playerColorPart)) {
                    elementCurrentClassList.remove(classNameCurrent);
                    break;
                }
            }
            elementCurrentClassList.add(colorClassName);
        }
    },
    /** Update all elements affected by player color from set losing conditions dialog. */
    updateSetLosingConditionColorElements = function() {
        const playerIdCurrent = Controler.requestPlayerIdSelection();
        const colorClassName = Controler.requestPlayerColorClassNameOfUnit();
        // get playerSelectionValueNew 
        const playerSelectionValueNew = 'P' + playerIdCurrent;

        const valueElementLosingCondition =  objectRepository.playerColorLosingConditionValue();
        valueElementLosingCondition.innerHTML = playerSelectionValueNew;

        // update affected element
        const valueElementLosingConditionClassList = valueElementLosingCondition.classList;
        for(classNameCurrent of valueElementLosingConditionClassList) {
            if(classNameCurrent.includes(classNameRepository.playerColorPart)) {
                valueElementLosingConditionClassList.remove(classNameCurrent);
                break;
            }
        }
        valueElementLosingConditionClassList.add(colorClassName);
    },
    /* Update losing conditions of the player id selection. */
    updateSetLosingConditionLosingConditions = function() {
        const playerId = Controler.requestPlayerIdSelection();
        const isNeutral = (playerId === 0);

        const allUnitsLostContainer = objectRepository.allUnitsLostContainer();
        const allUnitsLostContainerClassList = allUnitsLostContainer.classList;
        const allUnitsLostCheckbox = objectRepository.allUnitsLostCheckbox();

        const unitReachedFieldContainer = objectRepository.unitReachedFieldContainer();
        const unitReachedFieldContainerClassList = unitReachedFieldContainer.classList;
        const unitReachedFieldCheckbox = objectRepository.unitReachedFieldCheckbox();
        const unitReachedFieldSelectFieldButton = objectRepository.unitReachedFieldSelectFieldButton();
        const unitReachedFieldSelectFieldButtonClassList = unitReachedFieldSelectFieldButton.classList;

        const unitReachedFieldXCoordinateValue = objectRepository.unitReachedFieldXCoordinateValue();
        const unitReachedFieldYCoordinateValue = objectRepository.unitReachedFieldYCoordinateValue();

        if(isNeutral) {
            if(!allUnitsLostContainerClassList.contains('unchecked')) {
                allUnitsLostContainerClassList.add('unchecked');
            }
            allUnitsLostCheckbox.checked = false;
            allUnitsLostCheckbox.disabled = true;

            if(!unitReachedFieldContainerClassList.contains('unchecked')) {
                unitReachedFieldContainerClassList.add('unchecked');
            }
            unitReachedFieldCheckbox.checked = false;
            unitReachedFieldCheckbox.disabled = true;
            
            if(!unitReachedFieldSelectFieldButtonClassList.contains('hidden')) {
                unitReachedFieldSelectFieldButtonClassList.add('hidden')
            }

            unitReachedFieldXCoordinateValue.innerHTML = 'X: ' + 0;
            unitReachedFieldYCoordinateValue.innerHTML = 'Y: ' + 0;
        } else {
            const losingConditionsPlayer = Controler.requestLosingConditions(playerId);
            const losingConditionNames = [allUnitsLostContainer.getAttribute('losingCondition'), unitReachedFieldContainer.getAttribute('losingCondition')]
            const losingConditionContainerClassLists = [allUnitsLostContainerClassList, unitReachedFieldContainerClassList];
            const losingConditionCheckboxes = [allUnitsLostCheckbox, unitReachedFieldCheckbox];
    
            for(losingCondition of losingConditionsPlayer) {
                const losingConditionId = losingCondition.id;
                const index = losingConditionNames.indexOf(losingConditionId);

                // update view
                if(losingConditionContainerClassLists[index].contains('unchecked')) {
                    losingConditionContainerClassLists[index].remove('unchecked')
                }
                losingConditionCheckboxes[index].checked = true;
                losingConditionCheckboxes[index].disabled = false;
                // update specific elements
                if(losingConditionId === 'unitReachedField') {
                    if(unitReachedFieldSelectFieldButtonClassList.contains('hidden')) {
                        unitReachedFieldSelectFieldButtonClassList.remove('hidden')
                    }

                    const xCoordinate = losingCondition.xCoordinate;
                    const yCoordinate = losingCondition.yCoordinate;

                    unitReachedFieldXCoordinateValue.innerHTML = 'X: ' + xCoordinate;
                    unitReachedFieldYCoordinateValue.innerHTML = 'Y: ' + yCoordinate;
                }

                // update local lists
                losingConditionNames.splice(index, 1);
                losingConditionContainerClassLists.splice(index, 1);
                losingConditionCheckboxes.splice(index, 1);
    
            }
        
            let i = losingConditionNames.length;
            for(i; i > 0; i--) {
                const index = i - 1;
                // update view
                if(!losingConditionContainerClassLists[index].contains('unchecked')) {
                    losingConditionContainerClassLists[index].add('unchecked')
                }
                losingConditionCheckboxes[index].checked = false;
                losingConditionCheckboxes[index].disabled = false;

                // update specific elements
                if(losingConditionNames[index] === 'unitReachedField') {
                    if(!unitReachedFieldSelectFieldButtonClassList.contains('hidden')) {
                        unitReachedFieldSelectFieldButtonClassList.add('hidden')
                    }

                    unitReachedFieldXCoordinateValue.innerHTML = 'X: ' + 0;
                    unitReachedFieldYCoordinateValue.innerHTML = 'Y: ' + 0;
                }
            }
        }
    },
    /** Update scenario name.
     * @param {boolean} isOverwriting boolean value if the update should overwrite the known name with the current frontend input
     */
    updateScenarioName = function(isOverwriting) {
        const scenarioNameInput = objectRepository.scenarioNameInput();
        const nameKnown = Controler.requestScenarioName();
        
        if(isOverwriting) {
            const nameCurrent = scenarioNameInput.value;
            const isUnequalValue = (nameKnown !== nameCurrent);
            if(isUnequalValue) {
                Controler.reportScenarioName(nameCurrent);
            }
        } else {
            scenarioNameInput.value = nameKnown;
        }  
    },
    /** Async! Saves the current scenario. */
    saveScenario = async function() {
        await Controler.reportScenarioToSave();
        closeDialog();
    },
    /** Updates the loadable scenarios. */
    updateScenariosLoadable = function() {
        const containerElement = objectRepository.scenarioSelectionContainer();

        containerElement.innerHTML = '';

        const scenarioNames = Controler.requestScenarioNames();

        let scenarioName;
        for(scenarioName of scenarioNames) {
            const scenarioEntryCurrent = document.createElement('div');

            const scenarioEntryCurrentClassList = scenarioEntryCurrent.classList;
            scenarioEntryCurrentClassList.add(classNameRepository.scenarioList);

            const idCurrent = idNameRepository.loadScenarioPart + scenarioName;

            const inputCurrent = document.createElement('input');
            inputCurrent.id = idCurrent;
            inputCurrent.setAttribute('type', 'checkbox');
            inputCurrent.setAttribute('name', nameAttributeRepository.checkboxGroup);
            inputCurrent.setAttribute(attributeCustomRepository.scenarioName, scenarioName);
            inputCurrent.addEventListener('click', function() {updateAfterSelectionScenarioLoadable(this)});
            scenarioEntryCurrent.append(inputCurrent);

            const labelCurrent = document.createElement('label');
            labelCurrent.setAttribute('for', idCurrent);
            labelCurrent.innerHTML = scenarioName;
            scenarioEntryCurrent.append(labelCurrent);

            containerElement.append(scenarioEntryCurrent);
        }
    },
    /** Updates multiple elements after the selection of an loadable scenario changes.
     * @param {HTMLInputElement?} elementContainingEvent the input element which triggered the event or null
     */
    updateAfterSelectionScenarioLoadable = function(elementContainingEvent) {
        // update checkboxes
        /** Boolean value if the checkbox from this event was not checked.
         * @type {boolean} checked status BEVORE click (gets inverted! e.g.: checkbox was unchecked, click checks it and now the call returns checked)
         */
        let wasNotChecked   
        if(elementContainingEvent !== null) {
            wasNotChecked = elementContainingEvent.checked;
        

            const scenarioLoadableCheckboxes = document.getElementsByName(nameAttributeRepository.checkboxGroup);
            let scenarioLoadableCheckboxCurrent
            for(scenarioLoadableCheckboxCurrent of scenarioLoadableCheckboxes) {
                scenarioLoadableCheckboxCurrent.checked = false;
            }

            if(wasNotChecked) {
                elementContainingEvent.checked = true;
            }
        } else {
            wasNotChecked = false;
        }

        // update save and load button
        const scenarioNameInput = objectRepository.scenarioNameInput();
        const scenarioSaveButton = objectRepository.scenarioSaveButton();
        const scenarioLoadButton = objectRepository.scenarioLoadButton();
        const scenarioDeleteButton = objectRepository.scenarioDeleteButton();
        const scenarioResetButton = objectRepository.scenarioResetButton();
        if(wasNotChecked) {
            scenarioNameInput.disabled = true;
            scenarioSaveButton.disabled = true;
            scenarioResetButton.disabled = true;

            scenarioLoadButton.disabled = false;
            scenarioDeleteButton.disabled = false;
        } else {
            scenarioNameInput.disabled = false;
            scenarioSaveButton.disabled = false;
            scenarioResetButton.disabled = false;

            scenarioLoadButton.disabled = true;
            scenarioDeleteButton.disabled = true;
        }
    },
    /** TODO */
    loadAndUpdateScenario = function() {
        // find scenario to load
        const scenarioLoadableCheckboxes = document.getElementsByName(nameAttributeRepository.checkboxGroup);
        let scenarioLoadableCheckboxCurrent, elementContainingEvent, selectionName;
        for(scenarioLoadableCheckboxCurrent of scenarioLoadableCheckboxes) {
            const isChecked = scenarioLoadableCheckboxCurrent.checked

            if(isChecked) {
                elementContainingEvent = scenarioLoadableCheckboxCurrent;
                selectionName = scenarioLoadableCheckboxCurrent.getAttribute(attributeCustomRepository.scenarioName);
                break;
            }
        }

        // load scenario
        const isSuccess = Controler.reportScenarioToLoad(selectionName);

        if(isSuccess) {
            updateLevelEditorFull(elementContainingEvent);
        }
    },
    /** Async! Delete the selected scenario */
    deleteScenario = async function() {
        // find scenario to delete
        const scenarioLoadableCheckboxes = document.getElementsByName(nameAttributeRepository.checkboxGroup);

        let scenarioLoadableCheckboxCurrent, selectionName;
        for(scenarioLoadableCheckboxCurrent of scenarioLoadableCheckboxes) {
            const isChecked = scenarioLoadableCheckboxCurrent.checked

            if(isChecked) {
                selectionName = scenarioLoadableCheckboxCurrent.getAttribute(attributeCustomRepository.scenarioName);
                break;
            }
        }

        // delete
        const isSuccess = await Controler.reportScenarioToDelete(selectionName);

        // update dialog
        if(isSuccess) {
            updateScenariosLoadable();
            updateAfterSelectionScenarioLoadable(null);
        } 
    },
    /** TODO */
    resetScenario = function() {
        Controler.reportScenarioToReset();
        updateLevelEditorFull(null);
    },
    /** TODO javascript doc. */
    importScenario = function() {
        Controler.reportImportScenario();
    },
    /** TODO javascript doc. */
    exportScenario = function() {
        Controler.reportExportScenario();
    },
    /** TODO */
    updateSizeDialogCounter = function() {
        const currentGameMap = Controler.requestTerrains();
        const xSize = currentGameMap.length;
        const ySize = currentGameMap[0].length;

        objectRepository.currentXsizeValue().innerHTML = xSize;
        objectRepository.currentYsizeValue().innerHTML = ySize;
    },
    /** TODO */
    changeGameMapElement = function(xCoordinate, yCoordinate) {
        let isTerrain = false;
        let isUnit = false;
        let elementIdIndex, elementIdFound, coloumElement, terrainElement, canMoveOnIt, unitElementInFrontend;    
        const elementId = Controler.requestSetableElement();

        // exit when no selection was made
        if(elementId == 'none') {
            return;
        }

        if(elementId == 'unitReachedFieldPosition') {
            Controler.reportLosingConditionStatusChange('unitReachedField', xCoordinate, yCoordinate, true);
            openSetLosingConditionDialog();
            Controler.reportSetableElement('none');
            return;
        }

        // check type of selection
        const terrainIds = Controler.requestTerrainIds();
        elementIdIndex = terrainIds.indexOf(elementId);   
        
        if(elementIdIndex !== -1) {
            elementIdFound = terrainIds[elementIdIndex];
            isTerrain = true; 
        } else {
            const units = Controler.requestUnitIds();
            elementIdIndex = units.indexOf(elementId);
            if(elementIdIndex !== -1) {
                elementIdFound = units[elementIdIndex];
                isUnit = true;
            }
        }

        // handle selection
        if(!isTerrain && !isUnit) {
            Controler.reportError('Change Element on game map failed! Setable element id |' + elementId + '| is unknown!');
        }
        
        const coloumElementId = idNameRepository.mapRow + yCoordinate + idNameRepository.column + xCoordinate;
        coloumElement = document.getElementById(coloumElementId);
        terrainElement = coloumElement.getElementsByTagName('img')[0]; // TODO switch to svg

        unitElementInFrontend  = coloumElement.getElementsByTagName('svg')[0]; // TODO fix to [1] when all terrains are svgs too!
        
        if(isTerrain) {
            const unitIdInBackend = Controler.requestUnitIdStart(xCoordinate, yCoordinate);
            if(unitIdInBackend !== null) {
                canMoveOnIt = Controler.requestIsMovableOnTerrain(unitIdInBackend, elementIdFound);
                if(!canMoveOnIt) {
                    // model - delete unit
                    Controler.reportUnitStart(xCoordinate, yCoordinate, null, null);
                    // view - delete unit
                    if(unitElementInFrontend) {
                        coloumElement.removeChild(unitElementInFrontend);
                    } else {
                        Controler.reportError('Deleting Unit in Frontend failed, but existed in Backend bevore! Check Element with id: |' + coloumElement.id  + '|')
                    }
                    // view - give terrain listener again
                    terrainElement.addEventListener('click', function(){changeGameMapElement(xCoordinate, yCoordinate)});      
                }
            }
           
            // model - set terrain
            Controler.reportTerrain(xCoordinate, yCoordinate, elementId);
            // view  - set terrain
            
            terrainElement.src = getTerrainFileString(elementId);
            terrainElement.setAttribute('terrain', elementId); // set custom element attribute

            return;
        }

        if(isUnit) {
            // check valid terrain for unit  
            const terrainNameBelow = terrainElement.getAttribute('terrain');
            
            canMoveOnIt = Controler.requestIsMovableOnTerrain(elementIdFound, terrainNameBelow);
            if(canMoveOnIt) {
                // model - set unit
                const playerIdCurrent = Controler.requestPlayerIdSelection();  
                Controler.reportUnitStart(xCoordinate, yCoordinate, elementIdFound, playerIdCurrent);  
                // view - delete old unit  
                if(unitElementInFrontend) {
                    unitElementInFrontend.parentNode.removeChild(unitElementInFrontend);
                }

                // view - set new unit
                createAndAppendUnitPictureElement(elementId, classNameRepository.unitGraphicOnGameMap, coloumElement, xCoordinate, yCoordinate);             
            } else {
                console.log('Nope! Hier kann ich keine Einheit absetzen!') // TODO controler.report ... make audio sound
            }   
            
            return;
       }

       Controler.reportError('Change Element on game map failed! Setable element id |' + elementId + '| is unknown!');
    },
    /** TODO */
    changeGameMapSize = function(xFieldDifference, yFieldDifference) {
        const currentGameMap = Controler.requestTerrains();
        const gameMapXFieldsBevore = currentGameMap.length;
        const gameMapYFieldsBevore = currentGameMap[0].length;
        const gameMapXFieldMin = 2;
        const gameMapYFieldMin = 2;
        let gameMapXFieldsAfter, gameMapYFieldsAfter;

        gameMapXFieldsAfter = gameMapXFieldsBevore + xFieldDifference;
        gameMapYFieldsAfter = gameMapYFieldsBevore + yFieldDifference;

        if(gameMapXFieldsAfter === (gameMapXFieldMin -1)) {
            gameMapXFieldsAfter = gameMapXFieldMin;
        }
        if(gameMapYFieldsAfter === (gameMapYFieldMin - 1)) {
            gameMapYFieldsAfter = gameMapYFieldMin;
        }

        if(gameMapXFieldsBevore === gameMapXFieldsAfter && gameMapYFieldsBevore === gameMapYFieldsAfter){
            return
        }      

        // model
        const unitsCurrent = Controler.requestUnitsStart();

        Controler.reportGameMap(gameMapXFieldsAfter, gameMapYFieldsAfter, currentGameMap, unitsCurrent);
        
        // view  
        updateSizeDialogCounter();
        updateGameMap();
    },
    /** TODO */
    setSetableElement = function(terrainId) {
        Controler.reportSetableElement(terrainId);
    },
    /** Set the setable element to  */
    setSetableElementUnitReachedFieldPosition = function() {
        Controler.reportSetableElement('unitReachedFieldPosition');
        closeDialog()
    },
    /** TODO */
    changePlayerStartSize = function(isAddPlayer){
        let isUpdated;
        if(isAddPlayer) {
            isUpdated = Controler.reportAddPlayerStart();
        } else { // delete Player
            isUpdated = Controler.reportDeletePlayerStart();
        }

        if(isUpdated) {
            updatePlayerCounter();
            updateSetPlayerDialogForColors();

            if(!isAddPlayer) {
                updateGameMap();
            }
        }
    },

    // helper functions
    /** TODO */
    updatePlayerCounter = function() {
        const playerCount = Controler.requestPlayersStartSize();

        objectRepository.playerValueCurrent().innerHTML = playerCount;
    },

    /** TODO */
    updateSetPlayerDialogForColors = function() {
        const dialogElement = objectRepository.setPlayerDialog();

        // reset - but left permanent elements untouched
        const setPlayerDialogTitle = objectRepository.setPlayerDialogTitle();
        const playerSelectionContainer = objectRepository.playerSelectionContainer();
        const playerContainer = objectRepository.playerContainer();

        dialogElement.innerHTML = '';
        dialogElement.append(setPlayerDialogTitle);
        dialogElement.append(playerSelectionContainer);
        playerContainer.innerHTML = '';
        dialogElement.append(playerContainer);

        // add values
        const playersStartSize = Controler.requestPlayersStartSize();
        let playerIdCurrent;
        for(playerIdCurrent = 1; playerIdCurrent <= playersStartSize; playerIdCurrent++) {
            const playerIdToWrite = playerIdCurrent;
            const playerColorClassNameCurrent = Controler.requestPlayerColorClassNameOfPlayer(playerIdCurrent);

            // surrounding containerElement
            const trackContainerElement = document.createElement('div');
            trackContainerElement.classList.add('playerEntry')

            // arrowLeft
            const arrowLeftButton = document.createElement('button');
            arrowLeftButton.id = idNameRepository.playerColorPart1 + playerIdCurrent + idNameRepository.playerColorPart2BevoreButton;

            const arrowLeftPictureElement = document.createElement('img');
            arrowLeftPictureElement.src = './media/pic/unitOrTerrainDialog/arrowLeft.jpg'; // TODO move to fileStringRepository#
            arrowLeftPictureElement.classList.add('menuIcon');

            arrowLeftButton.append(arrowLeftPictureElement);
       
            arrowLeftButton.addEventListener('click', function() {Controler.reportChangePlayerStartColor(playerIdToWrite, -1)});
            trackContainerElement.append(arrowLeftButton);
            
            // playerColorElement
            const playerColorElement = document.createElement('p');
            playerColorElement.id = idNameRepository.playerColorPart1 + playerIdCurrent + idNameRepository.playerColorPart2Value;
            playerColorElement.innerHTML = 'P' + playerIdCurrent;
            playerColorElement.classList.add(playerColorClassNameCurrent);

            trackContainerElement.append(playerColorElement);

            // arrowRight
            const arrowRightButton = document.createElement('button');
            arrowRightButton.id = idNameRepository.playerColorPart1 + playerIdCurrent + idNameRepository.playerColorPart2NextButton;

            const arrowRightPictureElement = document.createElement('img');
            arrowRightPictureElement.src = './media/pic/unitOrTerrainDialog/arrowRight.jpg'; // TODO move to fileStringRepository#
            arrowRightPictureElement.classList.add('menuIcon');

            arrowRightButton.append(arrowRightPictureElement);
 
            arrowRightButton.addEventListener('click', function() {Controler.reportChangePlayerStartColor(playerIdToWrite, 1)});
            trackContainerElement.append(arrowRightButton);

            playerContainer.append(trackContainerElement);
        }
    },

    /** TODO */
    updateGameMap = function(){
        const currentMap = Controler.requestTerrains();
        const xFieldsLength = currentMap.length;
        const yFieldsLength = currentMap[0].length;
        const gameMapElement = document.getElementById('gameMap');
        let currentRow, currentTerrainId, currentTerrainFileString;

        gameMapElement.textContent = ''; // reset current element structure of game map

        for(let y = 0; y < yFieldsLength; y++) {
            currentRow = document.createElement('tr');
            currentRow.id = 'mapRow' + y;

            for(let x = 0; x < xFieldsLength; x++) {
                const currentColumn = document.createElement('td');
                currentColumn.id = 'mapRow' + y + 'Column' + x;
    
                currentTerrainId = currentMap[x][y].id;
                currentTerrainFileString = getTerrainFileString(currentTerrainId);

                const currentTerrainElement = document.createElement('img');
                currentTerrainElement.classList.add(classNameRepository.terrainGraphicOnGameMap);
                currentTerrainElement.src = currentTerrainFileString;
                currentTerrainElement.setAttribute('terrain', currentTerrainId); // set custom element attribute

                currentTerrainElement.addEventListener('click', function(){changeGameMapElement(x, y)});

                currentColumn.appendChild(currentTerrainElement);

                const unitId = Controler.requestUnitIdStart(x, y);
                if(unitId) {
                    createAndAppendUnitPictureElement(unitId, classNameRepository.unitGraphicOnGameMap, currentColumn, x, y);
                }

                currentRow.appendChild(currentColumn);
            }

            gameMapElement.appendChild(currentRow);
        }
    },
    /** TODO */
    updateLevelEditorFull = function(elementContainingEvent) {
        updateGameMap();
        updateSetPlayerDialogForColors();

        closeDialog();
        updateScenarioName(false);
        updateAfterSelectionScenarioLoadable(elementContainingEvent);
    },

    /** Create an inline svg graphic element for an given parent element and add it as a child. Can optional get an event listener to 
     * @param {string} unitId the id of the unit to place
     * @param {string} className an css class to add to the graphic element
     * @param {number?} xCoordinate the x coordinate of the unit on the game map
     * @param {number?} yCoordinate the y coordinate of the unit on the game map
     */
     createAndAppendUnitPictureElement = function(unitId, className, picParentElement, xCoordinate = undefined, yCoordinate = undefined) {
        const currentElementInnerHTML = Controler.requestUnitPictureString(unitId);
        let currentPlayerColor;
        currentPlayerColor = Controler.requestPlayerColorClassNameOfUnit();

        picParentElement.innerHTML = picParentElement.innerHTML + currentElementInnerHTML; // equal to elementWithPicButton.append(picElement)
        
        const picElement = picParentElement.getElementsByTagName('svg')[0]; 

        if(xCoordinate !== undefined && yCoordinate !== undefined) { // parentElement must be a coloumElement
            picElement.addEventListener('click', function(){changeGameMapElement(xCoordinate, yCoordinate)});

            currentPlayerColor = Controler.requestPlayerColorClassNameOfUnit(xCoordinate, yCoordinate);
        } else {
            currentPlayerColor = Controler.requestPlayerColorClassNameOfUnit();
        }

        picElement.classList.add(className);
        picElement.classList.add(currentPlayerColor);
        picElement.setAttribute('unit', unitId); // set custom element attribute
    }

    // public API / oeffentliche API
    return {
        /** TODO
         */
        reportUpdateGameMap: function(isUpdateSetScenarioDialogwanted = false) {
            updateGameMap();

            if(isUpdateSetScenarioDialogwanted) {
                closeDialog();
                openSetScenarioDialog();
            }
        },

        /** TODO
         * @param {number} playerId 
         */
        updatePlayerColorValueElementOfSetPlayerDialog: function(playerId) {
            //playerColorElement
            const playerColorElement = objectRepository.playerColorValue(playerId);
            const playerColorElementClassList = playerColorElement.classList;

            // remove old class
            let currentClassName;
            for(currentClassName of playerColorElementClassList) {
                if(currentClassName.includes(classNameRepository.playerColorPart)) {
                    playerColorElementClassList.remove(currentClassName);
                    break
                }
            }

            // add new class
            const playerColorClassNameCurrent = Controler.requestPlayerColorClassNameOfPlayer(playerId);
            playerColorElementClassList.add(playerColorClassNameCurrent);
        }
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function(){
    initView();
    console.log('View module is ready. |' + Date(Date.now()).slice(4,24));
}, false)
