/** eng: model class. Contains all data for the application.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: gameMap.js
const Model = (function () { // encapsulation / Kapselung
    // stored values from view / gespeicherte Werte aus der View
    /**
     * TODO
     */
    const classNameRepository = {
        classNamePrefix: 'playerColor'
    }
    
    /** Stores the setable element id for the level editor.
     * @type {string} setable element id
     */
    let elementIdSetableInLeveleditor = 'none';
    /**
     * Stores the current selection value of the unit or terrain dialog for the level editor.
     * @type {string} current selection value
     */
    let unitOrTerrainDialogSelectionCurrent = 'setTerrainDialog';
    /**
     * Stores the current selection value of the player id for the level editor.
     * @type {number} current selection value
     */
    let playerIdSelectionCurrent = 0;
    /** Localy stored scenarios.
     * @type {Map<string, {}} stored scenarios
     */
    let scenarios;


    // values initialized once / einmalig initialisierte Werte
    /** Stores the ids of all known terrains.
     * @type {[string]} all terrain ids
     */
    let terrainIds;
    /** Stores the ids of all known units.
     * @type {[string]} all unit ids
     */
    let unitIds;
    let unitPictures;

    // "private" functions / "private" Funktionen
    initScenario = function() {
        Scenario.resetScenario();
        Model.addPlayerStart();
        Model.addPlayerStart();
    },

    initUnitInlineSvgs = function() {
        unitPictures = UnitPicture.getUnitPictures();
    },

       
    filterTerrainIds = function() {
        const result = [];
        const allTerrains = Terrain.getAllTerrains();
        let currentMapValue;

        for(currentMapValue of allTerrains.values()){
            const currentId = currentMapValue.id;
            result.push(currentId);
        }

        return result;
    },

    filterUnitIds = function() {
        const result = [];
        const units = Unit.getAllUnits();
        let unitCurrent;

        for(unitCurrent of units.values()){
            const idCurrent = unitCurrent.id;
            result.push(idCurrent);
        }

        return result;
    },

    getUnusedColors = function() {
        const colorsAll = Player.getColors();
        const colorsAllLength = colorsAll.length;
        const colorAllMaxIndex = colorsAllLength - 1;

        const playersStartCurrent = Scenario.getPlayersStart();
        let colorsPossible, colorPossibleCurrent, colorPlayerCurrent;

        colorsPossible = colorsAll;
        for(let i = colorAllMaxIndex; i >= 0; i--) {
            colorPossibleCurrent = colorsPossible[i];
            for (let playerCurrent of playersStartCurrent.values()) {
                colorPlayerCurrent = playerCurrent.color;

                if(colorPossibleCurrent === colorPlayerCurrent) {
                    colorsPossible.splice(i, 1); // javascript don't have array.remove(index)
                    break;
                }
            }
        }


        return colorsPossible
    },

    getColorPossibleFirst = function() {
        const colorsPossibleNOW = getUnusedColors(); // TODO rename to colorsPossible
        const colorCurrent = colorsPossibleNOW[0];
        return colorCurrent
    },
    getColorPossibleNextUp = function(playerId) {
        let colorNewCandidate, colorNextUp, isColorNewFound;

        const playerStart = Scenario.getPlayerStart(playerId);
        const colorBevore = playerStart.color;

        const colorsAll = Player.getColors();
        const colorBevoreIndexFromAll = colorsAll.indexOf(colorBevore);

        const colorsPossible = getUnusedColors();
        const colorPossibleLength = colorsPossible.length;

        if(colorPossibleLength === 0) {
            return colorBevore;
        }

        const colorPossibleLastIndex = colorPossibleLength - 1;
        const colorPossibleLast = colorsPossible[colorPossibleLastIndex];
        const colorPossibleLastIndexFromAll = colorsAll.indexOf(colorPossibleLast);

        
        if(colorPossibleLastIndexFromAll < colorBevoreIndexFromAll) {
            colorNextUp = colorsPossible[0];
        } else {
            const colorsAllLength = colorsAll.length;
            const colorsAllLastIndex = colorsAllLength - 1;

            isColorNewFound = false;
            for(let i = colorBevoreIndexFromAll; i <= colorsAllLastIndex; i++) {
                colorNewCandidate = colorsAll[i];
                if(colorsPossible.includes(colorNewCandidate)) {
                    colorNextUp = colorNewCandidate;
                    isColorNewFound = true;
                    break
                }
            }
            if(!isColorNewFound) {
                Controler.reportError('No possible color candidate was found! Check current colors of players and what should be the next expected Color. Search direction was up.')
                colorNextUp = undefined;
            }
        }

        return colorNextUp
    },
    getColorPossibleNextDown = function(playerId) {
        let colorNewCandidate, colorNextDown, isColorNewFound;

        const playerStart = Scenario.getPlayerStart(playerId);
        const colorBevore = playerStart.color;

        const colorsAll = Player.getColors();
        const colorBevoreIndexFromAll = colorsAll.indexOf(colorBevore);

        const colorsPossible = getUnusedColors();
        const colorsPossibleLength = colorsPossible.length;

        if(colorsPossibleLength === 0) {
            return colorBevore;
        }

        const colorPossibleFirst = colorsPossible[0];
        const colorPossibleFirstIndexFromAll = colorsAll.indexOf(colorPossibleFirst);

       
        if(colorPossibleFirstIndexFromAll > colorBevoreIndexFromAll) {
            
            const colorsPossibleLastIndex = colorsPossibleLength - 1;
            colorNextDown = colorsPossible[colorsPossibleLastIndex];
        } else {
            isColorNewFound = false;
            for(let i = colorBevoreIndexFromAll; i >= 0; i--) {
                colorNewCandidate = colorsAll[i];

                if(colorsPossible.includes(colorNewCandidate)) {
                    colorNextDown = colorNewCandidate;
                    isColorNewFound = true;
                    break
                }
            }
            if(!isColorNewFound) {
                Controler.reportError('No possible color candidate was found! Check current colors of players and what should be the next expected Color. Search direction was down.')
                colorNextDown = undefined;
            }
        }

        return colorNextDown
    }

    // public API / oeffentliche API
    return {
        /** Resets the model. Resets the current scenario too.
         */
        resetModel: function() {
            terrainIds = filterTerrainIds();
            unitIds = filterUnitIds();
            scenarios = new Map();

            elementIdSetableInLeveleditor = 'none';
            unitOrTerrainDialogSelectionCurrent = 'setTerrainDialog';
            playerIdSelectionCurrent = 0;

            initScenario();
        },
        
        // scenario
        /** Async! TODO
         * @returns {boolean}
         */
        saveScenario: async function() {
            const scenarioCurrent = Scenario.getScenario();
            const scenarioName = scenarioCurrent.name;

            // store local
            scenarios.set(scenarioName, scenarioCurrent);
            // store permanent
            await SaveAndLoad.saveScenarios(scenarios);
           
            // TODO implement error handling;
            return true;
        },
        /** Async! TODO
         * @param {string} scenarioName 
         * @returns {boolean}
         */
        deleteScenario: async function(scenarioName) {
            const isScenarioCurrent = (scenarioName === Model.getScenarioName());

            // delete local
            scenarios.delete(scenarioName);
            // delete permanent trough overwriting store;
            const isSuccess = await SaveAndLoad.saveScenarios(scenarios);

            // TODO implement error handling;
            return isSuccess;
        },
        /** Async! TODO
         * @returns {Promise<boolean>} TODO
         */
        loadScenarios: async function() {
            // load from permanent store
            const scenariosLoaded = await SaveAndLoad.loadScenarios();

            if(scenariosLoaded) {
                const isExpectedInstance = (scenariosLoaded instanceof Map)
                if(isExpectedInstance) {
                    scenarios = scenariosLoaded;
                    return true;
                }
            }
            
            return false;
        },
        /** TODO
         * @returns {string[]} TODO
         */
        getScenarioNames: function() {
            const keys = scenarios.keys();
            const keysFormated = Array.from(keys);

            return keysFormated
        },
        /** TODO
         * @param {string} scenarioName TODO
         * @returns {boolean} TODO
         */
        loadScenario: function(scenarioName) {          
            const scenarioNew = scenarios.get(scenarioName);

            if(scenarioNew) {
                Scenario.setScenario(scenarioNew);
                return true;
            } else {
                return false;
            }        
        },
        /** TODO
         * @param {{}} scenarioNew  TODO
         * @param {boolean} minimalData
         */
        setScenario: function(scenarioNew, minimalData = undefined) {
           Scenario.setScenario(scenarioNew, minimalData);
        },
        /** TODO
         */
        resetScenario: function() {
            initScenario();
        },

        // scenario name
        /** Get the scenario name.
         * @returns {string} the scenario name
         */
        getScenarioName() {
            return Scenario.getName();
        },
        /** Set the scenario name.
         * @param {string} name the new scenario name
         */
        setScenarioName(name) {
            Scenario.setName(name);
        },

        // players
        /** Try to add a new player at start of a scenario.
         * @returns {boolean} boolean value of success from execution
         */
        addPlayerStart() {
            // check if another player can be added
            const playersStart = Scenario.getPlayersStart();
            const playersStartSize = playersStart.size;

            const colorsPossible = Player.getColors();
            const colorsPossibleSize = colorsPossible.length;

            if(playersStartSize >= colorsPossibleSize) {               
                return false;
            }

            const idCurrent = playersStartSize + 1;         
            const colorCurrent = getColorPossibleFirst();
            
            // build Player object
            const playerCurrent = Player.getPlayerTemplate();
            playerCurrent.id = idCurrent;
            playerCurrent.color = colorCurrent;

            Scenario.setPlayerStart(idCurrent, playerCurrent);
            
            return true;
        },
        /** Try to delete a the last player at start of a scenario.
         * @returns {boolean} boolean value of success from execution
         */
        deletePlayerStart() {
            // check if another player can be deleted
            const playersStart = Scenario.getPlayersStart();
            const playersStartSize = playersStart.size;

            if(playersStartSize <= 2) {               
                return false;
            }

            const playerIdDelete = playersStartSize;
            Scenario.deletePlayerStart(playerIdDelete);

            if(playerIdSelectionCurrent === playerIdDelete) {
                const playerIdBevore = playerIdDelete - 1;
                playerIdSelectionCurrent = playerIdBevore;
            }
            
            return true;
        },
        /** Get a specific player at the start of a scenario.
         * @param {number} id id of the player
         * @returns {Object} specific player
         */
        getPlayerStart(id) {
            return Scenario.getPlayerStart(id);
        },
        /** Get Size of players at the start of an scenario.
         * @returns {number} size of players at start
         */
        getPlayersStartSize: function() {
            return Scenario.getPlayersStartSize();
        },

        // player colors
        /** Try to change the color of a player at the start of an scenario in a set direction.
         * Direction can be -1 (preview value) or 1 (next value). 
         * @param {number} playerId id of the player
         * @param {number} changeDirectionValue direction of the color change 
         * @returns {boolean} boolean value of success from execution
         */
        changePlayerStartColor: function(playerId, changeDirectionValue) {
            let colorNew, colorNewCandidate;

            // find value
            const isSearchForNextFreeColor = (changeDirectionValue === 1);
            if(isSearchForNextFreeColor) {
                colorNewCandidate = getColorPossibleNextUp(playerId);
            } else { // - 1
                colorNewCandidate = getColorPossibleNextDown(playerId);
            }        
            if(!colorNewCandidate) {
                return false;
            }
            colorNew = colorNewCandidate;
            
            const playerStart = Scenario.getPlayerStart(playerId);
            const colorBevore = playerStart.color;

            if(colorBevore !== colorNew) {
                // set value
                playerStart.color = colorNew;
                Scenario.setPlayerStart(playerId, playerStart);

                return true;
            } else {
                return false;
            }
        },   
        /** Get player color class name through a unit or the saved player id selection. 
         * @param {number?} xCoordinate x coordinate of a unit or null
         * @param {number?} yCoordinate y coordinate of a unit or null
         * @returns {string} class name of player color
         */
        getPlayerColorClassNameOfUnit: function(xCoordinate = null, yCoordinate = null) { // TODO check if its not smarter to move the selection part of the id selection into "getPlayerColorClassNameOfPlayer(...)"
            let playerIdCurrent, playerColor;  

            if(xCoordinate != null && yCoordinate != null) {
                const unitCurrent = Scenario.getUnitStart(xCoordinate, yCoordinate);

                if(unitCurrent) {
                    playerIdCurrent = unitCurrent.ownerCurrent;
                } else {
                    return '';
                }        
            } else {
                playerIdCurrent = playerIdSelectionCurrent;
            }

            if(playerIdCurrent === 0) {
                playerColor = Player.getNeutralColor();
            } else {
                const player = Scenario.getPlayerStart(playerIdCurrent);
                playerColor = player.color;
            }
            // TODO add playerColorAdepted; playerColorAdepted = playerColor (... first letter big); className = classNamePrefix + playerColorAdepted;
            const className = classNameRepository.classNamePrefix + playerColor;

            return className;
        },
        /** Get player color class name through player id. 
         * @param {number} playerId id of the player
         * @returns {string} class name of player color
         */
        getPlayerColorClassNameOfPlayer: function(playerId) {
            let playerColor;  

            const player = Scenario.getPlayerStart(playerId);
            playerColor = player.color;
            
            // TODO add playerColorAdepted; playerColorAdepted = playerColor (... first letter big); className = classNamePrefix + playerColorAdepted;
            const className = classNameRepository.classNamePrefix + playerColor;

            return className;
        },
        /** Get the current player id selection.
         * @returns {number} player id selection
         */
        getPlayerIdSelection: function() {
            return playerIdSelectionCurrent;
        },
        /** Change the current player id selection in a given direction.
         * Direction can be -1 (preview value) or 1 (next value).
         * Reaching an end of the possible selections will set it to the value of the other end.
         * (Example with 2 players and current selection at 0 (neutral): change in direction back will set selection to 2)
         * @param {number} changeDirectionValue the direction for the change
         */
        changePlayerIdSelection: function(changeDirectionValue) {
            const playerIdMax = Scenario.getPlayersStartSize();

            let possibleSelection =  playerIdSelectionCurrent + changeDirectionValue; // -1 or 1
            
            if(possibleSelection < 0) {  
                possibleSelection = playerIdMax;
            }

            if(possibleSelection > playerIdMax) {  
                possibleSelection = 0;
            }

            playerIdSelectionCurrent = possibleSelection;
        },

        // losing conditions
        /** Get the losing conditions from an player.
         * @param {number} playerId the id of an player
         * @returns {[{}]} the  losing conditions from an player
         */
        getLosingConditions: function(playerId) {
            return Scenario.getLosingConditions(playerId);
        },
        /** Change (toogle) a given losing condition for the currently selected player.
         * @param {string} losingConditionId the id of the losing condition
         * @param {number?} xCoordinate the relevant x coordinate for the losing condition
         * @param {number?} yCoordinate the relevant y coordinate for the losing condition
         * @param {boolean} isUpdateWanted boolean value, if an existing losing condition should be updated (instead of beeing deleted)
         * @returns {boolean} boolean result of the operation
         */
        changeLosingCondition: function(losingConditionId, xCoordinate = null, yCoordinate = null, isUpdateWanted = false) {
            const losingConditionIdSpecialCase = 'unitReachedField';
            const playerIdSelection = playerIdSelectionCurrent;

            let losingConditionsPlayer = Scenario.getLosingConditions(playerIdSelection);

            let isExisting;
            for(let losingCondition of losingConditionsPlayer) {
                isExisting = (losingCondition.id === losingConditionId);

                if(isExisting) {
                    break;
                }
            }  

            if(xCoordinate === null && losingConditionId === losingConditionIdSpecialCase) {
                xCoordinate = 0;
            }
            if(yCoordinate === null && losingConditionId === losingConditionIdSpecialCase) {
                yCoordinate = 0;
            }

            if(isExisting) {
                if(isUpdateWanted) {
                    return Scenario.addLosingCondition(playerIdSelection, losingConditionId, xCoordinate, yCoordinate);
                } else {
                    return Scenario.deleteLosingCondition(playerIdSelection, losingConditionId);
                }    
            } else {
                return Scenario.addLosingCondition(playerIdSelection, losingConditionId, xCoordinate, yCoordinate);
            }
        },

        // terrains and units
        /** Set the game maps.
         * @param {number} xFieldsLength count of fields (x axis)
         * @param {number} yFieldsLength count of fields (y axis)
         * @param {[[{}]]} gameMapTemplate 2D array of terrain to use as template
         * @param {[[{}]]} unitPlacementTemplate 2D array of units to use as template
         */   
        setGameMaps: function(xFieldsLength, yFieldsLength, gameMapTemplate = undefined, unitPlacementTemplate = undefined) {         
            Scenario.setGameMaps(xFieldsLength, yFieldsLength, gameMapTemplate, unitPlacementTemplate);
        },
        /** Get the boolean value if an unit can move on the given terrain.
         * @param {string} unitId id of the unit
         * @param {string} terrainId id of the terrain
         * @returns {boolean} result of this operation
         */
        isMovableOnTerrain: function(unitId, terrainId){
            const units = Unit.getAllUnits();
            const unitCurrent = units.get(unitId);
            const unitMovementType = unitCurrent.movementType;

            const terrains = Terrain.getAllTerrains(); 
            const terrainCurrent = terrains.get(terrainId);
            const movementCosts = terrainCurrent.movementCosts;

            const movementCostCurrent = movementCosts[unitMovementType];
            if(movementCostCurrent === 0) {
                return false;
            } else {
                return true;
            }
        },

        // terrains
        /** TODO
         * @returns 
         */
        getTerrainIds: function() {
            const terrainIdsReferenceless = terrainIds.slice(0);
            return terrainIdsReferenceless;
        },
        /** Get the terrains of the game map.
         * @returns {[[Object]]} terrains of the game map
         */
        getTerrains: function() {
            return Scenario.getTerrains();
        },
        /** Get the terrain id at given coordinates on the game map.
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @returns {string} the found terrain id
         */
        getTerrainId(xCoordinate, yCoordinate) {
            const terrainCurrent = Scenario.getTerrain(xCoordinate, yCoordinate);
            const terrainId = terrainCurrent.id;
            return terrainId; 
        },
        /** Set the terrain at given coordinates on the game map.
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @param {string} terrainId the id of the terrain to set
         * @returns {boolean} result of this operation
         */
        setTerrain(xCoordinate, yCoordinate, terrainId) {
            const terrains = Terrain.getAllTerrains();
            const terrainCurrent = terrains.get(terrainId);

            if(terrainCurrent) {
                Scenario.setTerrain(xCoordinate, yCoordinate, terrainCurrent);
                return true;
            } else {
                return false;
            }   
        },
    
        // units
        /** TODO
         * @returns 
         */
        getUnitIds: function() {
            const unitIdsReferenceless = unitIds.slice(0);
            return unitIdsReferenceless;
        },
        /** Get the unit id at given coordinates on the game map.
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @returns {string?} the found unit id (or null when there was no unit at the given coordinates)
         */
        getUnitIdStart: function(xCoordinate, yCoordinate){        
            let result;
            const unitCurrent = Scenario.getUnitStart(xCoordinate, yCoordinate);

            if(unitCurrent) {
                const unitId = unitCurrent.id;
                result = unitId;
            } else {
                result = null;
            }
            return result;
        },
        /** Get the units of the game map at the start of the scenario.
         * @returns {[[Object]]} units of the game map at the start of the scenario.
         */
        getUnitsStart: function() {
            return Scenario.getUnitsStart();
        },
        /** Set a specific unit on the game map.
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @param {string?} unitId the id of the unit to set (or null when there should be no unit at the given coordinates)
         * @param {number} playerId the id of the player who own the unit
         * @returns {boolean} result of this operation
         */
        setUnitStart: function(xCoordinate, yCoordinate, unitId, playerId = 1){        
            if(unitId) {
                const units = Unit.getAllUnits();
                const unitCurrent = units.get(unitId);

                if(unitCurrent) {
                    unitCurrent.ownerCurrent = playerId;
                    Scenario.setUnitStart(xCoordinate, yCoordinate, unitCurrent);
                    return true;
                } else {
                    return false;
                }
            } else {
                Scenario.setUnitStart(xCoordinate, yCoordinate, undefined);
                return true
            }
        },     

        // stored view selections
        /** TODO
         * @param {*} elementId 
         */
        setSetableElement: function(elementId) {
            elementIdSetableInLeveleditor = elementId;
        },
        /** TODO
         * @returns 
         */
        getSetableElement: function() {
            return elementIdSetableInLeveleditor;
        },
        /** TODO
         * @param {*} selectionName 
         */
        setUnitOrTerrainDialogSelection: function(selectionName) {
            unitOrTerrainDialogSelectionCurrent = selectionName;
        },
        /** TODO
         * @returns 
         */
        getUnitOrTerrainDialogSelection: function() {
            return unitOrTerrainDialogSelectionCurrent;
        },

        // import
        uploadScenario: function() {
            ImportAndExport.uploadFileImport();
        },
        /** TODO */
        importScenario: function(importedScenarioSimple) {
            const nameScenario = importedScenarioSimple.name;

            Model.setScenarioName(nameScenario);
            Model.setScenario(importedScenarioSimple, true);
            
            Controler.reportImportedScenario();
        },

        // pictures as string
        /** TODO
         * @param {*} unitId 
         * @returns 
         */
        getUnitPictureString: function(unitId) {
            return unitPictures.get(unitId)
        }
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
    Model.resetModel();

    initUnitInlineSvgs();

    console.log('Model-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)
