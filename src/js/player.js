/** eng: player class. Contains all data for players.
 * https://creativecommons.org/licenses/by-sa/4.0/
 * by Rudolf Geist
 */
// Given: -
const Player = (function () { // encapsulation / Kapselung
  /** Private stored player template.
   * @type {{}} 
   */
  const playerTemplate = {id: 0, color: 'none', commmander: 'none',  funds: 0, units: 0};
  /** Private stored color names.
   * @type {[string]} 
   */
  const colorNames = ['Red', 'Blue', 'Green', 'Yellow', 'Black', 'White'];
  /** Private stored neutral color name.
   * @type {string}
   */
  const neutralColorName = 'Grey';

  // public API / oeffentliche API
  return {
    /** Get the default player.
     * @return {{}} default player
     */
    getPlayerTemplate() {
      const playerTemplateReferenceless = Object.assign({}, playerTemplate)

      return playerTemplateReferenceless;
    },
    /** Get the color names.
     * @returns {[string]} color names
     */
    getColors() {
      const colorsReferenceless = colorNames.slice(0);

      return colorsReferenceless;
    },
    /** Get the neutral color name.
     * @returns {string} neutral color name
     */
    getNeutralColor() {
      return neutralColorName;
    } 
  }
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
  console.log('Player-Module is ready. |' + Date(Date.now()).slice(4,24));
}, false)