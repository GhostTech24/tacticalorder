/**
 * eng: terrain class. Contains all data of terrains.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: -
const Terrain = (function () { // encapsulation / Kapselung
  const terrains = new Map(
    [
      ['city',      {id: 'city',      name: '', descripition: '', cover: 2, hidesAtFogOfWar: false,  income: 1000, costs: 0, conquerable: true,  buildableList: null,                            produces: null, repairsList: ['ground'],  specialFeature: ['fowSightByCapture'],  movementCosts: {infantry: 1, tireA: 1, tireB: 1, track: 1, air: 1, ship: 0, ferry: 0} }],
      ['plane',     {id: 'plane',     name: '', descripition: '', cover: 1, hidesAtFogOfWar: false,  income: 0,    costs: 0, conquerable: false, buildableList: ['mobileBase', 'mobileAirport'], produces: null, repairsList: null,        specialFeature: null,                   movementCosts: {infantry: 1, tireA: 2, tireB: 1, track: 1, air: 1, ship: 0, ferry: 0} }],
      ['street',    {id: 'street',    name: '', descripition: '', cover: 0, hidesAtFogOfWar: false,  income: 0,    costs: 0, conquerable: false, buildableList: null,                            produces: null, repairsList: null,        specialFeature: null,                   movementCosts: {infantry: 1, tireA: 1, tireB: 1, track: 1, air: 1, ship: 0, ferry: 0} }],
      ['woods',     {id: 'woods',     name: '', descripition: '', cover: 3, hidesAtFogOfWar: true,   income: 0,    costs: 0, conquerable: false, buildableList: null,                            produces: null, repairsList: null,        specialFeature: null,                   movementCosts: {infantry: 1, tireA: 3, tireB: 3, track: 2, air: 1, ship: 0, ferry: 0} }],
      ['sea',       {id: 'sea',       name: '', descripition: '', cover: 0, hidesAtFogOfWar: false,  income: 0,    costs: 0, conquerable: false, buildableList: null,                            produces: null, repairsList: null,        specialFeature: null,                   movementCosts: {infantry: 0, tireA: 0, tireB: 0, track: 0, air: 1, ship: 1, ferry: 1} }],
      ['mountain',  {id: 'mountain',  name: '', descripition: '', cover: 4, hidesAtFogOfWar: false,  income: 0,    costs: 0, conquerable: false, buildableList: null,                            produces: null, repairsList: null,        specialFeature: null,                   movementCosts: {infantry: 2, tireA: 0, tireB: 0, track: 0, air: 1, ship: 0, ferry: 0} }]
    ]
  );

  // public API / oeffentliche API
  return {
    /**
     * Get the specefic terrain.
     * @param {string} id the id of the terrain
     * @returns {object} terrain as object
     */
    getTerrain(id) {
      return terrains.get(id);
    },

    /**
     * Get all terrains.
     * @return {Map<String, Object>} All terrains as objects
     */
    getAllTerrains() { 
      return terrains;
    }
  }
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
  console.log('Terrain-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)