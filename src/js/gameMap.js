/**
 * eng: game map class. Contains all data of the current map.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: -
const GameMap = (function () { // encapsulation / Kapselung
  // private functions
  constructMap = function(xFieldsLength, yFieldsLength, copiedGameField) {  
    const xFields = [];
    for(let x = 0; x < xFieldsLength; x++) {
      const yFields = [];
      for(let y = 0; y < yFieldsLength; y++) {
        
        if(copiedGameField !== null) {
          if(x < copiedGameField.length) {
            if(y < copiedGameField[x].length) {
              const currentTerrain = copiedGameField[x][y];
              yFields.push(currentTerrain);
              continue;
            } 
          } 
        }

        yFields.push(Terrain.getTerrain('plane'));
      }

      xFields.push(yFields);
    }

    const resultTerrainGameMap = xFields;
    return resultTerrainGameMap;
  },

  constructUnitPlacement = function(xFieldsLength, yFieldsLength, unitPlacementTemplate) { 
    const xUnits = [];
    for(let x = 0; x < xFieldsLength; x++) {
      const yUnits = [];
      for(let y = 0; y < yFieldsLength; y++) {
        
        const currentUnit = searchCopyableUnit(x, y, unitPlacementTemplate);

        yUnits.push(currentUnit);
      }
      xUnits.push(yUnits);
    }

    const resultUnitsGameMap = xUnits;
    return resultUnitsGameMap;
  },

  searchCopyableUnit = function(unitXCoordinate, unitYCoordinate, unitPlacementTemplate) {
    // try to read value
    if(unitPlacementTemplate === null) { // only bevore first init needed
      return undefined;
    } 

    const unitPlacementLastXCoordinate = unitPlacementTemplate.length - 1;
    if(unitXCoordinate > unitPlacementLastXCoordinate) {
      return undefined;
    }
    const unitPlacementLastYCoordinate = unitPlacementTemplate[0].length - 1;
    if(unitYCoordinate > unitPlacementLastYCoordinate) {
      return undefined;
    }

    const unitBevore = unitPlacementTemplate[unitXCoordinate][unitYCoordinate];
    return unitBevore;
  }

  // public API / oeffentliche API
  return { 
    /**
     * Create the game maps and return them.
     * @param {number} xFieldsLength count of fields (x axis)
     * @param {number} yFieldsLength count of fields (y axis)
     * @param {[[{}]]?} gameMapTemplate terrain to use as template for the game map (optional)
     * @param {[[{}]]?} unitPlacementTemplate units to use as template  for the game map (optional)
     * @returns {[[[{}]]]} container of terrains an units for the game map
     */     
    createGameMaps(xFieldsLength = 20, yFieldsLength = 20, gameMapTemplate = null, unitPlacementTemplate = null) {
      const resultsArray = [];

      resultsArray.push(constructMap(xFieldsLength, yFieldsLength, gameMapTemplate)); // 2D array of terrain 
      resultsArray.push(constructUnitPlacement(xFieldsLength, yFieldsLength, unitPlacementTemplate)); // 2D array of units
 
      return resultsArray;
    }
  };
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
  console.log('GameMap-Modul ready. |' + Date(Date.now()).slice(4,24));
}, false)