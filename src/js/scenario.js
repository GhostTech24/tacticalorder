/**
 * eng: terrain class. Contains all data of terrains.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: gameMap.js, losingCondition.js
const Scenario = (function () { // encapsulation / Kapselung
  /** Private stored name from the scenario.
   * @type {string} the name of the scenario
   */
  let name;
  /** Private stored terrains from the scenario.
   * @type {[[Object]]} terrains of the game map
   */
  let terrains;
  /** Private stored units at the start from the scenario.
   * @type {[[{}]]} units of the game map at start of the scenario
   */
  let unitsStart;

  // TODO implement
  let currentDay;
  /** Private stored players at the start from the scenario.
   * @type {Map<number, {}>}
   */
  let playersStart;
  /** Private stored loosingConditions of players from the scenario.
   * @type {Map<number, [{}]>}
   */
  let losingConditions;
  const events = null; // [function(){when condition met do something and delete event, else return}, function(){like first description but different}, ...]

  updateLoseConditions = function(xFieldsLength, yFieldsLength) {

    // checks for 'unitReachedField'
    if(xFieldsLength !== null) {
      if(losingConditions) {
        let losingConditionsCurrent;
        for(losingConditionsCurrent of losingConditions.values()) {
          
          let losingConditionCurrent
          for(losingConditionCurrent of losingConditionsCurrent) {
            const xCoordinateCurrent = losingConditionCurrent.xCoordinate;
            if(xCoordinateCurrent) {

              const xCoordinateNew = xFieldsLength - 1;
              const isLosingReferencedField = (xCoordinateCurrent > xCoordinateNew)
              if(isLosingReferencedField) {
                console.log('DEBUG! Reference lost! Delete losing condition!')
                Scenario.deleteLosingCondition(losingConditionCurrent.affectedPlayerId, losingConditionCurrent.id)
              }
            }
          }
        }
      }
    }

    if(yFieldsLength !== null) {
      if(losingConditions) {
        let losingConditionsCurrent;
        for(losingConditionsCurrent of losingConditions.values()) {
          
          let losingConditionCurrent
          for(losingConditionCurrent of losingConditionsCurrent) {

            const yCoordinateCurrent = losingConditionCurrent.yCoordinate;
            if(yCoordinateCurrent) {
              const yCoordinateNew = yFieldsLength - 1;
            
              const isLosingReferencedField = (yCoordinateCurrent > yCoordinateNew)
              if(isLosingReferencedField) {
                console.log('DEBUG! Reference lost! Delete losing condition!')
                Scenario.deleteLosingCondition(losingConditionCurrent.affectedPlayerId, losingConditionCurrent.id)
              }
            }
          }
        }
      }
    }

  }

  /** Rebuild function. Rebuild the simplified losing conditions back to losing conditions with logic.
   *  @param {{}} scenarioWithLosingConditionsSimple the scenario that contain simplified losing conditions
   *  @param {boolean} boolean value if the data are in minimal (imported) form 
   *  @returns {Map<number, {}[]} structured losing conditions with logic
   */
  transformFromLosingConditionsSimple = function(scenarioWithLosingConditionsSimple, minimalData = false) {
    const losingConditionsNew = new Map();
      
    const losingConditionsSimplified = scenarioWithLosingConditionsSimple.losingConditions;

    const mapKeys = Array.from(losingConditionsSimplified.keys());
    const mapKeysLength = mapKeys.length;
    let i;
    for(i = 0; i < mapKeysLength; i++) {
      const indexLosingCondition = mapKeys[i];

      let losingConditionsPlayer;
      if(minimalData) { // Array
        const indexValues = 1;
        losingConditionsPlayer = losingConditionsSimplified[indexLosingCondition][indexValues];
      } else { // Map
        losingConditionsPlayer = losingConditionsSimplified.get(indexLosingCondition);
      }

      const playerId = i + 1;
      const losingConditionsWithLogic = [];

      for(const losingCondition of losingConditionsPlayer) {
  
        const losingConditionWithLogic = LosingCondition.getLosingCondition(losingCondition.id);

        losingConditionWithLogic.affectedPlayerId = playerId;
        losingConditionWithLogic.xCoordinate = losingCondition.xCoordinate;
        losingConditionWithLogic.yCoordinate = losingCondition.yCoordinate;

        losingConditionsWithLogic.push(losingConditionWithLogic);
      }    

      losingConditionsNew.set(playerId, losingConditionsWithLogic)
    }

    return losingConditionsNew;
  }

  transformFromTerrainsSimple = function(scenarioNew, minimalData = false) {
    if(minimalData) {
      const terrainsWithData = [];

      const gameMapSimple = scenarioNew.terrains;
      const gameMapRowLength = gameMapSimple.length;
      const gameMapColumnLength = gameMapSimple[0].length;

      let a;
      for(a = 0; a < gameMapRowLength; a++) {
        const terrainColumn = [];
        let b;
        for(b = 0; b < gameMapColumnLength; b++) {
          const terrainIdCurrent = gameMapSimple[a][b];
          const terrainCurrent = Terrain.getTerrain(terrainIdCurrent);
          terrainColumn.push(terrainCurrent);
        }

        terrainsWithData.push(terrainColumn);
      }

      return terrainsWithData;
    } else {
      return scenarioNew.terrains;
    }
  }

  transformFromUnitsStartSimple = function(scenarioNew, minimalData = false) {
    if(minimalData) {
      const unitsStartWithData = [];

      const unitsStartSimple = scenarioNew.unitsStart;
      const gameMapRowLength = unitsStartSimple.length;
      const gameMapColumnLength = unitsStartSimple[0].length;

      let a;
      for(a = 0; a < gameMapRowLength; a++) {
        const unitsStartColumn = [];
        let b;
        for(b = 0; b < gameMapColumnLength; b++) {   
          let unitCurrent;
          const unitSimpleCurrent = unitsStartSimple[a][b];
          const noUnitAtPosition = (unitSimpleCurrent.length === 0);

          if(noUnitAtPosition) {
            unitCurrent = undefined;
          } else {
            const indexUnitId = 0;
            const idUnitCurrent = unitSimpleCurrent[indexUnitId];

            unitCurrent = Unit.getUnit(idUnitCurrent);

            const indexWeapon1AmmoCurrent = 1;
            unitCurrent.weapon1.currentAmmo = unitSimpleCurrent[indexWeapon1AmmoCurrent];

            const indexWeapon2AmmoCurrent = 2;
            if(unitSimpleCurrent[indexWeapon2AmmoCurrent] !== null) {
              unitCurrent.weapon2.currentAmmo = unitSimpleCurrent[indexWeapon2AmmoCurrent];
            }

            const indexWeapon3AmmoCurrent = 3;
            if(unitSimpleCurrent[indexWeapon3AmmoCurrent] !== null) {
              unitCurrent.weapon3.currentAmmo = unitSimpleCurrent[indexWeapon3AmmoCurrent];
            }

            const indexHPCurrent = 4;
            unitCurrent.currentHP = unitSimpleCurrent[indexHPCurrent];

            const indexVisibilityCurrent = 5;
            unitCurrent.currentVisability = unitSimpleCurrent[indexVisibilityCurrent];

            const indexCostsCurrent = 6;
            unitCurrent.currentCosts = unitSimpleCurrent[indexCostsCurrent];

            const indexMovementCurrent = 7;
            unitCurrent.currentMovement = unitSimpleCurrent[indexMovementCurrent];
            
            const indexFuelCurrent = 8;
            unitCurrent.currentFuel = unitSimpleCurrent[indexFuelCurrent];

            const indexOwnerCurrent = 9;
            unitCurrent.ownerCurrent = unitSimpleCurrent[indexOwnerCurrent];
          }

          unitsStartColumn.push(unitCurrent);
        }

        unitsStartWithData.push(unitsStartColumn);
      }

      return unitsStartWithData;
    } else {
      return scenarioNew.unitsStart;
    }
  }

  transformFromPlayersStartSimple = function(scenarioNew, minimalData = false) {
    if(minimalData) {
      const playersStartCurrent = new Map();

      const playersStartSimple = scenarioNew.playersStart;
      const sizePlayers = playersStartSimple.length;

      let a = 0;
      for(a; a < sizePlayers; a++) {
        const indexIdPlayer = 0;
        const indexValuesPlayer = 1;

        const playerIdCurrent = playersStartSimple[a][indexIdPlayer];
        const playerCurrent = Object.assign({},  playersStartSimple[a][indexValuesPlayer]);
        playersStartCurrent.set(playerIdCurrent, playerCurrent);
      }


      return playersStartCurrent;
    } else {
      return scenarioNew.playersStart;
    }
  }

  // public API / oeffentliche API
  return {
    /** TODO
     * @param {boolean} minimalData boolean value, if only minimized data form is wanted (example: only terrain ids instead of terrain object.)
     * @returns {{}} TODO
     */
    getScenario: function(minimalData = false) {
      // init result object
      const scenarioCurrent = {
        name,
        terrains,
        unitsStart,
        playersStart,
        losingConditions
      }
       
      // transform losing conditions logicless
      const losingConditionsTransformed = new Map();
      
      const mapKeys = Array.from(losingConditions.keys());
      const mapKeysLength = mapKeys.length;

      let i;
      for(i = 0; i < mapKeysLength; i++) {
        const losingConditionsPlayer = losingConditions.get(mapKeys[i]);
        const playerId = i + 1;
        const losingConditionsSimplified = [];

        for(let losingCondition of losingConditionsPlayer) {
          
          const losingConditionLogicless = {
            id: losingCondition.id,
            affectedPlayerId: losingCondition.affectedPlayerId,
            xCoordinate: losingCondition.xCoordinate,
            yCoordinate: losingCondition.yCoordinate
          }

          losingConditionsSimplified.push(losingConditionLogicless);
        }
        
        losingConditionsTransformed.set(playerId, losingConditionsSimplified)
      }
      
      // other values
      let terrainsCurrent, unitsStartCurrent, playersStartCurrent, losingConditionsCurrent;
      if(minimalData) {
        const terrainsSimplified = [];
        const gameMapRowLength = terrains.length;
        const gameMapColumnLength = terrains[0].length;

        let a;
        for(a = 0; a < gameMapRowLength; a++) {
          const terrainColumn = [];
          let b;
          for(b = 0; b < gameMapColumnLength; b++) {
            const terrainIdCurrent = terrains[a][b].id;
            terrainColumn.push(terrainIdCurrent);
          }

          terrainsSimplified.push(terrainColumn);
        }

        terrainsCurrent = terrainsSimplified;

        const unitsStartSimplified = [];
        let c;
        for(c = 0; c < gameMapRowLength; c++) {
          const unitsStartColumn = [];
          let d;
          for(d = 0; d < gameMapColumnLength; d++) {
            let unitSimplified = [];
            const unitCurrent = unitsStart[c][d];

            if(unitCurrent !== undefined) {
              const unitIdCurrent = unitsStart[c][d].id;
              unitSimplified.push(unitIdCurrent);

              const unitWeapon1CurrentAmmoCurrent = unitsStart[c][d].weapon1.currentAmmo;
              unitSimplified.push(unitWeapon1CurrentAmmoCurrent);

              const unitWeapon2Current = unitsStart[c][d].weapon2;
              let unitWeapon2CurrentAmmoCurrent = null;
              if(unitWeapon2Current !== null) {
                unitWeapon2CurrentAmmoCurrent = unitsStart[c][d].weapon2.currentAmmo;
              }
              unitSimplified.push(unitWeapon2CurrentAmmoCurrent);

              const unitWeapon3Current = unitsStart[c][d].weapon3;
              let unitWeapon3CurrentAmmoCurrent = null;
              if(unitWeapon3Current !== null) {
                unitWeapon3CurrentAmmoCurrent = unitsStart[c][d].weapon3.currentAmmo;
              }
              unitSimplified.push(unitWeapon3CurrentAmmoCurrent);

              const unitHpCurrent = unitsStart[c][d].currentHP;
              unitSimplified.push(unitHpCurrent);

              const unitVisabilityCurrent = unitsStart[c][d].currentVisability;
              unitSimplified.push(unitVisabilityCurrent);

              
              const unitCoastsCurrent = unitsStart[c][d].currentCosts;
              unitSimplified.push(unitCoastsCurrent);

              const unitMovementCurrent = unitsStart[c][d].currentMovement;
              unitSimplified.push(unitMovementCurrent);

              const unitFuelCurrent = unitsStart[c][d].currentFuel;
              unitSimplified.push(unitFuelCurrent);

              
              const unitOwnerCurrent = unitsStart[c][d].ownerCurrent;
              unitSimplified.push(unitOwnerCurrent);
            }         
     
            unitsStartColumn.push(unitSimplified);
          }

          unitsStartSimplified.push(unitsStartColumn);
        }

        unitsStartCurrent = unitsStartSimplified;
        
        playersStartCurrent = Array.from(playersStart);
        losingConditionsCurrent = Array.from(losingConditionsTransformed);

      } else {
        terrainsCurrent = terrains;
        unitsStartCurrent = unitsStart;
        playersStartCurrent = playersStart;
        losingConditionsCurrent = losingConditionsTransformed;
      }

      // fill result object 
      scenarioCurrent.name = name;
      scenarioCurrent.terrains = terrainsCurrent;
      scenarioCurrent.unitsStart = unitsStartCurrent;
      scenarioCurrent.playersStart = playersStartCurrent;
      scenarioCurrent.losingConditions = losingConditionsCurrent;

      return scenarioCurrent;
    },
    /** TODO
     * @param {{}} scenarioNew  TODO
     */
    setScenario: function(scenarioNew, minimalData = undefined) {  
      name = scenarioNew.name;
      terrains = transformFromTerrainsSimple(scenarioNew, minimalData);
      unitsStart = transformFromUnitsStartSimple(scenarioNew, minimalData);
      playersStart = transformFromPlayersStartSimple(scenarioNew, minimalData);
      losingConditions = transformFromLosingConditionsSimple(scenarioNew, minimalData);
    },
    /** TODO
     */
    resetScenario: function() {
      this.setName();
      this.resetPlayersStart();
      this.setGameMaps();
    },

    // name
    /** Get the scenario name.
     * @returns {string} the scenario name
     */
    getName: function() {
      return name;
    },
    /** Set the scenario name.
     * @param {string} nameNew the new scenario name
     */
    setName: function(nameNew = 'newScenario') {
      name = nameNew;
    },

    // player
    /** TODO
     * @returns {Map<string, {}>} player object
     */
    getPlayersStart() {
      return playersStart;
    },
    /** TODO
     * @returns {number} size of players at start
     */
    getPlayersStartSize() {
      return playersStart.size;
    },
    /** Resets the players at the start of an scenario. Resets the connected losing conditions too. */
    resetPlayersStart() {
      playersStart = new Map();
      losingConditions = new Map();
    },
    /** TODO
     * @param {number} id id of the player
     * @returns {{}?} player object (or null, when id was not positive value)
     */
    getPlayerStart(id) {
      if(id <= 0) {
        return null;
      }
      const playerStartReferenceless = Object.assign({}, playersStart.get(id));

      return playerStartReferenceless;
    },
    /** TODO
     * @param {number} id the id of the player
     * @param {Object} playerNew the player object
     */
    setPlayerStart(id, playerNew) {
      // set player
      playersStart.set(id, playerNew);
      
      // check and set default losing condition
      const losingConditionsBevore = losingConditions.get(id);

      if(!losingConditionsBevore) {
        const losingConditionDefault = LosingCondition.getLosingCondition('allUnitsLost');
        losingConditionDefault.affectedPlayerId = id;

        const losingConditionsCurrent = [losingConditionDefault];
        losingConditions.set(id,losingConditionsCurrent);
      }
    },
    /** TODO
     * @param {number} id id of the player to delete
     */
    deletePlayerStart(id) {
      const xFieldsSize = unitsStart.length;
      const yFieldsSize = unitsStart[0].length;
      let x, y, unitCurrent, unitCurrentOwnerCurrent, terrainCurrent, terrainCurrentOwnerCurrent;

      // delete from players
      playersStart.delete(id);
      
      // deleteLosingConditions
      losingConditions.delete(id);

      // delete units from player
      for(x = 0; x < xFieldsSize; x++) {
        for(y = 0; y < yFieldsSize; y++) {
          unitCurrent = unitsStart[x][y];

          if(unitCurrent === undefined) {
            continue;
          }

          unitCurrentOwnerCurrent = unitCurrent.ownerCurrent;

          if(unitCurrentOwnerCurrent === id) {
            unitsStart[x][y] = undefined;
          }
        }
      }

      // delete controled properties from player
      console.error('TODO: delete controled properties from player')
    },

    // losing condition
    /** Get the losing conditions from an player.
     * @param {number} playerId the id of an player
     * @returns {[{}]} the  losing conditions from an player
     */
    getLosingConditions(playerId) {
      return losingConditions.get(playerId)
    },
    /** Add a losing condition for an player. Existing losing conditions get overwritten.
     * @param {number} playerId the id of an player
     * @param {string} losingConditionId the id of the losing condition. Default losing condition: 'allUnitsLost'
     * @param {number?} xCoordinate the relevant x coordinate for the losing condition or null
     * @param {number?} yCoordinate the relevant y coordinate for the losing condition or null
     */
    addLosingCondition(playerId, losingConditionId='allUnitsLost', xCoordinate = null, yCoordinate = null) {
      // build losing condition
      const losingCondition = LosingCondition.getLosingCondition(losingConditionId);

      if(!losingCondition) {
        return false;
      }

      losingCondition.affectedPlayerId = playerId;
      losingCondition.xCoordinate = xCoordinate;
      losingCondition.yCoordinate = yCoordinate;

      const isUnitReachedField = (losingConditionId === 'unitReachedField');
      if(isUnitReachedField) {
        const xEqualNull = (losingCondition.xCoordinate === null)
        if(xEqualNull) {
          losingCondition.xCoordinate = 0;
        }

        const yEqualNull = (losingCondition.yCoordinate === null)
        if(yEqualNull) {
          losingCondition.yCoordinate = 0;
        }
      }

      // add losing condition
      const losingConditionsPlayer = losingConditions.get(playerId);

      let losingConditionPlayer;
      for(losingConditionPlayer of losingConditionsPlayer) {
        const isExisting = (losingConditionPlayer.id === losingConditionId);
        if(isExisting) {
          const index = losingConditionsPlayer.indexOf(losingConditionPlayer);
          losingConditionsPlayer.splice(index, 1);
          break;
        }
      }
      
      losingConditionsPlayer.push(losingCondition);
      return true;
    },
    /** Delete a losing condition for an player. Add the default losing condition when no one is left.
     * @param {number} playerId the id of an player
     * @param {string} losingConditionId the id of the losing condition to delete
     */
    deleteLosingCondition(playerId, losingConditionId) {
      const losingConditionsCurrent = losingConditions.get(playerId);
      const losingConditionsCurrentLengthBevore = losingConditionsCurrent.length;
      
      let i
      for(i = 0; i < losingConditionsCurrentLengthBevore; i++) {
        const losingConditionCurrent = losingConditionsCurrent[i];
        const losingConditionIdCurrent = losingConditionCurrent.id;
        const isEqualValue = (losingConditionId === losingConditionIdCurrent)
        
        if(isEqualValue) {
          losingConditionsCurrent.splice(i, 1);

          const losingConditionsCurrentLengthAfter = losingConditionsCurrent.length;
          const hasNoLosingConditions = (losingConditionsCurrentLengthAfter === 0);
          if(hasNoLosingConditions) {
            Scenario.addLosingCondition(playerId);
          }

          return true;
        }
      }

      return false;
    },

    // game Maps
    /** Set the game maps. Handle affected losing conditions too.
     * @param {number} xFieldsLength count of fields (x axis)
     * @param {number} yFieldsLength count of fields (y axis)
     * @param {[[{}]]} gameMapTemplate 2D array of terrain to use as template
     * @param {[[{}]]} unitPlacementTemplate 2D array of units to use as template
     */     
    setGameMaps: function(xFieldsLength, yFieldsLength, gameMapTemplate = undefined, unitPlacementTemplate = undefined) {
      updateLoseConditions(xFieldsLength, yFieldsLength);

      // create and set game maps
      const resultMaps = GameMap.createGameMaps(xFieldsLength, yFieldsLength, gameMapTemplate, unitPlacementTemplate);
      
      terrains = resultMaps[0];
      unitsStart = resultMaps[1];
    },

    // terrains
    /** Get the terrains of the game map.
     * @returns {[[{}]]} terrains of the game map
     */
    getTerrains: function() {
      return terrains;
    },
    /** Get a specific terrain on the game map.
     * @param {number} xCoordinate the x coordinate on the game map
     * @param {number} yCoordinate the y coordinate on the game map
     * @returns {{}} the found terrain
     */
    getTerrain(xCoordinate, yCoordinate) {
      return terrains[xCoordinate][yCoordinate];
    },
    /** Set a specific terrain on the game map.
     * @param {number} xCoordinate the x coordinate on the game map
     * @param {number} yCoordinate the y coordinate on the game map
     * @param {{}} terrainNew the terrain to set
     */
    setTerrain(xCoordinate, yCoordinate, terrainNew) {
      terrains[xCoordinate].splice(yCoordinate, 1, terrainNew);
    },

    // units
    /** Get the units of the game map at the start of the scenario.
     * @returns {[[{}]]} units of the game map at the start of the scenario.
     */
    getUnitsStart: function() {
      return unitsStart;
    },
    /** Get a specific unit on the game map at the start of the scenario.
     * @param {number} xCoordinate the x coordinate on the game map
     * @param {number} yCoordinate the y coordinate on the game map
     * @returns {{}?} the found unit (or null when there was no unit at the given coordinates)
     */
    getUnitStart: function(xCoordinate, yCoordinate){        
      return unitsStart[xCoordinate][yCoordinate];
    },
    /** Set or delete a specific unit on the game map at the start of the scenario.
     * @param {number} xCoordinate the x coordinate on the game map
     * @param {number} yCoordinate the y coordinate on the game map
     * @param {{}?} unitNew the unit to set (or undefined when there should be no unit at the given coordinates)
     */
    setUnitStart: function(xCoordinate, yCoordinate, unitNew){             
      const unitBevore = unitsStart[xCoordinate][yCoordinate];
      
      // place unit
      unitsStart[xCoordinate][yCoordinate] = unitNew;

      // update unit counters
      if(unitBevore) {
        const ownerId = unitBevore.ownerCurrent;
        const ownerUpdated = Scenario.getPlayerStart(ownerId);
        const ownerUpdatedUnits = ownerUpdated.units;
        ownerUpdated.units = ownerUpdatedUnits - 1;

        Scenario.setPlayerStart(ownerId, ownerUpdated)
      }

      if(unitNew) {
        const ownerId = unitNew.ownerCurrent;
        const ownerUpdated = Scenario.getPlayerStart(ownerId);
        const ownerUpdatedUnits = ownerUpdated.units;
        ownerUpdated.units = ownerUpdatedUnits + 1;

        Scenario.setPlayerStart(ownerId, ownerUpdated)
      }
    }
  }
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
  Scenario.resetScenario();
  console.log('Scenario-Modul is ready. |' + Date(Date.now()).slice(4,24));
}, false)
  