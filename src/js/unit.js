/**
 * eng: unit class. Contains all data for Units.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: -
const Unit = (function () { // encapsulation / Kapselung
  const units = new Map(
    [
      ['infantry',       {id: 'infantry',      unitType: 'ground', name: '', descripition: '', armorType: 'lightBodyArmor',    visability: 2, costs: 1500, activeAbilitys: ['Conquer'], passiveAbilitys: ['MountainSight'], movement: 3, movementType: 'infantry',  fuel: 60, weapon1: {id: 'assaultRifle',     name: '', descripition: '', ammo: 9, rangeMin: 1, rangeMax: 1, damageList: [55, 50, 45,  8,  4,  2, 20,  8,  4,  0,  0,  0,  0,  0], currentAmmo: 9}, weapon2: null,                                                                                                                                                                  weapon3: null, currentHP: 100, currentVisability: 2, currentCosts: 1500, currentMovement: 3, currentFuel: 60, ownerCurrent: 1}  ],
      ['heavyInfantry',  {id: 'heavyInfantry', unitType: 'ground', name: '', descripition: '', armorType: 'heavyBodyArmor',    visability: 2, costs: 2500, activeAbilitys: ['Conquer'], passiveAbilitys: ['MountainSight'], movement: 2, movementType: 'infantry',  fuel: 40, weapon1: {id: 'bazooka',          name: '', descripition: '', ammo: 3, rangeMin: 1, rangeMax: 1, damageList: [65, 65, 65, 55, 25, 15,  0,  0,  0,  0,  0, 20, 15, 10], currentAmmo: 3}, weapon2: {id: 'lightMG',  name: '', descripition: '', ammo: 6, rangeMin: 1, rangeMax: 1, damageList: [65, 55, 50, 12,  6,  3, 30, 12,  4,  0,  0,  0,  0,  0], currentAmmo: 6}, weapon3: null, currentHP: 100, currentVisability: 2, currentCosts: 2500, currentMovement: 2, currentFuel: 40, ownerCurrent: 1}  ],
      ['lightTank',      {id: 'lightTank',     unitType: 'ground', name: '', descripition: '', armorType: 'lightArmorPlates',  visability: 1, costs: 7000, activeAbilitys: [],          passiveAbilitys: [],                movement: 6, movementType: 'track',     fuel: 60, weapon1: {id: 'lightArmorCannon', name: '', descripition: '', ammo: 6, rangeMin: 1, rangeMax: 1, damageList: [65, 65, 65, 55, 25, 15,  0,  0,  0,  0,  0, 20, 15, 10], currentAmmo: 6}, weapon2: {id: 'lightMG',  name: '', descripition: '', ammo: 9, rangeMin: 1, rangeMax: 1, damageList: [65, 55, 50, 12,  6,  3, 30, 12,  4,  0,  0,  0,  0,  0], currentAmmo: 9}, weapon3: null, currentHP: 100, currentVisability: 1, currentCosts: 7000, currentMovement: 6, currentFuel: 60, ownerCurrent: 1}  ]
    ]
  );

  /**
   * Get all units with referenceless Objects.
   * The Map prototype return it values referenced when they are objects.
   * A call from model and setting the ownerCurrent changed in the map too! 
   */
  getUnitsReferenceless =  function() {
    const unitsReferenceless = new Map();

    for (let [key, value] of units) {
      unitsReferenceless.set(key, Object.assign({}, value))
    }

    return unitsReferenceless;
  }

  // public API / oeffentliche API
  return {
    /** Get a specific unit.
     * @param {String} id the id of the wanted unit
     * @return {{}} the unit object or undefined (when not found)
     */
    getUnit(id) {
      const unitsReferenceless = getUnitsReferenceless();
      const unit = unitsReferenceless.get(id);

      return unit;
    },

    /** Get all units.
     * @return {Map<String, {}>} all unit objects
     */
    getAllUnits() {
      const unitsReferenceless = getUnitsReferenceless();

      return unitsReferenceless;
    }
    
  }
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
  console.log('Unit-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)