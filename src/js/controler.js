/** eng: controler class. Contains all data for the controler.
 * https://creativecommons.org/licenses/by-sa/4.0/
 */
// Given: -
const Controler = (function () { // encapsulation / Kapselung

    // public API / oeffentliche API
    return {
        // scenarios
        requestLoadScenarios: async function(){
            const isSuccess = await Model.loadScenarios();

            if(!isSuccess) {
                console.error('Error while loading scenarios async! Loading was success: |' + isSuccess + '|')
            }
        },
        /** TODO
         * @returns {string[]} TODO
         */
        requestScenarioNames: function(){
            return Model.getScenarioNames();
        },
        /** TODO javascript doc.
         */
        reportImportScenario: function() {
            Model.uploadScenario();
        },
        /** TODO javascript doc.
         */
         reportImportedScenario: function() {
            View.reportUpdateGameMap(true);
        },
        /** TODO javascript doc.
         */
        reportExportScenario: function() {
            console.log('DEBUG GER! exchange direct call to ImportAndExport to Model');
            ImportAndExport.exportData(Scenario.getScenario(true), Scenario.getName()); // TODO exchange for call to Model
        },

        // scenario name
        /** Request the scenario name.
         * @returns {string} the scenario name
         */
        requestScenarioName: function(){
            return Model.getScenarioName();
        },
        /** Report the scenario name.
         * @param {string} name the new scenario name
         */
        reportScenarioName: function(name) {
            Model.setScenarioName(name);
        },
        /** Report the scenario to load.
         * @param {string} scenarioName the scenario name
         * @returns {boolean} result of the operation
         */
        reportScenarioToLoad: function(scenarioName) {
            const isSuccess = Model.loadScenario(scenarioName);

            if(!isSuccess) {
                console.error('Error while loading Scenario! Scenario name was: |' + scenarioName + '|');
            }

            return isSuccess;
        },
        /** Async! TODO */
        reportScenarioToSave: async function() {
            await Model.saveScenario();
        },
        /** TODO */
        reportScenarioToReset: function() {
            Model.resetScenario();
        },
        /** TODO
         * @param {string} scenarioName 
         * @returns {boolean}
         */
        reportScenarioToDelete: async function(scenarioName) {
            const isSuccess =  await Model.deleteScenario(scenarioName);

            if(!isSuccess) {
                console.error('Error while deleting Scenario! Scenario name was: |' + scenarioName + '|');
            }

            return isSuccess;
        },
        
        // game map    
        /** Request the current terrains and return them.
         * @returns {[[{}]]} current terrains
         */
        requestTerrains: function(){
            return Model.getTerrains();
        },
        /** Report changes at the current game map.
         * @param {number} xFieldSize size of x fields
         * @param {number} yFieldSize size of y fields
         * @param {[[{}]]} terrains terrains to use as a template
         * @param {[[{}]]} unitsPlacement units placement to use as a template
         */
        reportGameMap: function(xFieldSize, yFieldSize, terrains, unitsPlacement){
            Model.setGameMaps(xFieldSize, yFieldSize, terrains, unitsPlacement);
        },
        /** Request the units of the game map at the start of the scenario.
         * @returns {[[{}]]} units of the game map at the start of the scenario.
         */
        requestUnitsStart: function(){
            return Model.getUnitsStart();
        },
        
        /**
         * request all terrain ids and return them.
         * @returns {[string]} all terrain ids
         */
        requestTerrainIds: function(){
            return Model.getTerrainIds();  
        },
        /**
         * request all unit ids and return them.
         * @returns {[string]} all unit ids
         */
        requestUnitIds: function(){
            return Model.getUnitIds();
        },

        /**
         * Report needs to set a specific terrain on the game map. (inclusive error handling)
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @param {string} terrainId the id of the terrain to set
         */
        reportTerrain: function(xCoordinate, yCoordinate, terrainId){
            const isSuccessful = Model.setTerrain(xCoordinate, yCoordinate, terrainId);

            if(!isSuccessful) {
                console.error('Error while setting terrain! Terrain id is unknown and was: |' + terrainId + '|');
            }
        },

        /**
         * Report needs to get a specific unit id on the game map at the start of the scenario.
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @returns {string?} the found unit id (or null when there was no unit at the given coordinates)
         */
        requestUnitIdStart: function(xCoordinate, yCoordinate) {
            return Model.getUnitIdStart(xCoordinate, yCoordinate);
        },
        /**
         * Report needs to set a specific unit on the game map. (inclusive error handling)
         * @param {number} xCoordinate the x coordinate on the game map
         * @param {number} yCoordinate the y coordinate on the game map
         * @param {string?} unitId the id of the unit to set (or null when there should be no unit at the given coordinates)
         */
        reportUnitStart: function(xCoordinate, yCoordinate, unitId, playerId){
            const isSuccessful = Model.setUnitStart(xCoordinate, yCoordinate, unitId, playerId);

            if(!isSuccessful) {
                console.error('Error while setting unit at start of scenario! Unit id is unknown and was: |' + unitId + '|');
            }  
        },
        /**
         * Report needs to get the boolean value if an unit can move on the given terrain
         * @param {string} unitId id of the unit
         * @param {string} terrainId id of the terrain
         * @returns {boolean} result of this operation
         */
        requestIsMovableOnTerrain: function(unitId, terrainId){
            return Model.isMovableOnTerrain(unitId, terrainId);
        },

        // player
        /**
         * 
         * @returns TODO
         */
        reportAddPlayerStart: function() {
            const isAdded = Model.addPlayerStart();

            if(!isAdded) {
                console.log('Cant add another Player!') // TODO change vor refuse sound
            }

            return isAdded;
        },
        /**
         * 
         * @returns TODO
         */
        reportDeletePlayerStart: function() {
            const isDeleted = Model.deletePlayerStart();

            if(!isDeleted) {
                console.log('Cant delete another Player!') // TODO change vor refuse sound
            }

            return isDeleted;
        },
        /**
         * TODO
         * @param {*} playerId 
         * @param {*} changeDirectionValue 
         */
        reportChangePlayerStartColor: function(playerId, changeDirectionValue) {
            const isChanged = Model.changePlayerStartColor(playerId, changeDirectionValue);

            if(isChanged) {
                View.reportUpdateGameMap();
                View.updatePlayerColorValueElementOfSetPlayerDialog(playerId);
            }  
        },

        /**
         * TODO
         * @param {number} xCoordinate 
         * @param {number} yCoordinate 
         * @returns 
         */
        requestPlayerColorClassNameOfUnit: function(xCoordinate, yCoordinate){
            return Model.getPlayerColorClassNameOfUnit(xCoordinate, yCoordinate);
        },
        /**
         * TODO
         * @param {*} playerId 
         * @returns 
         */
        requestPlayerColorClassNameOfPlayer: function(playerId){
            return Model.getPlayerColorClassNameOfPlayer(playerId);
        },
        /** Get the current player id selection.
         * @returns {number} player id selection
         */
        requestPlayerIdSelection: function() {
            return Model.getPlayerIdSelection();
        },
        /**
         * TODO
         * @returns
         */
        requestPlayersStartSize: function() {
            return Model.getPlayersStartSize();
        },

        /** Change the current player id selection in a given direction.
         * Direction can be -1 (preview value) or 1 (next value).
         * Reaching an end of the possible selections will set it to the value of the other end.
         * (Example with 2 players and current selection at 0 (neutral): change in direction back will set selection to 2)
         * @param {number} changeDirectionValue the direction for the change
         */
        reportPlayerIdChange: function(changeDirectionValue) {
            Model.changePlayerIdSelection(changeDirectionValue);
        },
        /**
         * TODO
         * @param {*} unitId 
         * @returns 
         */
        requestUnitPictureString: function(unitId) {
            return Model.getUnitPictureString(unitId);
        },

        // losing condition
        /** Requests the losing conditions from an player.
         * @param {number} playerId the id of an player
         * @returns {[{}]} the  losing conditions from an player
         */
        requestLosingConditions:function(playerId) {
            return Model.getLosingConditions(playerId);
        },
        /** Report the change (toogle) of a given losing condition for the currently selected player.
         * @param {string} losingConditionId the id of the losing condition
         * @param {number?} xCoordinate the relevant x coordinate for the losing condition
         * @param {number?} yCoordinate the relevant y coordinate for the losing condition
         * @param {boolean} isUpdateWanted boolean value, if an existing losing condition should be updated (instead of beeing deleted)
         */
        reportLosingConditionStatusChange: function(losingConditionId, xCoordinate, yCoordinate, isUpdateWanted) {
            const isChanged = Model.changeLosingCondition(losingConditionId, xCoordinate, yCoordinate, isUpdateWanted);
            
            if(!isChanged) {
                console.error('Error while changing losing condition! Losing condition Id was: |' + losingConditionId + '|');
            }
        },

        // selection
        /**
         * TODO
         * @param {*} elementId 
         */
        reportSetableElement: function(elementId){
            Model.setSetableElement(elementId);
        },
        /**
         * TODO
         * @returns 
         */
        requestSetableElement: function(){
            return Model.getSetableElement();
        },

        /**
         * TODO
         * @param {*} selection 
         */
        reportUnitOrTerrainDialogSelection: function(selection){
            Model.setUnitOrTerrainDialogSelection(selection);
        },
        /**
         * TODO
         * @returns 
         */
        requestUnitOrTerrainDialogSelection: function(){
            return Model.getUnitOrTerrainDialogSelection();
        },

        // error handling
        /** TODO
         * @param {string} errorMessage 
         */
        reportError: function(errorMessage) {
            console.error(errorMessage);
        }
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function(){
    console.log('Controler-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)