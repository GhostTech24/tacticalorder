/** eng: import and export class. Contains all data to import and export in the application.
 * https://creativecommons.org/licenses/by-sa/4.0/
 * 
 * by Rudolf Geist
 */
// Given: -
const ImportAndExport = (function () { // encapsulation / Kapselung


  // public API / oeffentliche API
  return {
    /** Upload function. Initialize the browser dialog to upload an file to import. */
    uploadFileImport: function () {
      const temporaryUploadElement = document.createElement('input');
      temporaryUploadElement.setAttribute('type', 'file');
      temporaryUploadElement.setAttribute('accept', '.json');

      document.body.appendChild(temporaryUploadElement);
      temporaryUploadElement.click();

      temporaryUploadElement.onchange = function () {
        const fileUploaded = temporaryUploadElement.files[0];

        ImportAndExport.importScenario(fileUploaded);
      }

      document.body.removeChild(temporaryUploadElement);
    },
    /** Import the scenario from an specific file.
     * 
     * @param {{}} fileUploaded uploaded file containing the scenario
     */
     importScenario: function (fileUploaded) {
      const filereader = new FileReader();
        filereader.onload = function () {
          
          const contentString = this.result;
          const importedScenarioSimple = JSON.parse(contentString);
         
          Model.importScenario(importedScenarioSimple);
        };

        filereader.readAsText(fileUploaded);
    },
    /** TODO javascript doc
     * @param {object} data
     * @param {string} filename the name for the file. (example: "theMap" for result "theMap.json")
     */
    exportData: function (data, filename) {
      const dataAsJsonString = JSON.stringify(data);
      const temporaryDownloadElement = document.createElement('a');

      temporaryDownloadElement.href = URL.createObjectURL(new Blob([dataAsJsonString], {
        type: 'application/json' // now: -> filename.json bevore: text/plain -> filename.txt
      }));

      temporaryDownloadElement.setAttribute('download', filename + '.json');
      document.body.appendChild(temporaryDownloadElement);
      temporaryDownloadElement.click();
      URL.revokeObjectURL(temporaryDownloadElement.href)
      document.body.removeChild(temporaryDownloadElement);
    }
  }
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function () {
  console.log('ImportAndExport-Module is ready. |' + Date(Date.now()).slice(4, 24));
}, false)