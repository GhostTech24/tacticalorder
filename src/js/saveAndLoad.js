/**
 * eng: save and load class. Contains all data to save and load in the application.
 * https://creativecommons.org/licenses/by-sa/4.0/
 * by Rudolf Geist
 */
// Given: -
const SaveAndLoad = (function () { // encapsulation / Kapselung
    /** local used indexed db to store values permanent.
     * @type {IDBObjectStore}
     */
    let indexedDbUsed;
    const dbName = 'tacticalOrderDb';
    const scenarioStoreKey = "scenarios";
    const objectStoreName = 'storedValues';

    /** eng: Validator of the PoC. Normally this should be a single Class or javascript-file, but for this PoC its enough.
     * ger: Validator des PoC. Normalerweise sollte dies eine eigene Klasse oder Javascript-Datei sein, aber fuer diesen PoC ist es genug. 
     */
    var validator = {
        /**
         * eng: Validator function. Returns the boolean value, if the browser support the indexDB technology currently.
         * ger: Validierungsfunktion. Gibt den boolschen Wert zurueck, ob der Browser aktuell die IndexDB-Technologie unterstuetzt.
         */
        isIndexDBSupported: function() {
            if (window.indexedDB) {
                return true;
            } else {
                console.error('Der Browser unterstuetzt aktuell nicht die Technologie IndexDB!');
                return false;
            }
        }
    };

    getDefaultMap = function() {
        const scenariosDefault = new Map();
        return scenariosDefault;
    }

    openIndexedDb = async function() {
         if(validator.isIndexDBSupported)  {
            indexedDbUsed = await new Promise((resolve, reject) => {
                let openDbRequest
                try { 
                    const dbVersion = '1';

                    openDbRequest = window.indexedDB.open(dbName, dbVersion); // async function.

                    openDbRequest.onerror = function() { // possible async result #1: error
                        const knownError = 'InvalidStateError'
                        const errorMessagePart = openDbRequest.error.toString().slice(0,17);     
                        const errorMessagePrivateOrNoChronic = 'Die IndexedDB wurde nicht geöffnet! Das kann mehrere Gründe haben, z.B: Privater Modus im Browser, keine Chronik im Browser.'
                    
                        let shownMessage
                        if(knownError === errorMessagePart) {
                            shownMessage = openDbRequest.error + '\n' + errorMessagePrivateOrNoChronic;
                        } else {
                            shownMessage = openDbRequest.error;
                        }

                        return reject(shownMessage)
                    };

                    openDbRequest.onsuccess = function() { // possible async result #2: successful (every following after the first success)
                        return resolve(openDbRequest.result)
                    };

                    openDbRequest.onupgradeneeded = function() { // possible async result #3: successful (first success only ) To trigger this event again, delete the offline-data!
                        const db = openDbRequest.result;
                        const objectStore = db.createObjectStore(objectStoreName); // no path, no key gen

                        console.log('Store angelegt. Instance: ' + objectStore);

                        objectStore.transaction.oncomplete = function() {
                            const defaultValueMap = getDefaultMap();

                            let myObjectStore = db.transaction([objectStoreName], "readwrite").objectStore(objectStoreName);
                            
                            myObjectStore.add(defaultValueMap, scenarioStoreKey);      
                            
                            resolve(openDbRequest.result)      
                        }
                    }
                } catch (errorMessage) {
                    const knownError = 'SecurityError'
                    const errorMessagePart = errorMessage.toString().slice(0,13);            
                    const errorMessageCookies = 'In the current firefox browser all cookies are blocked! DB can\'t be opened!'
                    
                    let shownMessage
                    if(knownError === errorMessagePart) {
                        shownMessage = errorMessage + '\n' + errorMessageCookies;
                    } else {
                        shownMessage = errorMessage;
                    }

                    reject(shownMessage);                   
                }
            }); 
        } else {
            return false;
        }

    };

    // openDb = function() {
    //     const DB_NAME = 'MyDb';
    //     const DB_VERSION = '1';

    //     console.log('DEBUG! try to open indexed DB!')
    //     if(validator.isIndexDBSupported)  {
    //         return new Promise((resolve, reject) => {
    //             let openDbRequest
    //             try {
                    
    //                 openDbRequest = window.indexedDB.open(DB_NAME, DB_VERSION); // async function.
    //                 openDbRequest.onerror = function() { // error
    //                     const errorMessagePart = openDbRequest.error.toString().slice(0,17);
    //                     const knownError = 'InvalidStateError'
    //                     const errorMessagePrivateOrNoChronic = 'Die IndexedDB wurde nicht geöffnet! Das kann mehrere Gründe haben, z.B: Privater Modus im Browser, keine Chronik im Browser.'
                    
    //                     let shownMessage

    //                     console.error('Ergebnis war: |' + errorMessagePart)

    //                     if(knownError === errorMessagePart) {
    //                         shownMessage = openDbRequest.error + '\n' + errorMessagePrivateOrNoChronic;
    //                     } else {
    //                         shownMessage = openDbRequest.error;
    //                     }

    //                     return reject(shownMessage)
    //                 };

    //                 openDbRequest.onsuccess = function() { // every successful (except first)
    //                     return resolve(openDbRequest.result)
    //                 };

    //                 openDbRequest.onupgradeneeded = function() { // only first successfull; To trigger this event again, delete the offline-data!
    //                     const db = openDbRequest.result;
    //                     const objectStore = db.createObjectStore(objectStoreName); // no path, no key gen

    //                     console.log('Store angelegt. Instance: ' + objectStore);

    //                     objectStore.transaction.oncomplete = function() {
    //                         const defaultValueMap = getDefaultMap();

    //                         var myObjectStore = db.transaction([objectStoreName], "readwrite").objectStore(objectStoreName);
                            
    //                         myObjectStore.add(defaultValueMap, scenarioStoreKey);      
                            
    //                         console.log('Store erfolgreich zum ersten mal eingerichtet! (mit Werten)')
    //                         resolve(openDbRequest.result)      
    //                     }
    //                 }
    //             } catch (errorMessage) {
    //                 const errorMessagePart = errorMessage.toString().slice(0,13);
    //                 const knownError = 'SecurityError'
    //                 const errorMessageCookies = 'Firefox wird ohne Cookies ausgeführt (Cookies von Drittseiten blockiert)! DB kann nicht geöffnet werden! siehe dazu: https://github.com/Modernizr/Modernizr/issues/1825 (Stand 2020.09.25)'
    //                 let shownMessage

    //                 if(knownError === errorMessagePart) {
    //                     shownMessage = errorMessage + '\n' + errorMessageCookies;
    //                 } else {
    //                     shownMessage = errorMessage;
    //                 }

    //                 reject(shownMessage);                   
    //             }
    //         }); 
    //     }
    // };

    saveInIndexedDb = function(storekey, value){
        return new Promise((resolve, reject) => {
            const indexedDb = indexedDbUsed;
            const readTransaction = indexedDb.transaction([objectStoreName], "readwrite") // Access to DB
            const targetObjectStore = readTransaction.objectStore(objectStoreName) // Access to ObjectStore
            const resultOfRead = targetObjectStore.get(storekey)

            resultOfRead.onerror = function() {
                reject(resultOfRead.errorCode)
            };

            resultOfRead.onsuccess = function() { 
                var itemWithWriteAccess = resultOfRead.result

                itemWithWriteAccess = value; // here happens the update on the item

                var resultOfWrite = targetObjectStore.put(itemWithWriteAccess, storekey) // save changed item on the store
                resultOfWrite.onerror = function(event) {
                    reject(resultOfRead.errorCode)
                };
                resultOfWrite.onsuccess = function(event) {
                    resolve(resultOfRead.result)
                };  
            };
        });
    };

    // saveData = function(appDb, storekey, value){
    //     return new Promise((resolve, reject) => {

    //         const readTransaction = appDb.transaction([objectStoreName], "readwrite") // Access to DB
    //         const targetObjectStore = readTransaction.objectStore(objectStoreName) // Access to ObjectStore
    //         const resultOfRead = targetObjectStore.get(storekey)

    //         resultOfRead.onerror = function() {
    //             reject(resultOfRead.errorCode)
    //         };

    //         resultOfRead.onsuccess = function() { 
    //             var itemWithWriteAccess = resultOfRead.result

    //             itemWithWriteAccess = value; // here happens the update on the item

    //             var resultOfWrite = targetObjectStore.put(itemWithWriteAccess, storekey) // save changed item on the store
    //             resultOfWrite.onerror = function(event) {
    //                 reject(resultOfRead.errorCode)
    //             };
    //             resultOfWrite.onsuccess = function(event) {
    //                 resolve(resultOfRead.result)
    //             };  
    //         };
    //     });
    // };

    loadFromIndexedDb = function(storekey) {
        return new Promise((resolve, reject) => {
            const indexedDb = indexedDbUsed;
            const readTransaction = indexedDb.transaction([objectStoreName]) // Access to DB
            const targetObjectStore = readTransaction.objectStore(objectStoreName) // Access to ObjectStore
            const resultOfRead = targetObjectStore.get(storekey)

            resultOfRead.onerror = function() {
                console.log('Es gab einen Fehler bei de Anfrage: ' + resultOfRead.errorCode)
                reject(resultOfRead.errorCode)
            };

            resultOfRead.onsuccess = function() {            
                resolve(resultOfRead.result) // alternativ: event.target.result wenn in function event ist
            };
        });
    };

    // loadData = function(appDb, storekey) {
    //     return new Promise((resolve, reject) => {
    //         const readTransaction = appDb.transaction([objectStoreName]) // Access to DB
    //         const targetObjectStore = readTransaction.objectStore(objectStoreName) // Access to ObjectStore
    //         const resultOfRead = targetObjectStore.get(storekey)

    //         resultOfRead.onerror = function() {
    //             console.log('Es gab einen Fehler bei de Anfrage: ' + resultOfRead.errorCode)
    //             reject(resultOfRead.errorCode)
    //         };

    //         resultOfRead.onsuccess = function() {            
    //             resolve(resultOfRead.result) // alternativ: event.target.result wenn in function event ist
    //         };
    //     });
    // };
    
    closeIndexedDb = function() {
        const indexedDb = indexedDbUsed;
        indexedDb.close();
    };

    deleteIndexedDb = function() {
        return new Promise((resolve, reject) => {
            const transactionDelete = indexedDB.deleteDatabase(dbName)

            transactionDelete.onerror = function() {
                console.log('Es gab einen Fehler bei de Anfrage: ' + transactionDelete.errorCode)
                reject(transactionDelete.errorCode)
            };

            transactionDelete.onsuccess = function() {            
                resolve(transactionDelete.result) // alternativ: event.target.result wenn in function event ist
            };
        });
    };

    // public API / oeffentliche API
    return {
        /** eng: Validator function. Returns the boolean value, if the browser support the indexDB technology currently.
         * ger: Validierungsfunktion. Gibt den boolschen Wert zurueck, ob der Browser aktuell die IndexDB-Technologie unterstuetzt.
         * @returns {boolean} boolean value, if the browser support the indexDB technology
         */
        isIndexDBSupported: function() {
            const isIndexedDBSupported = (window.indexedDB !== undefined);
            if(!isIndexedDBSupported) { // TODO move to controler; only return value
                console.error('Der Browser unterstuetzt aktuell nicht die Technologie IndexDB!');
            }

            return isIndexedDBSupported;
        },

        /** Async! Saves the scenarios localy.
         * @param {Map<string, {}>} scenarios the scenarios
         * @returns {boolean} result of the operation
         */
        saveScenarios: async function(scenarios) {
            await openIndexedDb();
            await saveInIndexedDb(scenarioStoreKey, scenarios)
            closeIndexedDb();
            // TODO check how to handle multiple error handling from all the possible errors!

            return true;
        },

        /**  Async! Load and get the localy saved scenarios.
         * @returns {Map<string, {}>} loaded scenarios
         */
        loadScenarios: async function() {
            await openIndexedDb();
            const   loadedScenarios = await loadFromIndexedDb(scenarioStoreKey);  
            closeIndexedDb();
            
            return loadedScenarios;
        },

        /** Deletes all localy saved data. */
        deleteSaveData: function() {
            deleteIndexedDb();
        }
    }
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
        console.log('SaveAndLoad-Module is ready. |' + Date(Date.now()).slice(4,24));
}, false)
